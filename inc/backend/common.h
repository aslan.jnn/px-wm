#pragma once

#include "common/visualmap.h"
#include "sharedds/windowproperties.h"
#include <string>
#include <utility>
#include <xcb/damage.h>
#include <xcb/xcb.h>

namespace pxwm {
    namespace backend {
        namespace common {
            struct ManagedWindowData {
                xcb_damage_damage_t damage_id;
                sharedds::WindowGeomProperties geometries;
                uint8_t is_mapped = 0;
            };

            bool accept_window_for_redir(xcb_connection_t* conn,
                xcb_window_t window,
                pxwm::common::x11::VisualMap* visualmap);
            void add_window_default_event_listeners(xcb_connection_t* conn, xcb_window_t window);
            void create_damage_listener(xcb_connection_t* conn, xcb_window_t window, ManagedWindowData& data);
            void remove_window_default_event_listener(xcb_connection_t* conn,
                xcb_window_t window,
                ManagedWindowData& data);
        }
    }
}