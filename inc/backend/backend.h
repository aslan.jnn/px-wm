#pragma once

#include "config.h"
#include "sharedds/windowproperties.h"
#include <condition_variable>
#include <memory>
#include <string>
#include <xcb/xcb.h>

namespace pxwm {
    namespace backend {
        /**
         * The mother of backend eventloop threads. Starts both backend and frontend EventLoop in it's own thread.
         */
        class Backend {
        public:
            Backend(const std::shared_ptr<states::Config> cfg);
            Backend(const Backend&) = delete;
            Backend& operator=(const Backend&) = delete;
            ~Backend();
            void run();
            bool request_add_display(const std::string& display, pxwm::states::BkFrontEndOptions options);
            bool request_remove_bk_display(const std::string& display);
            bool request_rescale_bk_display(const std::string& display, int scfactor);
            std::pair<bool, pxwm::states::BkFrontEndOptions> get_bk_display_info(const std::string& display);
            const std::vector<std::string> get_managed_bk_displays();
            const std::pair<bool, std::vector<std::pair<xcb_window_t, xcb_window_t>>> get_managed_windows(
                const std::string& display);

        private:
            class Priv;
            std::unique_ptr<Priv> p;
        };
    }
}