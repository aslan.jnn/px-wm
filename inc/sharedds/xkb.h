#pragma once

#include <cstdint>

namespace pxwm {
    namespace sharedds {
        enum class XkbUpdateFlag : uint8_t { NONE = 0, DESC = 1, STATES = 2, CONTROLS = 4, ALL = 7 };

        enum class XkbDescUpdateFlag : uint8_t { NONE = 0, MAPS = 1, COMPAT_MAPS = 2, NAMES = 4, ALL = 7 };

        enum class XkbStateUpdateFlag : uint8_t {
            NONE = 0,
            LOCKED_MODS = 1,
            LATCHED_MODS = 2,
            LOCKED_GROUP = 4,
            LATCHED_GROUP = 8,
            ALL = 15
        };

        enum class XkbControlUpdateFlag : uint8_t {
            NONE = 0,
            STICKY_KEYS = 1,
            STICKY_KEYS_OPTIONS = 2,
            REPEAT_KEY = 4,
            REPEAT_KEY_DELAY_INTERVAL = 8,
            ALL = 15
        };

        struct XkbUpdatedInfo {
            XkbUpdateFlag flags = XkbUpdateFlag::NONE;
            XkbDescUpdateFlag desc_flags = XkbDescUpdateFlag::NONE;
            XkbControlUpdateFlag ctrl_flags = XkbControlUpdateFlag::NONE;
            XkbStateUpdateFlag state_flags = XkbStateUpdateFlag::NONE;

            constexpr static XkbUpdatedInfo none() {
                return XkbUpdatedInfo{
                    XkbUpdateFlag::NONE, XkbDescUpdateFlag::NONE, XkbControlUpdateFlag::NONE, XkbStateUpdateFlag::NONE};
            }

            constexpr static XkbUpdatedInfo all() {
                return XkbUpdatedInfo{
                    XkbUpdateFlag::ALL, XkbDescUpdateFlag::ALL, XkbControlUpdateFlag::ALL, XkbStateUpdateFlag::ALL};
            }
        };

        struct XkbStateChangedEventInfo {
            uint16_t changed;
            uint8_t latched_mods;
            uint8_t locked_mods;
            uint8_t latched_group;
            uint8_t locked_group;
        };
    }
}