#pragma once

#include <cstdint>
#include <string>

namespace pxwm {
    namespace sharedds {
        /**
         * @brief Contains data about a cursor.
         * By convention, when semantic_name is filled, the data is empty, and vice versa.
         */
        class CursorData {
        public:
            std::string semantic_name = "";
            struct {
                uint8_t format = 0;
                void* image;
                std::pair<uint16_t, uint16_t> size;
                std::pair<uint16_t, uint16_t> hotspot;
            } data;
        };
    }
}