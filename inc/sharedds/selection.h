#pragma once

#include "windowproperties.h"
#include <xcb/xcb.h>

namespace pxwm {
    namespace sharedds {
        struct SelectionInfo {
            xcb_atom_t selection = XCB_ATOM_NONE;
            xcb_atom_t multiple = XCB_ATOM_NONE;
            xcb_atom_t sr_prop = XCB_ATOM_NONE;
        };
        struct SelectionRequest {
            xcb_atom_t target;
            sharedds::XProperty param;
        };
    }
}