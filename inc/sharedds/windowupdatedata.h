#pragma once

#include <algorithm>
#include <cstdint>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <xcb/xcb.h>

namespace pxwm {
    namespace sharedds {
        class WindowUpdateData {
        public:
            uint16_t x1 = 65535;
            uint16_t y1 = 65535;
            uint16_t x2 = 0;
            uint16_t y2 = 0;
            bool updated = false;

            void reset() {
                x2 = y2 = updated = 0;
                x1 = y1 = 65535;
            }

            void add_box(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
                x1 = std::min(x1, (uint16_t)x);
                y1 = std::min(y1, (uint16_t)y);
                x2 = std::max(x2, (uint16_t)(w + x));
                y2 = std::max(y2, (uint16_t)(h + y));
            }
        };

        typedef std::unordered_map<xcb_window_t, WindowUpdateData> window_update_data_map_t;

        class WindowUpdateDataContainer {
        public:
            window_update_data_map_t data;
            std::unordered_set<xcb_window_t> updated;
        };
    }
}