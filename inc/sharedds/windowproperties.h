#pragma once

#include <memory>
#include <string>
#include <utility>
#include <xcb/xcb.h>

namespace pxwm {
    namespace sharedds {

        struct WindowGeomProperties {
            /**
             * Size of this window, stored as "width,height"
             */
            std::pair<uint16_t, uint16_t> size;
            /**
             * Position of this window relative to top-left screen, stored as "x,y"
             */
            std::pair<int16_t, int16_t> pos;
            /**
             * Border size of the window according to X11. Currently unused.
             */
            uint16_t border_size;
            /**
             * A boolean value that specifies whether the window prefer it's geometry or structure intact, not altered
             * by the window manager.
             */
            uint8_t override_redirect;
        };

        struct XPropertyValue {
            xcb_atom_t prop_type = 0;
            /**
             * Value can be either 0, 8, 16 or 32. If set to 0, then the complete value is not exist for this property
             * (e.g. error occurred when retrieving property value, or the property value hasn't been fetched yet.)
             */
            uint8_t format_bpp = 0;
            /**
             * Size of data pointed by data_ptr, in bytes.
             */
            uint32_t data_len = 0;
            std::shared_ptr<void> data_ptr = nullptr;
        };

        struct XProperty {
            xcb_atom_t prop_atom = 0;
            XPropertyValue value;
        };
    }
}