#pragma once

#include "3rdparty/xcbxkb_compat.h"
#include "sharedds/selection.h"
#include "sharedds/windowproperties.h"
#include "sharedds/xkb.h"
#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <type_traits>
#include <xcb/xproto.h>

namespace pxwm {
    namespace intermsg {
        namespace frontendeventloop {
            // to clearly communicate the intent :)
            typedef uint8_t sbool_t;

            enum class FrontEndResultType : uint8_t {
                NONE,
                CLOSE,
                GEOM_CFG,
                MOUSE_MOTION,
                KEYBUTTON,
                DEPRESS_ALL,
                FOCUS_REQ,
                FOCUS_OUT,
                XKB_UPDATE,
                PROPERTY_CHANGE,
                CLIENT_MESSAGE,
                SELECTION_REPLY,
                SELECTION_OWN,
                SELECTION_REQUEST
            };

            struct Message {
                FrontEndResultType type = FrontEndResultType::NONE;
            };

            struct WindowCloseMessage {
                FrontEndResultType type = FrontEndResultType::CLOSE;
                xcb_window_t back_window_id;
            };

            enum class GeomCfgUpdated : uint8_t { NONE = 0, SIZE = 1, POS = 2 };
            struct GeomCfgMessage {
                FrontEndResultType type = FrontEndResultType::GEOM_CFG;
                xcb_window_t back_window_id;
                GeomCfgUpdated updated = GeomCfgUpdated::NONE;
                sharedds::WindowGeomProperties new_geoms;
            };

            struct MouseMotionMessage {
                FrontEndResultType type = FrontEndResultType::MOUSE_MOTION;
                xcb_window_t back_window_id;
                sbool_t pressed;
                uint16_t x;
                uint16_t y;
            };

            enum class KeyButtonType : uint8_t { KEY, BUTTON };

            struct KeyButtonMessage {
                FrontEndResultType type = FrontEndResultType::KEYBUTTON;
                KeyButtonType kb_type;
                uint8_t kb_code;
                uint16_t state;
                sbool_t pressed;
            };

            struct DepressAllRequestMessage {
                FrontEndResultType type = FrontEndResultType::DEPRESS_ALL;
            };

            struct FocusRequestMessage {
                FrontEndResultType type = FrontEndResultType::FOCUS_REQ;
                xcb_window_t back_window_id;
            };

            struct FocusOutMessage {
                FrontEndResultType type = FrontEndResultType::FOCUS_OUT;
            };

            struct XkbUpdateMessage {
                FrontEndResultType type = FrontEndResultType::XKB_UPDATE;
                sharedds::XkbUpdatedInfo updated = sharedds::XkbUpdatedInfo::none();
                sharedds::XkbStateChangedEventInfo state_evt = {0};
            };

            struct PropertyChangeMessage {
                FrontEndResultType type = FrontEndResultType::PROPERTY_CHANGE;
                xcb_window_t target;
                xcb_atom_t atom;
                /**
                 * use only values from xcb_property_t.
                 */
                uint8_t state;
                /**
                 * ID is specific to class that gave this message. Only used if state equals to XCB_PROPERTY_NEW_VALUE.
                 */
                uint16_t prop_id;
            };

            struct CMMessage {
                FrontEndResultType type = FrontEndResultType::CLIENT_MESSAGE;
                /**
                 * ID is specific to class that gave this message. Can be used to retrieve the data of this message.
                 */
                uint16_t cm_id;
            };

            struct SelReplyMessage {
                FrontEndResultType type = FrontEndResultType::SELECTION_REPLY;
                xcb_window_t requestor;
            };

            struct SelOwnMessage {
                FrontEndResultType type = FrontEndResultType::SELECTION_OWN;
                xcb_atom_t selection;
                bool takeown;
            };

            struct SelRequestMessage {
                FrontEndResultType type = FrontEndResultType::SELECTION_REQUEST;
                xcb_window_t requestor;
            };

            constexpr int MSG_MAX_SIZE = std::max(
                {sizeof(Message), sizeof(WindowCloseMessage), sizeof(GeomCfgMessage), sizeof(MouseMotionMessage),
                    sizeof(KeyButtonMessage), sizeof(DepressAllRequestMessage), sizeof(FocusRequestMessage),
                    sizeof(FocusOutMessage), sizeof(XkbUpdateMessage), sizeof(PropertyChangeMessage), sizeof(CMMessage),
                    sizeof(SelReplyMessage), sizeof(SelOwnMessage), sizeof(SelRequestMessage)});

            typedef struct alignas(alignof(std::max_align_t)) { uint8_t data[MSG_MAX_SIZE]; } message_store_t;
        }
    }
}