#pragma once

#include <cstdint>

namespace pxwm {
    namespace intermsg {
        namespace eventloopbackend {
            constexpr int MSG_SIZE = 2;

            // messages to p backend
            enum class Type : uint8_t { NOTHING, STOPPED };

            enum class StopCause : uint8_t {
                UNKNOWN,
                SERVER_DISCONNECT,
                FRONTEND_SERVER_DISCONNECT,
                FRONTEND_SERVER_CANNOT_CONNECT,
                FRONTEND_UNSUPPORTED_EXT,
                FRONTEND_INIT_ERROR,
                FRONTEND_VISUAL_UNSUPPORTED,
                XKB_INIT_ERR,
                REQ_STOP
            };

            struct Message {
                Type type;
                uint8_t pad[MSG_SIZE - 1];
            };

            struct StopMessage {
                Type type;
                StopCause stop_cause;
            };
        }
    }
}