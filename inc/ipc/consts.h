#pragma once

#include <map>
#include <string>

namespace pxwm {
    namespace ipc {
        struct ConstData {
            static constexpr int IPC_API_VERSION = 2;
            enum class ServerRet { SUCCESS, FAIL, UNKNOWN_CMD, INVALID_PARAM, NO_DISPLAY, INVALID_VALUE };

            std::map<ServerRet, std::string> serverret_value_map{{ServerRet::SUCCESS, "success"},
                {ServerRet::FAIL, "fail"}, {ServerRet::INVALID_PARAM, "invalid_param"},
                {ServerRet::UNKNOWN_CMD, "unknown_command"}, {ServerRet::NO_DISPLAY, "no_display"},
                {ServerRet::INVALID_VALUE, "invalid_value"}};
            std::map<std::string, ServerRet> serverret_string_map;

            ConstData();
        };
        ConstData& ipc_consts();
    }
}