#pragma once

#include <memory>
#include <string>

namespace pxwm {
    namespace ipc {
        class Client {
        public:
            Client(std::string socketfile);
            Client(const Client&) = delete;
            Client& operator=(const Client&) = delete;
            ~Client();
            void run_cmdline_args(int argc, char** argv);

        private:
            class Priv;
            std::unique_ptr<Priv> p;
        };
    }
}