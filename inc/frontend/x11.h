#pragma once

#include "common/visualmap.h"
#include "config.h"
#include "intermsg/frontend_eventloop.h"
#include "sharedds/clientmsgdata.h"
#include "sharedds/cursordata.h"
#include "sharedds/selection.h"
#include "sharedds/windowproperties.h"
#include "sharedds/windowupdatedata.h"
#include <memory>
#include <queue>
#include <utility>
#include <xcb/xcb.h>

namespace pxwm {
    namespace frontend {
        class X11Frontend {
        public:
            X11Frontend(xcb_connection_t* back_conn,
                std::string back_display_name,
                xcb_window_t back_root,
                pxwm::sharedds::WindowUpdateDataContainer& wupd_data,
                std::queue<pxwm::intermsg::frontendeventloop::message_store_t>& ret_msg_queue,
                std::shared_ptr<common::x11::VisualMap> be_visualmap,
                pxwm::states::BkFrontEndOptions options);
            X11Frontend(X11Frontend&) = delete;
            ~X11Frontend();
            X11Frontend& operator=(const X11Frontend&) = delete;
            bool is_frontend_connected();
            bool frontend_has_exts();
            bool frontend_init_error();
            bool frontend_visual_unsupported();
            xcb_connection_t* get_fconn();
            void new_window(xcb_window_t back_window_id,
                sharedds::WindowGeomProperties& geometries,
                std::vector<sharedds::XProperty>& properties,
                std::pair<std::uint32_t, xcb_rectangle_t*> shape_data,
                common::x11::VisualInfo be_visualinfo);
            void delete_window(xcb_window_t back_window_id);
            void unmap_window(xcb_window_t back_window_id);
            void map_window(xcb_window_t back_window_id, sharedds::WindowGeomProperties& geometries);
            void req_refresh_all_windows();
            void req_rescale(uint16_t scfactor);
            /**
             * @brief Poll frontend for events and handle those events.
             * All property and client message IDs will be invalidated after a call to this method.
             * @return false if an error occurred, true otherwise.
             */
            bool poll_event();
            void stop();
            /**
             * Reconfigure frontend's size.
             * @param bwin Backend window ID of managed window.
             * @param geometries New geometry of the managed window. Only size will be taken care of.
             */
            void hdlbe_configure_notify(xcb_window_t bwin, sharedds::WindowGeomProperties& geometries);
            /**
             * Notify frontend that a property has been changed or deleted.
             * @param cevt Backend's property change event.
             */
            void hdlbe_property_change(xcb_property_notify_event_t* cevt);
            /**
             * Notify frontend that somebody at backend sends a client message.
             * @param cevt Backend's client message event.
             */
            void hdlbe_client_message(xcb_client_message_event_t* cevt);
            /**
             * Notify frontend that backend's focus has switched to a window.
             * @param be_wid Backend's window that received a focus in.
             */
            void hdlbe_focus_in(xcb_window_t be_wid);
            /**
             * Notify frontend that backend's cursor is changed.
             * @param cur The cursor data. Contents in the image pointer will be copied first. If format is 0,
             * semantic_name will be used instead.
             */
            void hdlbe_cursor_change(sharedds::CursorData cur);
            /**
             * Notify frontend that a window has changed it's shape.
             * @param be_wid Backend's window that changed it's shape.
             * @param rect_len Ammount of rectangle in new shape.
             * @param rects New shape's rectangle data. This data will be copied.
             */
            void hdlbe_shape_change(xcb_window_t be_wid, uint32_t rect_len, xcb_rectangle_t* rects);
            /**
             * Notify frontend that a window in backend is requesting a selection value.
             * @param be_wid The backend window that initiates the request.
             * @param selinfo The selection atom in which the request take place, and it's associated property.
             * @param reqs A reference to a list of data and their target properties that is being requested.
             * @return true if frontend acknowledge the request, false otherwise.
             */
            bool hdlbe_selection_request(xcb_window_t be_wid,
                sharedds::SelectionInfo selinfo,
                std::vector<sharedds::SelectionRequest>& reqs);
            /**
             * Notify frontend that a backend selection owner has replied, and supply the replied data.
             * @param requestor Requestor window ID.
             * @param selinfo The selection atom in which the request take place, and it's associated property. Value of
             * NONE in both properties atom means that the owner rejected the request.
             * @param data The replied data from the selection owner.
             */
            void hdlbe_selection_reply(uint32_t requestor,
                sharedds::SelectionInfo selinfo,
                std::vector<sharedds::XProperty>& data);
            /**
             * Take own or give up the selection ownership.
             * @param mode true to take own, false to give up.
             */
            void hdlbe_take_own_selection(xcb_atom_t bk_sel, bool mode);
            /**
             * Get all windows managed by this frontend.
             * @return A vector of tuple of xcb_window_t, where the first of the tuple is backend window ID, and the
             * second of the tuple is frontend window ID.
             */
            std::vector<std::pair<xcb_window_t, xcb_window_t>> get_managed_windows();
            /**
             * @brief Returns a reference to XProperties identified by 'id'.
             * XProperty returned by this function will be invalidated after call to poll_event.
             * @param id Property ID given by PropertiesForwardMessage.
             * @return Pointer to XProperty if id points to a valid property, nullptr otherwise.
             */
            sharedds::XPropertyValue* get_prop_from_msg(int id);
            sharedds::ClientMsg* get_cm_data_from_msg(int id);
            std::pair<sharedds::SelectionInfo, std::vector<sharedds::XProperty>> get_sel_reply_data_from_msg(
                xcb_window_t requestor);
            std::pair<sharedds::SelectionInfo, std::vector<sharedds::SelectionRequest>>
            get_sel_request_refdata_from_msg(xcb_window_t requestor);

        private:
            class Priv;
            class EventHandlers;
            std::unique_ptr<Priv> p;
        };
    }
}