#pragma once

#include "frontend/widtrans.h"
#include "sharedds/clientmsgdata.h"
#include <memory>
#include <xcb/xcb.h>

namespace pxwm {
    namespace frontend {
        namespace x11 {
            class CMMgr {
            public:
                CMMgr(WIDTrans* wid_trans, xcb_connection_t* fconn, xcb_connection_t* bconn);
                ~CMMgr();
                CMMgr(const CMMgr&) = delete;
                CMMgr& operator=(const CMMgr&) = delete;
                /**
                 * Modify backend's received client message, so that it is suitable for use in frontend.
                 * @param prop Reference to client message data to modify. Guaranteed to be intact if return is false.
                 * Set target and atom members to backend's.
                 * @return true if this client message should be forwarded to frontend, false otherwise.
                 */
                bool forward_cm(pxwm::sharedds::ClientMsg& cm);
                /**
                 * Modify frontend's received client message, so that it is suitable for use in backend.
                 * @param prop Reference to client message data to modify. Guaranteed to be intact if return is false.
                 * Set target and atom members to frontend's.
                 * @return true if this client message should be forwarded to backend, false otherwise.
                 */
                bool backward_cm(pxwm::sharedds::ClientMsg& cm);

                static pxwm::sharedds::ClientMsg convert_from_event(xcb_client_message_event_t* evt);

            private:
                class Priv;
                std::unique_ptr<Priv> p;
            };
        }
    }
}