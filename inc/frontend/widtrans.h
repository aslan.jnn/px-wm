#pragma once

#include <xcb/xcb.h>

namespace pxwm {
    namespace frontend {
        class WIDTrans {
        public:
            virtual ~WIDTrans() = default;
            /**
             * Translate backend's window ID to frontend's.
             * @param bw_id Backend's window ID.
             * @return Frontend's window ID if the requested window is managed by frontend, 0 otherwise.
             */
            virtual xcb_window_t get_fw_id(xcb_window_t bw_id) = 0;
            /**
             * Translate frontend's window ID to backend's.
             * @param fw_id Frontend's window ID.
             * @return Backend's window ID if the requested window is managed by frontend, 0 otherwise.
             */
            virtual xcb_window_t get_bw_id(xcb_window_t fw_id) = 0;
            /**
             * Retrieve frontend's root window ID.
             */
            virtual xcb_window_t get_fe_root_id() = 0;
            /**
             * Retrieve backend's root window ID.
             */
            virtual xcb_window_t get_be_root_id() = 0;
        };
    }
}