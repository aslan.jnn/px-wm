#pragma once

#include "common/visualmap.h"
#include "shared/memory.h"
#include <map>
#include <set>
#include <string>
#include <xcb/xcb.h>

namespace pxwm {
    namespace common {
        namespace x11 {
            constexpr int MAX_PROPERTIES_SIZE = 16777216;

            struct XExtensionInfo {
                uint8_t opcode, first_error, first_event;
            };

            bool init(const std::string& display, xcb_connection_t*& out_conn, xcb_screen_t*& out_screen);
            bool is_screen_color_supported(VisualMap* visualmap);
            /**
             * Connect and check the display server for supported color configuration.
             */
            bool connect_and_check_display(const std::string& display,
                std::shared_ptr<VisualMap>& visualmap,
                bool is_backend,
                xcb_connection_t*& out_conn,
                xcb_screen_t*& out_screen);
            bool is_visual_supported(const VisualInfo& visual);
            void disconnect_display(xcb_connection_t* conn);
            xcb_atom_t get_atom(xcb_connection_t* xconn, bool only_if_exists, const std::string name);
            std::string get_atom_name(xcb_connection_t* xconn, const xcb_atom_t atom);
            xcb_atom_t translate_atom(xcb_connection_t* src_conn, xcb_connection_t* dst_conn, xcb_atom_t atom);
            void translate_atoms(xcb_connection_t* src_conn,
                xcb_connection_t* dst_conn,
                uint32_t atoms_count,
                xcb_atom_t* atoms);
            void clear_atom_cache(xcb_connection_t* xconn);
            /**
             * Make sure extensions required is present on the X server, and return their major opcode, first error and
             * first event.
             * @param ext_ids A set of opaque pointer to xcb_extension_t provided by XCB.
             * @param ret pointer to map to XExtensionInfo, which contains the extension's major opcode, first error and
             * first event.
             * @return true if all extensions is present on the server, false otherwise.
             */
            bool query_extensions(xcb_connection_t* xconn,
                std::set<xcb_extension_t*> ext_ids,
                std::map<xcb_extension_t*, XExtensionInfo>* ret);
            /**
             * Compare one or more extensions and it's version against predefined version requirements. Will print a
             * message
             * if an extension isn't supported or extension version is earlier than the predefined version.
             * @param ext_ids A set of opaque pointer to xcb_extension_t provided by XCB.
             * @return true if all extensions is present on the server and their version is supported, false otherwise.
             */
            bool init_and_check_extensions_version(xcb_connection_t* xconn, std::set<xcb_extension_t*> ext_ids);
        }
    }
}