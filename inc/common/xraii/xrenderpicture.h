#pragma once

#include <xcb/render.h>
#include <xcb/xcb.h>

namespace pxwm {
    namespace common {
        namespace xraii {
            /**
             * @brief RAII wrapped version of XCB's render picture.
             * This object is meant to be used with a smart pointer, and therefore cannot be moved or copied.
             */
            class XRenderPicture {
            public:
                xcb_connection_t* const conn;
                const xcb_render_picture_t id;

                XRenderPicture();
                XRenderPicture(xcb_connection_t* conn,
                    xcb_drawable_t drawable,
                    xcb_render_pictformat_t format,
                    uint32_t value_mask,
                    const uint32_t* value_list);
                XRenderPicture(const XRenderPicture&) = delete;
                XRenderPicture& operator=(const XRenderPicture&) = delete;
                ~XRenderPicture();

            private:
                xcb_render_picture_t init(xcb_connection_t* conn,
                    xcb_drawable_t drawable,
                    xcb_render_pictformat_t format,
                    uint32_t value_mask,
                    const uint32_t* value_list);
            };
        }
    }
}