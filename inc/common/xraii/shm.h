#pragma once

#include <xcb/xcb.h>
#include <xcb/xcb_image.h>

namespace pxwm {
    namespace common {
        namespace xraii {
            class SHM {
            public:
                xcb_connection_t* const conn;
                /**
                 * Size of the SHM segment as specified by user.
                 */
                const uint32_t size;
                /**
                 * SHM info, for use with XCB's MIT-SHM extension.
                 */
                const xcb_shm_segment_info_t info;
                /**
                 * true if successfully attached the given SHM segment to server success, false otherwise.
                 */
                const bool success;

                /**
                 * Construct this instance, create a new SHM segment, and attach it to the specified display server.
                 * @param size Size of the SHM segment.
                 */
                SHM(xcb_connection_t* conn, uint32_t size);
                /**
                 * @brief Construct this instance with an existing SHM segment, and attach it to the specified display
                 * server.
                 * This instance will create another attachment to the specified SHM ID.
                 * @param size Size of the existing SHM segment.
                 * @param shmid An ID generated by a successful call to shmget.
                 */
                SHM(xcb_connection_t* conn, uint32_t size, int shmid);
                SHM(const SHM&) = delete;
                SHM& operator=(const SHM&) = delete;
                ~SHM();

            private:
                bool valid_shmid = true;
                xcb_shm_segment_info_t generate_new();
                xcb_shm_segment_info_t generate_existing(int shmid);
                bool init();
            };
        }
    }
}