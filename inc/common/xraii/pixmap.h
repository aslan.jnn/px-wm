#pragma once

#include <xcb/xcb.h>

namespace pxwm {
    namespace common {
        namespace xraii {
            /**
             * @brief RAII wrapped version of XCB's pixmap.
             * This object is meant to be used with a smart pointer, and therefore cannot be moved or copied.
             */
            class Pixmap {
            public:
                xcb_connection_t* const conn;
                const xcb_pixmap_t id;

                Pixmap();
                /**
                 * @brief Wrap an existing, created pixmap, in this instance.
                 * The pixmap destruction will be managed by this instance.
                 * @param pxm_id an X ID. This ID must already refer to a pixmap.
                 */
                Pixmap(xcb_connection_t* conn, xcb_pixmap_t pxm_id);
                /**
                 * Construct this instance and create a new pixmap.
                 */
                Pixmap(xcb_connection_t* conn, uint8_t depth, xcb_drawable_t drawable, uint16_t width, uint32_t height);
                Pixmap(const Pixmap&) = delete;
                Pixmap& operator=(const Pixmap&) = delete;
                ~Pixmap();

            private:
                xcb_pixmap_t init(xcb_connection_t* conn,
                    uint8_t depth,
                    xcb_drawable_t drawable,
                    uint16_t width,
                    uint32_t height);
            };
        }
    }
}