#pragma once

#include <xcb/xcb.h>

namespace pxwm {
    namespace common {
        namespace xraii {
            /**
             * @brief RAII wrapped version of XCB's colormap.
             * This object is meant to be used with a smart pointer, and therefore cannot be moved or copied.
             */
            class ColorMap {
            public:
                xcb_connection_t* const conn;
                const xcb_colormap_t id;

                ColorMap();
                /**
                 * Create a default ColorMap in the associated display with allocation set to none.
                 */
                ColorMap(xcb_connection_t* conn, xcb_window_t window, xcb_visualid_t visual);
                ColorMap(const ColorMap&) = delete;
                ColorMap& operator=(const ColorMap&) = delete;
                ~ColorMap();

            private:
                xcb_colormap_t init(xcb_connection_t* conn, xcb_window_t window, xcb_visualid_t visual);
            };
        }
    }
}