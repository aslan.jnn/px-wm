#pragma once

#include "shared/fixedpt.h"
#include <map>
#include <memory>
#include <set>
#include <string>
#include <utility>
#include <vector>

namespace pxwm {
    namespace states {

        enum class ScalingAlgorithm { NEAREST, BILINEAR, RAA };
        enum class FrameRateMode : int { LOW = 4, NORMAL = 2, HIGH = 1 };
        enum class SHMMode : uint8_t { NONE = 0, IMAGE = 1, PIXMAP = 2 };

        struct ConstsData {
            const std::string appname = "px-wm";
            const std::string default_socketfile_path;
            static constexpr uint16_t min_scaling_factor = 1;
            static constexpr uint16_t max_scaling_factor = 8;
            const std::map<ScalingAlgorithm, std::pair<uint8_t, uint8_t>> scale_alg_scale_limits;
            const std::map<std::string, ScalingAlgorithm> avail_scale_algorithms;
            const std::map<std::string, FrameRateMode> avail_framerate_modes;
            const std::map<std::string, SHMMode> avail_shm_modes;
            const std::map<ScalingAlgorithm, std::string> vmap_scale_algorithms;
            const std::map<FrameRateMode, std::string> vmap_framerate_modes;
            const std::map<SHMMode, std::string> vmap_shm_modes;
            ConstsData();

        private:
            struct Priv;
            std::unique_ptr<Priv> p;
        };

        struct Consts {
            static const ConstsData values;
        };

        struct BkFrontEndOptions {
            ScalingAlgorithm scale_algorithm = ScalingAlgorithm::NEAREST;
            FrameRateMode framerate_mode = FrameRateMode::NORMAL;
            SHMMode max_shm_mode = SHMMode::PIXMAP;
            uint16_t scale_factor = 1;
        };

        struct Config {
            std::map<std::string, BkFrontEndOptions> bk_displays;
            std::string socketfile_path = Consts::values.default_socketfile_path;
        };
    }
}