#pragma once

#include "3rdparty/xcbxkb_compat.h"
#include "sharedds/xkb.h"
#include "xkb/x11frontend.h"
#include <memory>
#include <xcb/xcb.h>

namespace pxwm {
    namespace xkb {
        class Backend {
        public:
            /**
             * @brief Construct the instance.
             * @param bconn A connected and working xcb connection to backend.
             */
            Backend(xcb_connection_t* bconn);
            ~Backend();
            /**
             * @brief Initialize backend's XKB with the maps, states and controls from the specified frontend.
             * This method must be called once and only once before calling all other functions from backend.
             * @param fe A valid instance of XKB frontend, wrapped in shared_ptr.
             * @return true if all maps, states and controls are successfully copied from the specified frontend to
             * backend, false otherwise.
             */
            bool init(std::shared_ptr<X11Frontend> fe);
            /**
             * Instruct backend to retreive XKB data from frontend and update the backend accordingly.
             * @param updated A set of flags to control which update will be done.
             * @param state_info State change event. If specified, will fetch the state data from this instead of
             * requesting the frontend when updated has STATES set but not DESC set.
             * @return true if all request updated successfully, false otherwise.
             */
            bool update_backend(sharedds::XkbUpdatedInfo updated,
                sharedds::XkbStateChangedEventInfo* state_info = nullptr);

        private:
            class Priv;
            std::unique_ptr<Priv> p;
        };
    }
}