#include "frontend/x11.h"
#include "3rdparty/xcbxkb_compat.h"
#include "common/eventpooler.h"
#include "common/handlerhelpers.h"
#include "common/selmgr.h"
#include "common/x11.h"
#include "common/xraii/gc.h"
#include "common/xraii/pixmap.h"
#include "common/xraii/xrenderpicture.h"
#include "frontend/widtrans.h"
#include "frontend/x11/cmmgr.h"
#include "frontend/x11/propmgr.h"
#include "frontend/x11/renderer.h"
#include "intermsg/frontend_eventloop.h"
#include "shared/bitwiseenumclass.h"
#include "shared/i18n.h"
#include "shared/memory.h"
#include "shared/util.h"
#include "sharedds/clientmsgdata.h"
#include <assert.h>
#include <atomic>
#include <cstdio>
#include <glog/logging.h>
#include <unordered_map>
#include <xcb/composite.h>
#include <xcb/xcb.h>
#include <xcb/xcb_icccm.h>
#include <xcb/xcb_renderutil.h>
#include <xcb/xcb_util.h>
#include <xcb/xfixes.h>

using namespace std;
using namespace px::bitwiseenumclass;
using namespace px::memory;
using namespace px::util;
using namespace pxwm;
using namespace pxwm::common;
using namespace pxwm::sharedds;
using namespace pxwm::states;
using namespace pxwm::frontend;
using namespace pxwm::common::x11;

typedef pair<uint16_t, uint16_t> geom_pair_t;

class X11Frontend::Priv {
public:
    struct FWindowData;
    struct SimpleGeometry;
    class X11FWIDTrans;
    typedef unordered_map<xcb_window_t, shared_ptr<X11Frontend::Priv::FWindowData>> w_map_t;

    BkFrontEndOptions options;
    //'new_scfactor_req' is used to enforce ordering of live rescaling routine
    atomic<uint16_t> new_scfactor_req;
    unique_ptr<EventHandlers> eh;
    // essentials
    string back_display_name;
    xcb_connection_t* be_conn;
    xcb_connection_t* fe_conn;
    xcb_screen_t* fe_scr;
    xcb_window_t be_root;
    xcb_window_t fe_root;
    unordered_set<uint8_t> fe_supported_pixmap_depth;
    map<uint8_t, xcb_render_pictforminfo_t> fe_render_formats;
    map<xcb_extension_t*, common::x11::XExtensionInfo> xexts;
    // flags
    bool is_connected = false, fe_has_ext = false, fe_init_has_err = false, fe_visual_unsupported = false;
    bool first_start = true;
    // backend <-> frontend
    WindowUpdateDataContainer& wudc;
    queue<intermsg::frontendeventloop::message_store_t>& msg_queue;
    // helpers, managers
    unique_ptr<WIDTrans> wid_trans;
    unique_ptr<x11::PropMgr> propmgr;
    unique_ptr<x11::CMMgr> cmmgr;
    unique_ptr<SelMgr> selmgr;
    shared_ptr<VisualMap> fe_visualmap;
    shared_ptr<VisualMap> be_visualmap;
    unique_ptr<x11::Renderer> renderer;
    // data stores for helpers
    vector<XPropertyValue> bk_propmsg_store;
    vector<sharedds::ClientMsg> bk_cmdata_store;
    map<xcb_window_t, queue<pair<SelectionInfo, vector<XProperty>>>> bk_selreply_store;
    map<xcb_window_t, pair<SelectionInfo, vector<SelectionRequest>>> bk_selrequest_store;
    // helpers for backend event handler
    struct {
        unique_ptr<handlerhelpers::CursorHelper> cursor_helper;
    } hdlbe_helpers;

    w_map_t bw_map;
    w_map_t fw_map;
    /**
     * Contains currently focused window. Aranged in (backend window ID,frontend window ID) tuple.
     */
    pair<xcb_window_t, xcb_window_t> focused = {0, 0};
    xcb_window_t requested_focus = 0;

    Priv(WindowUpdateDataContainer& p_wupd_data, queue<intermsg::frontendeventloop::message_store_t>& msg_queue)
        : wudc(p_wupd_data), msg_queue(msg_queue) {}
    Priv(Priv&) = delete;
    ~Priv() {}
    Priv& operator=(const Priv&) = delete;

    void check_resolution_match(uint16_t new_scfactor);
    bool check_frontend_has_visual_supported();
    xcb_window_t create_frontend_window(WindowGeomProperties& geometries,
        VisualInfo be_visual,
        VisualInfo& out_fe_visual);
    void redraw_window(FWindowData& data, WindowUpdateData& bbox);
    void close_window(xcb_window_t fwid);
    /**
     * @brief Set or delete property to a frontend's window, according to the supplied parameter.
     * Note that this method doesn't sync. Caller should manually sync the connection. This method also doesn't check if
     * a window is owned by frontend. It is caller's responsibility to make sure that a window is a valid frontend's
     * owned window.
     */
    void set_fwin_property(x11::PropData& prop);
    bool select_frontend_xkb_events();
    void select_root_events();
    void first_start_routine();
    void backward_all_root_properties();
    void check_and_takeown_selections();
    void set_shape(xcb_window_t fwid);
    void rescale(uint16_t scfactor);
    void process_pre_events();
    std::vector<frontend::x11::PropData> generate_initial_properties(xcb_window_t bwin_id);
    void forward_premap_properties(xcb_window_t bwin_id);
    void push_be_property_change(x11::PropData& data);
};

struct X11Frontend::Priv::FWindowData {
    // basic
    xcb_window_t bwin_id;
    xcb_window_t fwin_id;
    xcb_window_t fparent_id;
    WindowGeomProperties geometries;
    // only 'pos' and 'size' member will be used.
    WindowGeomProperties scaled_geom;

    // 0 means unshaped
    pair<uint32_t, unique_ptr<xcb_rectangle_t, free_delete>> shape_rects;

    // flags
    bool pause_draw = false;
    bool req_full_redraw = false;
    bool withdrawn = false;
};

class X11Frontend::Priv::X11FWIDTrans : public WIDTrans {
public:
    X11FWIDTrans(Priv* p) : p{p} {}
    xcb_window_t get_fw_id(xcb_window_t bw_id);
    xcb_window_t get_bw_id(xcb_window_t fw_id);
    xcb_window_t get_fe_root_id();
    xcb_window_t get_be_root_id();

private:
    Priv* p;
};

class X11Frontend::EventHandlers {
public:
    unique_ptr<EventPooler> evt_pooler;

    EventHandlers(Priv* p);
    EventHandlers(EventHandlers&) = delete;
    ~EventHandlers() {}
    EventHandlers& operator=(const EventHandlers&) = delete;

    bool poll_and_handle_events();

private:
    Priv* p;
    // helpers
    handlerhelpers::FocusIOHelper focus_io_helper;
    handlerhelpers::ConfigureNotifyHelper configure_notify_helper;

    void handle_error(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_expose(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_reparent_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_configure_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_focus_notify(xcb_generic_event_t* evt, bool in, int ord, bool is_last);
    void handle_motion(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_keybutton(xcb_generic_event_t* evt,
        intermsg::frontendeventloop::KeyButtonType kb_type,
        bool pressed,
        int ord,
        bool is_last);
    void handle_client_message(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_destroy_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_property_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_selection_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_selection_request(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_xkb(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_xfixes_selection_notify(xcb_generic_event_t* evt, int ord, bool is_last);
};

X11Frontend::X11Frontend(xcb_connection_t* back_conn,
    string back_display_name,
    xcb_window_t back_root,
    pxwm::sharedds::WindowUpdateDataContainer& wupd_data,
    queue<intermsg::frontendeventloop::message_store_t>& ret_msg_queue,
    std::shared_ptr<common::x11::VisualMap> be_visualmap,
    BkFrontEndOptions options) {
    p = make_unique<Priv>(wupd_data, ret_msg_queue);
    p->back_display_name = back_display_name;
    p->be_conn = back_conn;
    p->be_root = back_root;
    p->wudc = wupd_data;
    p->options = options;
    p->new_scfactor_req = options.scale_factor;
    p->be_visualmap = be_visualmap;
    p->is_connected = common::x11::connect_and_check_display("", p->fe_visualmap, false, p->fe_conn, p->fe_scr);
    // check and initialize required frontend's extension
    if (p->is_connected) {
        set<xcb_extension_t*> req_exts{&xcb_xkb_id, &xcb_render_id, &xcb_xfixes_id};
        if (common::x11::query_extensions(p->fe_conn, req_exts, &p->xexts) &&
            common::x11::init_and_check_extensions_version(p->fe_conn, req_exts)) {
            // OK!
            p->fe_has_ext = true;
            p->fe_root = p->fe_scr->root;
            // check RENDER formats and pixmap formats
            for (auto depth : {pair<uint8_t, xcb_pict_standard_t>(24, XCB_PICT_STANDARD_RGB_24),
                     pair<uint8_t, xcb_pict_standard_t>(32, XCB_PICT_STANDARD_ARGB_32)}) {
                if (auto xrender_format_sel_p =
                        xcb_render_util_find_standard_format(xcb_render_util_query_formats(p->fe_conn), depth.second)) {
                    // first, we check first if standard render format is available
                    if (xrender_format_sel_p) {
                        p->fe_render_formats[depth.first] = *xrender_format_sel_p;
                        // then we check for pixmap format for this depth
                        if (p->fe_visualmap->get_pixmap_depth_bpp_format(depth.first).count(32) &&
                            p->fe_visualmap->get_pixmap_scanline_pad(depth.first).count(32)) {
                            p->fe_supported_pixmap_depth.insert(depth.first);
                        }
                    }
                }
            }
            // make sure the frontend has at least one supported visual, in addition to pixmap depth (and render
            // format).
            if (!(p->fe_supported_pixmap_depth.size() && p->check_frontend_has_visual_supported())) {
                p->fe_visual_unsupported = true;
                return;
            }
            // warn user if sizes isn't optimal
            p->check_resolution_match(options.scale_factor);
            // important objects
            p->wid_trans = make_unique<Priv::X11FWIDTrans>(p.get());
            p->propmgr = make_unique<x11::PropMgr>(p->wid_trans.get(), p->fe_conn, p->be_conn);
            p->propmgr->set_fe_scaling_factor(p->options.scale_factor);
            p->cmmgr = make_unique<x11::CMMgr>(p->wid_trans.get(), p->fe_conn, p->be_conn);
            p->selmgr = make_unique<SelMgr>(p->fe_conn, p->fe_scr);
            p->renderer = make_unique<x11::Renderer>(p->back_display_name, p->be_conn, p->fe_conn, p->fe_scr,
                p->be_visualmap.get(), p->fe_visualmap.get(), p->fe_render_formats, p->options);
            if (p->renderer->init_has_error()) {
                p->fe_init_has_err = true;
                return;
            }
            // internal (nested) objects
            p->eh = make_unique<EventHandlers>(p.get());
            // helpers objects
            p->hdlbe_helpers.cursor_helper = make_unique<handlerhelpers::CursorHelper>(p->fe_conn, p->fe_scr);
            // start listening to XKB event(s)
            if (!p->select_frontend_xkb_events()) {
                LOG(ERROR) << "Cannot select frontend XKB.";
                p->fe_init_has_err = true;
                return;
            }
            // listen to frontend's root window events
            p->select_root_events();
        }
    }
}

X11Frontend::~X11Frontend() {
    stop();
    //... so that associated object's destructors are properly called
    auto fe_conn = p->fe_conn;
    p = nullptr;
    disconnect_display(fe_conn);
}

bool X11Frontend::is_frontend_connected() {
    return p->is_connected;
}

bool X11Frontend::frontend_has_exts() {
    return p->fe_has_ext;
}

bool X11Frontend::frontend_init_error() {
    return p->fe_init_has_err;
}

bool X11Frontend::frontend_visual_unsupported() {
    return p->fe_visual_unsupported;
}

xcb_connection_t* X11Frontend::get_fconn() {
    return p->is_connected ? p->fe_conn : nullptr;
}

void X11Frontend::stop() {
    if (p->is_connected) {
        p->is_connected = false;
    }
}

bool X11Frontend::poll_event() {
    if (p->first_start) {
        p->first_start = false;
        p->first_start_routine();
    } else {
        // clear backing stores for backend's extended message datas
        p->bk_propmsg_store.clear();
        p->bk_cmdata_store.clear();
    }
    // anything outside event handlers that requires processing? (e.g. from user request)
    p->process_pre_events();
    // fire event handlers
    if (!p->eh->poll_and_handle_events()) {
        stop();
        return false;
    }
    return true;
}

void X11Frontend::hdlbe_configure_notify(xcb_window_t bwin, WindowGeomProperties& geometries) {
    Priv::w_map_t::iterator wit;
    if (check_and_get_mapped_value(p->bw_map, bwin, wit)) {
        auto& wdata = wit->second;

        // configure frontend window (to scaled values)
        uint32_t cfg_values[4];
        uint32_t* cfg_values_start = &cfg_values[2];
        uint16_t cfg_flags = 0;
        uint16_t scfactor = p->options.scale_factor;
        if (wdata->geometries.override_redirect) {
            // also configure position if override_redirect
            cfg_flags = XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y;
            cfg_values[0] = geometries.pos.first * scfactor;
            cfg_values[1] = geometries.pos.second * scfactor;
            cfg_values_start = cfg_values;
        }
        // always update size
        cfg_flags |= XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT;
        cfg_values[2] = geometries.size.first * scfactor;
        cfg_values[3] = geometries.size.second * scfactor;
        xcb_configure_window(p->fe_conn, wdata->fwin_id, cfg_flags, cfg_values_start);
        xcb_flush(p->fe_conn);

        // update internal data structure
        // we assume that window manager will always obey if the window is override-redirect
        if (wdata->geometries.override_redirect) {
            wdata->geometries.size = geometries.size;
            wdata->geometries.pos = geometries.pos;
            wdata->scaled_geom.size = {geometries.size.first * scfactor, geometries.size.second * scfactor};
            wdata->scaled_geom.pos = {geometries.pos.first * scfactor, geometries.pos.second * scfactor};
        }
        wdata->req_full_redraw = true;
    }
}

void X11Frontend::hdlbe_property_change(xcb_property_notify_event_t* cevt) {
    Priv::w_map_t::iterator it;
    if (check_and_get_mapped_value(p->bw_map, cevt->window, it)) {
        x11::PropData prop = x11::PropMgr::convert_from_event(p->be_conn, cevt);
        if (prop.target && p->propmgr->forward_property(prop)) {
            p->set_fwin_property(prop);
            xcb_flush(p->fe_conn);
        }
    }
}

void X11Frontend::hdlbe_client_message(xcb_client_message_event_t* cevt) {
    if (p->bw_map.count(cevt->window)) {
        auto cm = x11::CMMgr::convert_from_event(cevt);
        if (p->cmmgr->forward_cm(cm)) {
            // send the client_message (directly. The manager has taken care of all window/atom conversions)
            xcb_client_message_event_t send;
            memset(&send, 0, sizeof(xcb_client_message_event_t));
            send.response_type = XCB_CLIENT_MESSAGE;
            send.format = cm.format;
            send.window = cm.window;
            send.type = cm.msg_type;
            send.data = cm.data;
            xcb_send_event(
                p->fe_conn, cm.send_info.propagate, cm.send_info.destination, cm.send_info.event_mask, (char*)&send);
            xcb_flush(p->fe_conn);
        }
    }
}

void X11Frontend::hdlbe_focus_in(xcb_window_t be_wid) {
    Priv::w_map_t::iterator it;
    if (p->focused.first != be_wid && check_and_get_mapped_value(p->bw_map, be_wid, it)) {
        p->requested_focus = it->second->fwin_id;
        xcb_set_input_focus(p->fe_conn, XCB_INPUT_FOCUS_PARENT, it->second->fwin_id, XCB_CURRENT_TIME);
        p->hdlbe_helpers.cursor_helper->assign_to_window(p->requested_focus);
        xcb_flush(p->fe_conn);
    }
}

void X11Frontend::hdlbe_cursor_change(sharedds::CursorData cur) {
    p->hdlbe_helpers.cursor_helper->change_cursor(cur);
    p->hdlbe_helpers.cursor_helper->assign_to_window(p->focused.second);
}

void X11Frontend::hdlbe_shape_change(xcb_window_t be_wid, uint32_t rect_len, xcb_rectangle_t* rects) {
    Priv::w_map_t::iterator it;
    if (check_and_get_mapped_value(p->bw_map, be_wid, it)) {
        size_t sz = rect_len * sizeof(xcb_rectangle_t);
        it->second->shape_rects = {rect_len, make_unique_malloc((xcb_rectangle_t*)malloc(sz))};
        memcpy(it->second->shape_rects.second.get(), rects, sz);
        p->set_shape(it->second->fwin_id);
    }
}

bool X11Frontend::hdlbe_selection_request(xcb_window_t be_wid, SelectionInfo selinfo, vector<SelectionRequest>& reqs) {
    // no need to check for managed and mapped windows, because some clipboard client's windows are not mapped, hence
    // unmanaged.
    if (reqs.size()) {
        SelConverter::convert_request_atoms(p->be_conn, p->fe_conn, selinfo, reqs);
        return p->selmgr->request_selection(be_wid, selinfo, reqs);
    } else {
        return false;
    }
}

void X11Frontend::hdlbe_selection_reply(uint32_t requestor, SelectionInfo selinfo, vector<XProperty>& data) {
    SelConverter::convert_reply_atoms(p->be_conn, p->fe_conn, selinfo, data);
    p->selmgr->supply_data(p->eh->evt_pooler.get(), requestor, selinfo, data);
}

void X11Frontend::hdlbe_take_own_selection(xcb_atom_t bk_sel, bool mode) {
    auto fe_sel = translate_atom(p->be_conn, p->fe_conn, bk_sel);
    if (mode) {
        p->selmgr->acquire_selection(fe_sel);
    } else {
        p->selmgr->release_selection(fe_sel);
    }
}

vector<pair<xcb_window_t, xcb_window_t>> X11Frontend::get_managed_windows() {
    vector<pair<xcb_window_t, xcb_window_t>> ret;
    ret.reserve(p->bw_map.size());
    for (auto kvp : p->bw_map) {
        ret.push_back({kvp.first, kvp.second->fwin_id});
    }
    return ret;
}

sharedds::XPropertyValue* X11Frontend::get_prop_from_msg(int id) {
    if (id >= p->bk_propmsg_store.size()) {
        return nullptr;
    }
    return &p->bk_propmsg_store[id];
}

ClientMsg* X11Frontend::get_cm_data_from_msg(int id) {
    if (id >= p->bk_cmdata_store.size()) {
        return nullptr;
    }
    return &p->bk_cmdata_store[id];
}

pair<SelectionInfo, vector<XProperty>> X11Frontend::get_sel_reply_data_from_msg(xcb_window_t requestor) {
    map<xcb_window_t, queue<pair<SelectionInfo, vector<XProperty>>>>::iterator it;
    if (check_and_get_mapped_value(p->bk_selreply_store, requestor, it)) {
        if (it->second.size()) {
            auto ret = it->second.front();
            it->second.pop();
            // GC
            if (!it->second.size()) {
                p->bk_selreply_store.erase(requestor);
            }
            return ret;
        }
        // GC
        if (!it->second.size()) {
            p->bk_selreply_store.erase(requestor);
        }
    }
    return {};
}

pair<SelectionInfo, vector<SelectionRequest>> X11Frontend::get_sel_request_refdata_from_msg(xcb_window_t requestor) {
    map<xcb_window_t, pair<SelectionInfo, vector<SelectionRequest>>>::iterator it;
    if (check_and_get_mapped_value(p->bk_selrequest_store, requestor, it)) {
        auto ret = it->second;
        p->bk_selrequest_store.erase(requestor);
        return ret;
    }
    return {};
}

X11Frontend::EventHandlers::EventHandlers(Priv* p)
    : p{p}, focus_io_helper{p->focused, p->msg_queue, p->wid_trans.get(), p->fe_conn} {
    map<uint8_t, function<void(xcb_generic_event_t*, int, bool)>> event_handlers;
    event_handlers[0] = bind(&EventHandlers::handle_error, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_REPARENT_NOTIFY] =
        bind(&EventHandlers::handle_reparent_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_CONFIGURE_NOTIFY] =
        bind(&EventHandlers::handle_configure_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_EXPOSE] =
        bind(&EventHandlers::handle_expose, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_FOCUS_IN] =
        bind(&EventHandlers::handle_focus_notify, this, placeholders::_1, true, placeholders::_2, placeholders::_3);
    event_handlers[XCB_FOCUS_OUT] =
        bind(&EventHandlers::handle_focus_notify, this, placeholders::_1, false, placeholders::_2, placeholders::_3);
    event_handlers[XCB_MOTION_NOTIFY] =
        bind(&EventHandlers::handle_motion, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_KEY_PRESS] = bind(&EventHandlers::handle_keybutton, this, placeholders::_1,
        intermsg::frontendeventloop::KeyButtonType::KEY, true, placeholders::_2, placeholders::_3);
    event_handlers[XCB_KEY_RELEASE] = bind(&EventHandlers::handle_keybutton, this, placeholders::_1,
        intermsg::frontendeventloop::KeyButtonType::KEY, false, placeholders::_2, placeholders::_3);
    event_handlers[XCB_BUTTON_PRESS] = bind(&EventHandlers::handle_keybutton, this, placeholders::_1,
        intermsg::frontendeventloop::KeyButtonType::BUTTON, true, placeholders::_2, placeholders::_3);
    event_handlers[XCB_BUTTON_RELEASE] = bind(&EventHandlers::handle_keybutton, this, placeholders::_1,
        intermsg::frontendeventloop::KeyButtonType::BUTTON, false, placeholders::_2, placeholders::_3);
    event_handlers[XCB_CLIENT_MESSAGE] =
        bind(&EventHandlers::handle_client_message, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_DESTROY_NOTIFY] =
        bind(&EventHandlers::handle_destroy_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_PROPERTY_NOTIFY] =
        bind(&EventHandlers::handle_property_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_SELECTION_NOTIFY] =
        bind(&EventHandlers::handle_selection_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_SELECTION_REQUEST] =
        bind(&EventHandlers::handle_selection_request, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[p->xexts[&xcb_xkb_id].first_event] =
        bind(&EventHandlers::handle_xkb, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[p->xexts[&xcb_xfixes_id].first_event + XCB_XFIXES_SELECTION_NOTIFY] = bind(
        &EventHandlers::handle_xfixes_selection_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);

    evt_pooler = make_unique<EventPooler>(p->fe_conn, event_handlers);
}

bool X11Frontend::EventHandlers::poll_and_handle_events() {
    bool status = evt_pooler->poll_for_events();
    // run event helpers
    focus_io_helper.forward_be();
    p->hdlbe_helpers.cursor_helper->assign_to_window(p->focused.second);

    return status;
}

void X11Frontend::EventHandlers::handle_error(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_generic_error_t* cevt = (xcb_generic_error_t*)evt;
    // logging only
    LOG(INFO) << string_printf(
        "Error %d (%s) occured on Frontend X11 server.", cevt->error_code, xcb_event_get_error_label(cevt->error_code));
}

void X11Frontend::EventHandlers::handle_expose(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_expose_event_t* cevt = (xcb_expose_event_t*)evt;
    Priv::w_map_t::iterator wit;
    if (check_and_get_mapped_value(p->fw_map, cevt->window, wit)) {
        auto bwin_id = wit->second->bwin_id;
        uint16_t scfactor = p->options.scale_factor;
        auto& upd_data = p->wudc.data[bwin_id];
        upd_data.add_box(
            cevt->x / scfactor, cevt->y / scfactor, (cevt->width / scfactor) + 1, (cevt->height / scfactor) + 1);
        if (!upd_data.updated) {
            upd_data.updated = true;
            p->wudc.updated.insert(bwin_id);
        }
    }
}

void X11Frontend::EventHandlers::handle_reparent_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_reparent_notify_event_t* cevt = (xcb_reparent_notify_event_t*)evt;
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    Priv::w_map_t::iterator wit;
    if (check_and_get_mapped_value(p->fw_map, cevt->window, wit)) {
        wit->second->fparent_id = cevt->parent;
    }
}

void X11Frontend::EventHandlers::handle_configure_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    // WM non synth events: all if has root window as the parent, size only otherwise
    // WM synth events: all (we trust this!)
    xcb_configure_notify_event_t* cevt = (xcb_configure_notify_event_t*)evt;
    Priv::w_map_t::iterator wit;
    // Ignore this event on override-redirect windows. These windows are not supposed to be user movable / resizable.
    if (check_and_get_mapped_value(p->fw_map, cevt->window, wit) && !wit->second->geometries.override_redirect) {
        handlerhelpers::ConfigureNotifyHelper::fd_geom_t upd_geom{
            intermsg::frontendeventloop::GeomCfgUpdated::NONE, {}};
        // only trust position changes if WM sends it, or if parent is root
        if ((cevt->response_type & 0x80) || (wit->second->fparent_id == p->fe_scr->root)) {
            upd_geom.first |= intermsg::frontendeventloop::GeomCfgUpdated::POS;
            upd_geom.second.pos = {cevt->x, cevt->y};
        }
        // always trust size changes
        upd_geom.first |= intermsg::frontendeventloop::GeomCfgUpdated::SIZE;
        upd_geom.second.size = {cevt->width, cevt->height};
        // request full redraw on any possible size change
        wit->second->req_full_redraw = true;
        // save to helper
        configure_notify_helper.insert(cevt->window, upd_geom);

        // do the jobs at the end of this event stream
        if (is_last) {
            uint16_t scfactor = p->options.scale_factor;
            UniquePointerWrapper_free<xcb_generic_error_t> err;
            // repeat for each windows that received configure_notify in this event
            // stream
            for (auto& kvp : configure_notify_helper.get()) {
                if (!check_and_get_mapped_value(p->fw_map, kvp.first, wit)) {
                    continue;
                }
                auto& wdata = wit->second;

                // request size if not specified (Mutter WM ICCCM "bug")
                if (!enumval(kvp.second.first & intermsg::frontendeventloop::GeomCfgUpdated::POS)) {
                    bool req_sz_success = true;
                    auto w_abs_geom = make_unique_malloc(xcb_translate_coordinates_reply(
                        p->fe_conn, xcb_translate_coordinates(p->fe_conn, kvp.first, p->fe_root, 0, 0), &err.pop()));
                    if (err.get() || !w_abs_geom.get()) {
                        LOG(ERROR) << "Cannot query absolute position";
                        req_sz_success = false;
                    }
                    if (req_sz_success) {
                        kvp.second.first |= intermsg::frontendeventloop::GeomCfgUpdated::POS;
                        kvp.second.second.pos = {w_abs_geom->dst_x, w_abs_geom->dst_y};
                    }
                }
                // update internal data structure
                if (enumval(kvp.second.first & intermsg::frontendeventloop::GeomCfgUpdated::POS)) {
                    wdata->geometries.pos = {
                        kvp.second.second.pos.first / scfactor, kvp.second.second.pos.second / scfactor};
                    wdata->scaled_geom.pos = kvp.second.second.pos;
                }
                if (enumval(kvp.second.first & intermsg::frontendeventloop::GeomCfgUpdated::SIZE)) {
                    wdata->geometries.size = {
                        kvp.second.second.size.first / scfactor, kvp.second.second.size.second / scfactor};
                    wdata->scaled_geom.size = kvp.second.second.size;
                }
                // send to backend
                intermsg::frontendeventloop::message_store_t msg_store;
                auto msg = new (msg_store.data) intermsg::frontendeventloop::GeomCfgMessage();
                msg->back_window_id = wdata->bwin_id;
                msg->updated = kvp.second.first;
                msg->new_geoms = wdata->geometries;
                p->msg_queue.push(msg_store);
            }
            // end loop. don't forget!
            configure_notify_helper.clear();
        }
    }
}

void X11Frontend::EventHandlers::handle_focus_notify(xcb_generic_event_t* evt, bool in, int ord, bool is_last) {
    focus_io_helper.focus_changed = true;
}

void X11Frontend::EventHandlers::handle_motion(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_motion_notify_event_t* cevt = (xcb_motion_notify_event_t*)evt;
    Priv::w_map_t::iterator wit;
    if (check_and_get_mapped_value(p->fw_map, cevt->event, wit) /*&& wit->second->fwin_id == p->focused.second*/) {
        // send msg to backend
        intermsg::frontendeventloop::message_store_t msg_store;
        auto msg = new (msg_store.data) intermsg::frontendeventloop::MouseMotionMessage();
        msg->back_window_id = wit->second->bwin_id;
        msg->pressed = cevt->state ? 1 : 0;
        msg->x = cevt->event_x / p->options.scale_factor;
        msg->y = cevt->event_y / p->options.scale_factor;
        p->msg_queue.push(msg_store);
    }
}

void X11Frontend::EventHandlers::handle_keybutton(xcb_generic_event_t* evt,
    intermsg::frontendeventloop::KeyButtonType kb_type,
    bool pressed,
    int ord,
    bool is_last) {
    xcb_key_press_event_t* cevt = (xcb_key_press_event_t*)evt;

    intermsg::frontendeventloop::message_store_t msg_store;
    auto msg = new (msg_store.data) intermsg::frontendeventloop::KeyButtonMessage();
    msg->kb_type = kb_type;
    msg->pressed = pressed;
    msg->kb_code = cevt->detail;
    msg->state = cevt->state;
    p->msg_queue.push(msg_store);
}

void X11Frontend::EventHandlers::handle_client_message(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_client_message_event_t* cevt = (xcb_client_message_event_t*)evt;
    if (cevt->type == get_atom(p->fe_conn, false, "WM_PROTOCOLS") &&
        cevt->data.data32[0] == get_atom(p->fe_conn, false, "WM_DELETE_WINDOW")) {
        // close request by frontend's WM
        p->close_window(cevt->window);
    } else {
        auto cm = x11::CMMgr::convert_from_event(cevt);
        if (p->cmmgr->backward_cm(cm)) {
            intermsg::frontendeventloop::message_store_t msg_store;
            intermsg::frontendeventloop::CMMessage* msg = new (msg_store.data) intermsg::frontendeventloop::CMMessage();
            msg->cm_id = p->bk_cmdata_store.size();
            p->bk_cmdata_store.push_back(cm);
            p->msg_queue.push(msg_store);
        }
    }
}

void X11Frontend::EventHandlers::handle_destroy_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_destroy_notify_event_t* cevt = (xcb_destroy_notify_event_t*)evt;
    p->selmgr->destroy_requestor_data(cevt->window);
    p->close_window(cevt->window);
}

void X11Frontend::EventHandlers::handle_property_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_property_notify_event_t* cevt = (xcb_property_notify_event_t*)evt;
    x11::PropData data = x11::PropMgr::convert_from_event(p->fe_conn, cevt);
    if (data.target && (p->fw_map.count(cevt->window) || cevt->window == p->fe_root) &&
        p->propmgr->backward_property(data)) {
        p->push_be_property_change(data);
    }
}

void X11Frontend::EventHandlers::handle_selection_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_selection_notify_event_t* cevt = (xcb_selection_notify_event_t*)evt;
    auto sel_result = p->selmgr->parse_selection_notify(evt_pooler.get(), cevt);
    // translate atoms to backend and store it
    SelConverter::convert_reply_atoms(p->fe_conn, p->be_conn, get<1>(sel_result), get<2>(sel_result));
    p->bk_selreply_store[get<0>(sel_result)].push({get<1>(sel_result), get<2>(sel_result)});
    // send to backend
    intermsg::frontendeventloop::message_store_t msg_store;
    intermsg::frontendeventloop::SelReplyMessage* msg =
        new (msg_store.data) intermsg::frontendeventloop::SelReplyMessage();
    msg->requestor = get<0>(sel_result);
    p->msg_queue.push(msg_store);
}

void X11Frontend::EventHandlers::handle_selection_request(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_selection_request_event_t* cevt = (xcb_selection_request_event_t*)evt;
    // only process the one that asks our selection proxy window
    if (cevt->owner != p->selmgr->proxy_wid) {
        return;
    }
    auto selreqs = p->selmgr->handle_request(cevt);
    if (selreqs.second.size()) {
        // don't forget!
        SelConverter::convert_request_atoms(p->fe_conn, p->be_conn, selreqs.first, selreqs.second);
        // tell backend
        intermsg::frontendeventloop::message_store_t msg_store;
        intermsg::frontendeventloop::SelRequestMessage* msg =
            new (msg_store.data) intermsg::frontendeventloop::SelRequestMessage();
        msg->requestor = cevt->requestor;
        p->msg_queue.push(msg_store);
        // save
        p->bk_selrequest_store[cevt->requestor] = selreqs;
    }
}

void X11Frontend::EventHandlers::handle_xkb(xcb_generic_event_t* evt, int ord, bool is_last) {
    intermsg::frontendeventloop::message_store_t msg_store;
    intermsg::frontendeventloop::Message* msg = new (msg_store.data) intermsg::frontendeventloop::Message();
    XkbDescUpdateFlag have_desc_update = XkbDescUpdateFlag::NONE;
    switch (evt->pad0) {
        case XCB_XKB_NEW_KEYBOARD_NOTIFY: {
            auto cevt = (xcb_xkb_new_keyboard_notify_event_t*)evt;
            if (cevt->changed & XCB_XKB_NKN_DETAIL_KEYCODES) {
                have_desc_update = XkbDescUpdateFlag::ALL;
            }
            break;
        }
        case XCB_XKB_STATE_NOTIFY: {
            auto cevt = (xcb_xkb_state_notify_event_t*)evt;
            auto msg = new (msg_store.data) intermsg::frontendeventloop::XkbUpdateMessage();
            msg->updated.flags = XkbUpdateFlag::STATES;
            if (p->focused.first == 0 && p->focused.second == 0) {
                // update everything if not focused
                msg->updated.state_flags = XkbStateUpdateFlag::ALL;
            } else {
                // only update groups otherwise
                msg->updated.state_flags = XkbStateUpdateFlag::LATCHED_GROUP | XkbStateUpdateFlag::LOCKED_GROUP;
            }
            msg->state_evt = {
                cevt->changed, cevt->latchedMods, cevt->lockedMods, (uint8_t)cevt->latchedGroup, cevt->lockedGroup};
            break;
        }
        case XCB_XKB_CONTROLS_NOTIFY: {
            auto msg = new (msg_store.data) intermsg::frontendeventloop::XkbUpdateMessage();
            msg->updated.flags = XkbUpdateFlag::CONTROLS;
            msg->updated.ctrl_flags = XkbControlUpdateFlag::ALL;
            break;
        }
        case XCB_XKB_MAP_NOTIFY:
            have_desc_update |= XkbDescUpdateFlag::MAPS;
            break;
        case XCB_XKB_COMPAT_MAP_NOTIFY:
            have_desc_update |= XkbDescUpdateFlag::COMPAT_MAPS;
            break;
        case XCB_XKB_NAMES_NOTIFY:
            have_desc_update |= XkbDescUpdateFlag::NAMES;
            break;
    }
    if (enumval(have_desc_update)) {
        auto msg = new (msg_store.data) intermsg::frontendeventloop::XkbUpdateMessage();
        msg->updated.flags = XkbUpdateFlag::DESC;
        msg->updated.desc_flags = have_desc_update;
    }

    if (enumval(msg->type)) {
        p->msg_queue.push(msg_store);
    }
}

void X11Frontend::EventHandlers::handle_xfixes_selection_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    auto cevt = (xcb_xfixes_selection_notify_event_t*)evt;
    if (cevt->owner != p->selmgr->proxy_wid) {
        intermsg::frontendeventloop::message_store_t msg_store;
        intermsg::frontendeventloop::SelOwnMessage* msg =
            new (msg_store.data) intermsg::frontendeventloop::SelOwnMessage;
        msg->selection = translate_atom(p->fe_conn, p->be_conn, cevt->selection);
        msg->takeown = cevt->owner;
        p->msg_queue.push(msg_store);
    }
}

void X11Frontend::Priv::check_resolution_match(uint16_t new_scfactor) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto fe_geom = make_unique_malloc(xcb_get_geometry_reply(fe_conn, xcb_get_geometry(fe_conn, fe_root), &err.pop()));
    auto be_geom = make_unique_malloc(xcb_get_geometry_reply(be_conn, xcb_get_geometry(be_conn, be_root), &err.pop()));
    if (fe_geom.get() && be_geom.get()) {
        auto comparer = [&new_scfactor](int be_sz, int fe_sz) -> bool {
            int min = fe_sz / new_scfactor;
            int max = min + (fe_sz % new_scfactor ? 1 : 0);
            return be_sz >= min && be_sz <= max;
        };
        if (!comparer(be_geom->width, fe_geom->width) || !comparer(be_geom->height, fe_geom->height)) {
            fprintf(stderr,
                _("Backend's framebuffer size (%1$dx%2$d) is not optimal against frontend area of (%3$dx%4$d) using scale factor %5$d. Windows location, windows sizing and mouse inputs may not work properly."),
                be_geom->width, be_geom->height, fe_geom->width, fe_geom->height, new_scfactor);
            fputc('\n', stderr);
            LOG(WARNING) << string_printf(
                "Backend's framebuffer size (%dx%d) isn't optimal against frontend's size (%dx%d) with scale factor %d.",
                be_geom->width, be_geom->height, fe_geom->width, fe_geom->height, new_scfactor);
        }
    } else {
        fputs(_("Unable to compare root window size."), stderr);
        fputc('\n', stderr);
        LOG(ERROR) << "Cannot retrieve root window geometries.";
    }
}

bool X11Frontend::Priv::check_frontend_has_visual_supported() {
    for (const auto& visual : fe_visualmap->available_visuals()) {
        if (is_visual_supported(visual)) {
            // at least ONE visual is supported
            return true;
        }
    }
    return false;
}

xcb_window_t X11Frontend::Priv::create_frontend_window(WindowGeomProperties& geometries,
    VisualInfo be_visual,
    VisualInfo& out_fe_visual) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    xcb_window_t ret = xcb_generate_id(fe_conn);

    // find the suitable visual for use with frontend window. automatically select other compatible depth if the
    // requested depth is not supported. depth must be either 32 or 24.
    if ((!fe_supported_pixmap_depth.count(be_visual.depth)) ||
        (!(out_fe_visual = fe_visualmap->get_visual_type_by_depth(be_visual.depth)).id) ||
        (!fe_render_formats.count(be_visual.depth))) {
        // 24 or 32
        uint8_t alt_depth = be_visual.depth ^ 56;
        LOG(INFO) << string_printf(
            "Backend window visual depth %d is not supported by frontend. Reverting to depth %d.", be_visual.depth,
            alt_depth);
        out_fe_visual = fe_visualmap->get_visual_type_by_depth(alt_depth);
        // alt depth MUST be supported, otherwise our selection code (both window management and frontend) must be buggy
        if (!(out_fe_visual.id && fe_supported_pixmap_depth.count(alt_depth) && fe_render_formats.count(alt_depth))) {
            LOG(FATAL) << "Unsupported depth for frontend window creation.";
        }
    }
    xcb_colormap_t colormap = fe_visualmap->get_default_visual_colormap(out_fe_visual.id);
    uint32_t events = XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_FOCUS_CHANGE |
        XCB_EVENT_MASK_BUTTON_MOTION | XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE |
        XCB_EVENT_MASK_POINTER_MOTION | XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE |
        XCB_EVENT_MASK_PROPERTY_CHANGE;
    uint32_t cw_flag = XCB_CW_BACK_PIXEL | XCB_CW_BORDER_PIXEL | XCB_CW_BIT_GRAVITY | XCB_CW_OVERRIDE_REDIRECT |
        XCB_CW_EVENT_MASK | XCB_CW_COLORMAP;
    uint32_t cw_vals[] = {0, 0, XCB_GRAVITY_NORTH_WEST, geometries.override_redirect, events, colormap};

    err.pop() = xcb_request_check(
        fe_conn, xcb_create_window_checked(fe_conn, out_fe_visual.depth, ret, fe_scr->root, geometries.pos.first,
                     geometries.pos.second, geometries.size.first, geometries.size.second, 0,
                     XCB_WINDOW_CLASS_INPUT_OUTPUT, out_fe_visual.id, cw_flag, cw_vals));
    if (err.get()) {
        LOG(ERROR) << "Cannot create window: " << xcb_event_get_error_label(err.get()->error_code);
    }

    // hook WM_DELETE_WINDOW from X11 protocol. We want window to tell us when closed instead of closing itself
    // altogether.
    xcb_atom_t atom_del_window = get_atom(fe_conn, false, "WM_DELETE_WINDOW");
    xcb_atom_t atom_wm_protoc = get_atom(fe_conn, false, "WM_PROTOCOLS");
    xcb_change_property(fe_conn, XCB_PROP_MODE_REPLACE, ret, atom_wm_protoc, XCB_ATOM_ATOM, 32, 1, &atom_del_window);

    // it's the caller's responsibility to sync
    return ret;
}

void X11Frontend::Priv::redraw_window(FWindowData& data, sharedds::WindowUpdateData& bbox) {
    constexpr uint32_t plane_mask = ~0L;
    if (data.pause_draw) {
        return;
    }
    UniquePointerWrapper_free<xcb_generic_error_t> err;

    // geom_pair_t offset;
    // geom_pair_t size;
    xcb_rectangle_t upd_area;

    // calculate area to redraw based on parameters (damage, full_redraw)
    bool force_resize_update = false;
    if (data.req_full_redraw) {
        upd_area.x = upd_area.y = 0;
        upd_area.width = data.geometries.size.first;
        upd_area.height = data.geometries.size.second;
        // Something important is happening. To be sure, let's refresh the xrender backing pixmap :-)
        force_resize_update = true;
        data.req_full_redraw = false;
    } else {
        upd_area.x = max(0, bbox.x1 - 1);
        upd_area.y = max(0, bbox.y1 - 1);
        upd_area.width = min(bbox.x2 + 1 - upd_area.x, data.geometries.size.first - upd_area.x);
        upd_area.height = min(bbox.y2 + 1 - upd_area.y, data.geometries.size.second - upd_area.y);
    }

    renderer->update_window(data.fwin_id, force_resize_update, upd_area);
}

void X11Frontend::Priv::close_window(xcb_window_t fwid) {
    w_map_t::iterator it;
    if (check_and_get_mapped_value(fw_map, fwid, it)) {
        // send "close" message to backend. let backend reports back of actual window closing later on.
        intermsg::frontendeventloop::message_store_t msg_store;
        intermsg::frontendeventloop::WindowCloseMessage* msg =
            new (msg_store.data) intermsg::frontendeventloop::WindowCloseMessage();
        msg->back_window_id = it->second->bwin_id;
        msg_queue.push(msg_store);
    } else {
        LOG(WARNING) << "Frontend attempts to close non-managed top level window.";
    }
}

void X11Frontend::Priv::set_fwin_property(x11::PropData& prop) {
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        int data_count = prop.prop_val.data_len / (prop.prop_val.format_bpp / 8);
        xcb_change_property(fe_conn, XCB_PROP_MODE_REPLACE, prop.target, prop.property, prop.prop_val.prop_type,
            prop.prop_val.format_bpp, data_count, prop.prop_val.data_ptr.get());
    } else {
        xcb_delete_property(fe_conn, prop.target, prop.property);
    }
}

bool X11Frontend::Priv::select_frontend_xkb_events() {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    uint16_t enabled_events = XCB_XKB_EVENT_TYPE_NEW_KEYBOARD_NOTIFY | XCB_XKB_EVENT_TYPE_STATE_NOTIFY |
        XCB_XKB_EVENT_TYPE_CONTROLS_NOTIFY | XCB_XKB_EVENT_TYPE_MAP_NOTIFY | XCB_XKB_EVENT_TYPE_NAMES_NOTIFY |
        XCB_XKB_EVENT_TYPE_COMPAT_MAP_NOTIFY;
    err.pop() = xcb_request_check(fe_conn, xcb_xkb_select_events_aux_checked(fe_conn, XCB_XKB_ID_USE_CORE_KBD,
                                               enabled_events, 0, enabled_events, 0, 0, nullptr));
    return !err.get();
}

void X11Frontend::Priv::select_root_events() {
    uint32_t events = XCB_EVENT_MASK_PROPERTY_CHANGE;
    xcb_change_window_attributes(fe_conn, fe_root, XCB_CW_EVENT_MASK, &events);
    xcb_flush(fe_conn);
}

void X11Frontend::Priv::first_start_routine() {
    // initial properties forwarding from root window
    backward_all_root_properties();
    // take own backend's selection if frontend's selection owner is not none
    check_and_takeown_selections();
}

void X11Frontend::Priv::backward_all_root_properties() {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto props =
        make_unique_malloc(xcb_list_properties_reply(fe_conn, xcb_list_properties(fe_conn, fe_root), &err.pop()));
    if (err.get()) {
        LOG(ERROR) << string_printf("Cannot retrieve properties list for frontend's root (%s)",
            xcb_event_get_error_label(err.get()->error_code));
        return;
    }
    // request
    auto count = xcb_list_properties_atoms_length(props.get());
    auto atoms_arr = xcb_list_properties_atoms(props.get());
    vector<xcb_get_property_cookie_t> requests(count);
    for (int i = 0; i < count; ++i) {
        requests[i] =
            xcb_get_property(fe_conn, 0, fe_root, atoms_arr[i], XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE);
    }
    // reply
    x11::PropData to_bw;
    for (int i = 0; i < count; ++i) {
        auto prop_rep = make_unique_malloc(xcb_get_property_reply(fe_conn, requests[i], &err.pop()));
        if (err.get() || prop_rep.get() == nullptr) {
            LOG(ERROR) << string_printf("Cannot retrieve property '%s' for frontend's root (%s)",
                get_atom_name(fe_conn, atoms_arr[i]).c_str(), xcb_event_get_error_label(err.get()->error_code));
        }
        // first thing first: copas
        to_bw.target = fe_root;
        to_bw.property = atoms_arr[i];
        to_bw.state = XCB_PROPERTY_NEW_VALUE;
        to_bw.prop_val.prop_type = prop_rep->type;
        to_bw.prop_val.format_bpp = prop_rep->format;
        to_bw.prop_val.data_len = xcb_get_property_value_length(prop_rep.get());
        to_bw.prop_val.data_ptr = make_shared_malloc(malloc(to_bw.prop_val.data_len));
        memcpy(to_bw.prop_val.data_ptr.get(), xcb_get_property_value(prop_rep.get()), to_bw.prop_val.data_len);
        // let prop manager decide and alter
        if (propmgr->backward_property(to_bw)) {
            push_be_property_change(to_bw);
        }
    }
}

void X11Frontend::Priv::check_and_takeown_selections() {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto sels = selmgr->managed_selections();
    intermsg::frontendeventloop::message_store_t msg_store;
    intermsg::frontendeventloop::SelOwnMessage* msg = new (msg_store.data) intermsg::frontendeventloop::SelOwnMessage();
    msg->takeown = true;
    for (auto sel : sels) {
        auto selowner = make_unique_malloc(
            xcb_get_selection_owner_reply(fe_conn, xcb_get_selection_owner(fe_conn, sel), &err.pop()));
        if (!err.get() && selowner->owner) {
            msg->selection = translate_atom(fe_conn, be_conn, sel);
            msg_queue.push(msg_store);
        }
    }
}

void X11Frontend::Priv::set_shape(xcb_window_t fwid) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;

    // forward from stored shape data
    w_map_t::iterator it;
    if (check_and_get_mapped_value(fw_map, fwid, it) && it->second->shape_rects.first) {
        // rescale if needed
        if (options.scale_factor > 1) {
            // make a copy buffer
            auto scaled_rects =
                make_unique_malloc((xcb_rectangle_t*)malloc(it->second->shape_rects.first * sizeof(xcb_rectangle_t)));

            // hopefully, our compiler vectorizes this multiplying code!
            int16_t* rect_components_dst = (int16_t*)scaled_rects.get();
            int16_t* rect_components_src = (int16_t*)it->second->shape_rects.second.get();
            const int scfactor = options.scale_factor;
            // count of rectangle components (x,y,width,height, all in 16 bit integer)
            const int rect_count_sint = it->second->shape_rects.first * 4;
            for (int i = 0; i < rect_count_sint; ++i) {
                rect_components_dst[i] = rect_components_src[i] * scfactor;
            }
            // apply!
            xcb_shape_rectangles(fe_conn, XCB_SHAPE_SO_SET, XCB_SHAPE_SK_BOUNDING, 0, fwid, 0, 0,
                it->second->shape_rects.first, scaled_rects.get());
            xcb_flush(fe_conn);
        } else {
            xcb_shape_rectangles(fe_conn, XCB_SHAPE_SO_SET, XCB_SHAPE_SK_BOUNDING, 0, fwid, 0, 0,
                it->second->shape_rects.first, it->second->shape_rects.second.get());
            xcb_flush(fe_conn);
        }
    }
}

void X11Frontend::Priv::process_pre_events() {
    // check scale change request
    if (new_scfactor_req != options.scale_factor) {
        // warn user if resolution mismatch
        check_resolution_match(new_scfactor_req);
        // do the rescaling
        rescale(new_scfactor_req.load(memory_order_relaxed));
        // resend position to backend to match the new scale factor (any size changes will be handled by
        // configure_notify, which is triggered by reconfiguration in 'rescale' method)
        for (auto kvp : bw_map) {
            intermsg::frontendeventloop::message_store_t msg_store;
            auto msg = new (msg_store.data) intermsg::frontendeventloop::GeomCfgMessage();
            msg->updated = intermsg::frontendeventloop::GeomCfgUpdated::POS;
            msg->back_window_id = kvp.first;
            // corresponds to backend's pos
            msg->new_geoms = kvp.second->geometries;
            msg_queue.push(msg_store);
        }
    }
}

std::vector<frontend::x11::PropData> X11Frontend::Priv::generate_initial_properties(xcb_window_t bwin_id) {
    w_map_t::iterator it;
    if (check_and_get_mapped_value(bw_map, bwin_id, it)) {
        // WM_NORMAL_HINTS
        x11::PropData p_normhints;
        memset(&p_normhints, 0, sizeof(x11::PropData));
        p_normhints.target = bwin_id;
        p_normhints.property = get_atom(be_conn, false, "WM_NORMAL_HINTS");
        p_normhints.state = XCB_PROPERTY_NEW_VALUE;
        p_normhints.prop_val.format_bpp = 32;
        p_normhints.prop_val.data_len = 72;
        p_normhints.prop_val.prop_type = get_atom(be_conn, false, "WM_SIZE_HINTS");
        p_normhints.prop_val.data_ptr = make_shared_malloc(malloc(p_normhints.prop_val.data_len));
        xcb_size_hints_t* szhints = (xcb_size_hints_t*)p_normhints.prop_val.data_ptr.get();
        szhints->flags = XCB_ICCCM_SIZE_HINT_P_RESIZE_INC;
        szhints->width_inc = szhints->height_inc = 1;

        return {p_normhints};
    } else {
        return {};
    }
}

void X11Frontend::Priv::forward_premap_properties(xcb_window_t bwin_id) {
    w_map_t::iterator it;
    if (check_and_get_mapped_value(bw_map, bwin_id, it)) {
        //_NET_WM_STATE
        auto prop = x11::PropMgr::get_from_window(be_conn, bwin_id, get_atom(be_conn, false, "_NET_WM_STATE"));
        if (prop.target) {
            prop.target = it->second->fwin_id;
            prop.property = get_atom(fe_conn, false, "_NET_WM_STATE");
            prop.prop_val.prop_type = XCB_ATOM_ATOM;
            xcb_atom_t* states = (xcb_atom_t*)prop.prop_val.data_ptr.get();
            for (int i = 0; i < prop.prop_val.data_len / 4; ++i) {
                states[i] = get_atom(fe_conn, false, get_atom_name(be_conn, states[i]));
            }
            set_fwin_property(prop);
        } else {
            LOG(ERROR) << "Cannot forward premap properties.";
        }
    }
}

void X11Frontend::Priv::push_be_property_change(x11::PropData& data) {
    intermsg::frontendeventloop::message_store_t msg_store;
    intermsg::frontendeventloop::PropertyChangeMessage* msg =
        new (msg_store.data) intermsg::frontendeventloop::PropertyChangeMessage();
    msg->target = data.target;
    msg->atom = data.property;
    msg->state = data.state;
    // extended data
    if (msg->state == XCB_PROPERTY_NEW_VALUE) {
        // prop_id (index) == it's index in array
        msg->prop_id = bk_propmsg_store.size();
        bk_propmsg_store.push_back(data.prop_val);
    }
    // send the message to backend
    msg_queue.push(msg_store);
}

xcb_window_t X11Frontend::Priv::X11FWIDTrans::get_bw_id(xcb_window_t fw_id) {
    w_map_t::iterator it;
    if (check_and_get_mapped_value(p->fw_map, fw_id, it)) {
        return it->second->bwin_id;
    }
    return 0;
}

xcb_window_t X11Frontend::Priv::X11FWIDTrans::get_fw_id(xcb_window_t bw_id) {
    w_map_t::iterator it;
    if (check_and_get_mapped_value(p->bw_map, bw_id, it)) {
        return it->second->fwin_id;
    }
    return 0;
}

xcb_window_t X11Frontend::Priv::X11FWIDTrans::get_fe_root_id() {
    return p->fe_root;
}

xcb_window_t X11Frontend::Priv::X11FWIDTrans::get_be_root_id() {
    return p->be_root;
}

void X11Frontend::new_window(xcb_window_t back_window_id,
    WindowGeomProperties& geometries,
    vector<XProperty>& properties,
    pair<uint32_t, xcb_rectangle_t*> shape_data,
    VisualInfo be_visualinfo) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto fwdata = make_shared<Priv::FWindowData>();
    fwdata->bwin_id = back_window_id;
    // window size
    fwdata->scaled_geom = fwdata->geometries = geometries;
    fwdata->scaled_geom.pos = {
        geometries.pos.first * p->options.scale_factor, geometries.pos.second * p->options.scale_factor};
    fwdata->scaled_geom.size = {
        geometries.size.first * p->options.scale_factor, geometries.size.second * p->options.scale_factor};
    // window and renderer data creation
    VisualInfo fe_visualinfo;
    fwdata->fwin_id = p->create_frontend_window(fwdata->scaled_geom, be_visualinfo, fe_visualinfo);
    fwdata->req_full_redraw = true;
    p->renderer->new_window(
        fwdata->fwin_id, fwdata->bwin_id, fwdata->geometries.size, be_visualinfo.depth, fe_visualinfo.depth);
    // IMPORTANT! add to map
    p->bw_map[back_window_id] = fwdata;
    p->fw_map[fwdata->fwin_id] = fwdata;

    // initial value properties
    for (auto& prop : p->generate_initial_properties(back_window_id)) {
        if (p->propmgr->forward_property(prop)) {
            p->set_fwin_property(prop);
        }
    }
    // forwardable properties
    for (auto const& prop : properties) {
        x11::PropData prop_data = x11::PropMgr::convert_from_xproperty(back_window_id, prop);
        if (p->propmgr->forward_property(prop_data)) {
            p->set_fwin_property(prop_data);
        }
    }
    // additional properties that can't be forwarded, but must be set on premap.
    p->forward_premap_properties(back_window_id);
    // show the window
    fwdata->withdrawn = false;
    xcb_map_window(p->fe_conn, fwdata->fwin_id);
    xcb_flush(p->fe_conn);
    // shape (if exist)
    if (shape_data.first && shape_data.second) {
        // save
        size_t datasz = shape_data.first * sizeof(xcb_rectangle_t);
        fwdata->shape_rects = {shape_data.first, make_unique_malloc((xcb_rectangle_t*)malloc(datasz))};
        memcpy(fwdata->shape_rects.second.get(), shape_data.second, datasz);
        // scale and set to the window
        p->set_shape(fwdata->fwin_id);
    }
}

void X11Frontend::delete_window(xcb_window_t back_window_id) {
    if (!p->bw_map.count(back_window_id)) {
        LOG(INFO) << "Backend attempts to remove an unmanaged window.";
        return;
    }

    // request backend to depress all keyboard if the closed window is the one who is at the front
    if (p->focused.first == back_window_id) {
        intermsg::frontendeventloop::message_store_t msg_store;
        intermsg::frontendeventloop::DepressAllRequestMessage* msg =
            new (msg_store.data) intermsg::frontendeventloop::DepressAllRequestMessage();
        p->msg_queue.push(msg_store);
    }

    auto& fwdata = p->bw_map[back_window_id];

    // remove data from helpers
    p->renderer->delete_window(fwdata->fwin_id);
    p->propmgr->delete_frontend_window(fwdata->fwin_id);

    // remove the actual window from frontend
    xcb_destroy_window(p->fe_conn, fwdata->fwin_id);
    xcb_flush(p->fe_conn);

    // IMPORTANT! remove from map
    p->bw_map.erase(back_window_id);
    p->fw_map.erase(fwdata->fwin_id);
}

void X11Frontend::unmap_window(xcb_window_t back_window_id) {
    Priv::w_map_t::iterator it;
    if (check_and_get_mapped_value(p->bw_map, back_window_id, it)) {
        it->second->withdrawn = true;
        xcb_unmap_window(p->fe_conn, it->second->fwin_id);
        xcb_flush(p->fe_conn);
    }
}

void X11Frontend::map_window(xcb_window_t back_window_id, sharedds::WindowGeomProperties& geometries) {
    Priv::w_map_t::iterator it;
    if (check_and_get_mapped_value(p->bw_map, back_window_id, it) && it->second->withdrawn) {
        it->second->withdrawn = false;
        it->second->req_full_redraw = true;
        p->forward_premap_properties(back_window_id);
        // let's configure_notify update the size and pos
        uint32_t or_state = (it->second->geometries.override_redirect = geometries.override_redirect);
        xcb_change_window_attributes(p->fe_conn, it->second->fwin_id, XCB_CW_OVERRIDE_REDIRECT, &or_state);
        uint32_t cw_flag =
            XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT;
        uint16_t scfactor = p->options.scale_factor;
        uint32_t cw_vals[] = {(uint32_t)geometries.pos.first * scfactor, (uint32_t)geometries.pos.second * scfactor,
            (uint32_t)geometries.size.first * scfactor, (uint32_t)geometries.size.second * scfactor};
        xcb_configure_window(p->fe_conn, it->second->fwin_id, cw_flag, cw_vals);
        xcb_map_window(p->fe_conn, it->second->fwin_id);
        xcb_flush(p->fe_conn);
    }
}

void X11Frontend::req_refresh_all_windows() {
    for (auto upd_bwin : p->wudc.updated) {
        auto& wupd_data = p->wudc.data[upd_bwin];
        p->redraw_window(*p->bw_map[upd_bwin].get(), wupd_data);
        wupd_data.reset();
    }
    p->wudc.updated.clear();
}

void X11Frontend::req_rescale(uint16_t scfactor) {
    p->new_scfactor_req = scfactor;
}

void X11Frontend::Priv::rescale(uint16_t scfactor) {
    options.scale_factor = scfactor;
    // update renderer data
    renderer->rescale(scfactor);
    // update scaled properties
    for (auto re_fwd_prop : propmgr->set_fe_scaling_factor(scfactor)) {
        if (propmgr->forward_property(re_fwd_prop)) {
            set_fwin_property(re_fwd_prop);
        }
    }
    xcb_flush(fe_conn);

    for (auto kvp : fw_map) {
        // mapped windows only
        if (!kvp.second->withdrawn) {
            // reconfigure windows
            uint32_t vmask = XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT;
            uint32_t vvals[] = {(uint32_t)(kvp.second->geometries.size.first * scfactor),
                (uint32_t)(kvp.second->geometries.size.second * scfactor)};
            xcb_configure_window(fe_conn, kvp.second->fwin_id, vmask, vvals);
            // request full redraw
            kvp.second->req_full_redraw = true;
            wudc.updated.insert(kvp.second->bwin_id);
            // default area. it will eventually ignored anyway, because of full_redraw.
            wudc.data[kvp.second->bwin_id].add_box(0, 0, 1, 1);
            // update unscaled internal data structure to follow old scaled data. configure_notify will update the data
            // structure to the new one.
            kvp.second->scaled_geom.size = {vvals[0], vvals[1]};
            kvp.second->geometries.size = {
                kvp.second->scaled_geom.size.first / scfactor, kvp.second->scaled_geom.size.second / scfactor};
            kvp.second->geometries.pos = {
                kvp.second->scaled_geom.pos.first / scfactor, kvp.second->scaled_geom.pos.second / scfactor};
        }
        // update shape
        if (kvp.second->shape_rects.first) {
            set_shape(kvp.first);
        }
    }
    xcb_flush(fe_conn);
}
