#include "frontend/x11/renderer.h"
#include "common/xraii/gc.h"
#include "common/xraii/pixmap.h"
#include "common/xraii/shm.h"
#include "common/xraii/xrenderpicture.h"
#include "external/pxdip.h"
#include "shared/bitwiseenumclass.h"
#include "shared/i18n.h"
#include "shared/memory.h"
#include "shared/util.h"
#include <algorithm>
#include <glog/logging.h>
#include <map>
#include <sys/shm.h>
#include <xcb/render.h>
#include <xcb/shm.h>
#include <xcb/xcb_image.h>
#include <xcb/xcb_renderutil.h>
#include <xcb/xcb_util.h>

using namespace std;
using namespace px;
using namespace px::memory;
using namespace px::util;
using namespace pxwm;
using namespace pxwm::common;
using namespace pxwm::frontend::x11;

typedef pair<uint16_t, uint16_t> sz_pair_t;

class Renderer::Priv {
public:
    struct WindowData;

    bool init_error = false;
    string display_name;
    xcb_connection_t* fe_conn;
    xcb_connection_t* be_conn;
    xcb_screen_t* fe_scr;
    common::x11::VisualMap* be_visualmap;
    common::x11::VisualMap* fe_visualmap;
    unordered_set<uint8_t> be_supported_depth;
    unordered_set<uint8_t> fe_supported_depth;
    states::ScalingAlgorithm scale_algorithm;
    states::SHMMode usercfg_max_shm;
    uint16_t scale_factor;
    map<xcb_window_t, WindowData> wdata;
    // global xrender data
    struct {
        xcb_render_transform_t src_transform;
        map<uint8_t, xcb_render_pictforminfo_t> formats;
    } xrender_data;
    states::SHMMode max_shm_mode;
    // one pxdip instance for one renderer thread
    void* pxdip_instance;

    Priv();
    ~Priv();

    static states::SHMMode get_shm_mode(xcb_connection_t* conn, unordered_set<uint8_t> supported_pixmap_depth);
    static unordered_set<uint8_t> get_supported_pixmap_depth(common::x11::VisualMap* visualmap);
    states::SHMMode get_max_shm_mode();

    void update_xrender_transform();
    void resize_xrender_pixmap(WindowData& data, bool force);
    static void area_copy(uint32_t* __restrict__ src,
        uint32_t* __restrict__ dst,
        uint16_t src_w,
        uint16_t src_h,
        uint16_t dst_x,
        uint16_t dst_y,
        uint16_t dst_w,
        uint16_t dst_h);
};

struct Renderer::Priv::WindowData {
    xcb_window_t fwid, bwid;
    // original geometry of the window (e.g. unscaled geometry)
    sz_pair_t size;
    states::SHMMode shm_mode;
    uint8_t fe_depth;
    uint8_t be_depth;
    unique_ptr<xraii::GC> fe_gc;
    struct {
        // for "get_image" and "put_image" purposes (e.g. partial/per area). these 2 SHM are "connected".
        unique_ptr<xraii::SHM> be_shm_z_img;
        unique_ptr<xraii::SHM> fe_shm_z_img;
        // for whole window refresh when using ::PIXMAP mode
        unique_ptr<xraii::SHM> be_shm_z_whole;
        // order matters. Pixmap must be destroyed first before SHM.
        unique_ptr<xraii::SHM> fe_shm_pxm_rp_src;
        unique_ptr<xraii::Pixmap> fe_pxm_rp_src;
        // for use as custom scaler's destination. contains the whole scaled image of the window.
        unique_ptr<xraii::SHM> cst_shm_dst;
    } shm_data;
    struct {
        sz_pair_t rp_size{0, 0};
        unique_ptr<xraii::XRenderPicture> rp_src;
        xraii::Pixmap* fe_pxm_rp_src;
        // dst is backed directly by frontend window
        unique_ptr<xraii::XRenderPicture> rp_dst;
        xcb_render_pictforminfo_t format;
    } xrender_data;
    unique_ptr<xraii::Pixmap> fe_regular_rp_src;
    // for non shared memory codepath
    unique_ptr<uint32_t[]> cst_src_buff;
    unique_ptr<uint32_t[]> cst_dst_buff;
};

Renderer::Renderer(string display_name,
    xcb_connection_t* be_conn,
    xcb_connection_t* fe_conn,
    xcb_screen_t* fe_scr,
    common::x11::VisualMap* be_visualmap,
    common::x11::VisualMap* fe_visualmap,
    map<uint8_t, xcb_render_pictforminfo_t> render_formats,
    states::BkFrontEndOptions options) {
    p = make_unique<Priv>();
    p->display_name = display_name;
    p->fe_conn = fe_conn;
    p->be_conn = be_conn;
    p->fe_scr = fe_scr;
    p->be_visualmap = be_visualmap;
    p->fe_visualmap = fe_visualmap;
    p->be_supported_depth = Priv::get_supported_pixmap_depth(p->be_visualmap);
    p->fe_supported_depth = Priv::get_supported_pixmap_depth(p->fe_visualmap);
    p->xrender_data.formats = render_formats;
    p->scale_algorithm = options.scale_algorithm;
    p->scale_factor = options.scale_factor;
    p->usercfg_max_shm = options.max_shm_mode;
    p->max_shm_mode = p->get_max_shm_mode();
    p->update_xrender_transform();
}

Renderer::~Renderer() {}

bool Renderer::init_has_error() {
    return p->init_error;
}

void Renderer::new_window(xcb_window_t fwid,
    xcb_window_t bwid,
    std::pair<uint16_t, uint16_t> size,
    uint8_t be_depth,
    uint8_t fe_depth) {
    // reset existing windows data if already exists
    if (p->wdata.count(fwid)) {
        delete_window(fwid);
    }
    auto& data = p->wdata[fwid];
    data.fwid = fwid;
    data.bwid = bwid;
    data.size = size;
    data.fe_depth = fe_depth;
    data.be_depth = be_depth;

    if ((!p->be_supported_depth.count(be_depth)) || (!p->fe_supported_depth.count(fe_depth))) {
        data.shm_mode = states::SHMMode::NONE;
    } else if (be_depth != fe_depth) {
        data.shm_mode = states::SHMMode::IMAGE;
    } else if (be_depth == fe_depth) {
        data.shm_mode = states::SHMMode::PIXMAP;
    } else {
        data.shm_mode = states::SHMMode::NONE;
    }
    data.shm_mode = min(p->max_shm_mode, data.shm_mode);

    data.fe_gc = make_unique<xraii::GC>(p->fe_conn, fwid, 0, nullptr);
    // XRender pictures
    data.xrender_data.format = p->xrender_data.formats[fe_depth];
    data.xrender_data.rp_dst =
        make_unique<xraii::XRenderPicture>(p->fe_conn, fwid, data.xrender_data.format.id, 0, nullptr);
    p->resize_xrender_pixmap(data, true);
}

void Renderer::delete_window(xcb_window_t fwid) {
    // all X resources inside this structure are RAII-ed, so no worries about freeing them from server!
    p->wdata.erase(fwid);
}

void Renderer::update_window(xcb_window_t fwid, bool size_changed, xcb_rectangle_t area) {
    map<xcb_window_t, Priv::WindowData>::iterator it;
    if (!check_and_get_mapped_value(p->wdata, fwid, it)) {
        return;
    }
    auto& data = it->second;
    // plane mask for all channels
    constexpr uint32_t plane_mask = ~0L;
    UniquePointerWrapper_free<xcb_generic_error_t> err;

    if (size_changed) {
        data.size = {area.width, area.height};
    }
    // resize output if necessary
    p->resize_xrender_pixmap(data, size_changed);

    bool use_custom_scaler;
    switch (p->scale_algorithm) {
        case states::ScalingAlgorithm::NEAREST:
        case states::ScalingAlgorithm::BILINEAR:
            use_custom_scaler = false;
            break;
        default:
            // no need to invoke custom scaler if scale_factor is 1
            use_custom_scaler = p->scale_factor > 1;
            break;
    }
    uint16_t scfactor = p->scale_factor;

    // Opaquify frontend if backend window is 24 bit, but the frontend window is 32 bit. Simply call this function after
    // image fetch, this will automatically determine whether to opaquify or not.
    auto dip_opaquify = [&](uint32_t* data, uint32_t px_count) {
        if (it->second.be_depth == 24 && it->second.fe_depth == 32) {
            pxdip_setmask(p->pxdip_instance, px_count, data, 0xFF000000);
        }
    };

    // SHM get image cannot retrieve area of the windows outside root's area (it'll error BadMatch), so ...
    auto shm_get_image_failover = [this, &err, &plane_mask](xcb_connection_t* conn, xcb_drawable_t drawable, int16_t x,
        int16_t y, uint16_t width, uint16_t height, uint16_t whole_width, uint16_t whole_height,
        const xcb_shm_segment_info_t& shminfo_part, const xcb_shm_segment_info_t& shminfo_whole) {
        // if z_whole is set, and {width,height} == {whole_width,whole_height}, directly draw to z_whole
        bool full_redraw = (width == whole_width) && (height == whole_height);
        auto img_shm_s = make_unique_malloc(xcb_shm_get_image_reply(conn,
            xcb_shm_get_image(conn, drawable, x, y, width, height, plane_mask, XCB_IMAGE_FORMAT_Z_PIXMAP,
                (full_redraw && shminfo_whole.shmseg ? shminfo_whole.shmseg : shminfo_part.shmseg), 0),
            &err.pop()));
        if (err.get()) {
            if (err.get()->error_code == XCB_MATCH) {
                // BadMatch error means that we have to use core get_image
                auto img_f = make_unique_malloc(xcb_get_image_reply(conn,
                    xcb_get_image(conn, XCB_IMAGE_FORMAT_Z_PIXMAP, drawable, x, y, width, height, plane_mask),
                    &err.pop()));
                if (err.get() || !img_f.get()) {
                    LOG(ERROR) << "Error retrieving image in SHM failover: "
                               << xcb_event_get_error_label(err.get()->error_code);
                    return;
                }
                if (shminfo_whole.shmseg) {
                    // if set, the caller asked the area in the whole image to be updated
                    // let's save some CPU cycles
                    if (full_redraw) {
                        memcpy(shminfo_whole.shmaddr, xcb_get_image_data(img_f.get()), 4 * width * height);
                    } else {
                        p->area_copy((uint32_t*)xcb_get_image_data(img_f.get()), (uint32_t*)shminfo_whole.shmaddr,
                            width, height, x, y, whole_width, whole_height);
                    }
                } else {
                    // otherwise, the caller only asks for partial updated image data
                    memcpy(shminfo_part.shmaddr, xcb_get_image_data(img_f.get()), 4 * width * height);
                }
            } else {
                // unknown error. we don't know about this
                LOG(ERROR) << string_printf("Error SHM get_image: %d\n", err.get()->error_code);
            }
        } else if (shminfo_whole.shmseg && !full_redraw) {
            // same as above, if set, the caller asked the area in the whole image to be updated
            p->area_copy((uint32_t*)shminfo_part.shmaddr, (uint32_t*)shminfo_whole.shmaddr, width, height, x, y,
                whole_width, whole_height);
        }
    };

    switch (data.shm_mode) {
        case states::SHMMode::PIXMAP: {
            // should we do full redraw instead?
            const uint32_t window_area = data.size.first * data.size.second;
            if ((size_changed || ((area.width * area.height * 2) > window_area)) && !use_custom_scaler) {
                // full redraw. this will directly dump the img to RP's backing pixmap
                auto shm_img = make_unique_malloc(xcb_shm_get_image_reply(p->be_conn,
                    xcb_shm_get_image(p->be_conn, data.bwid, 0, 0, data.size.first, data.size.second, plane_mask,
                        XCB_IMAGE_FORMAT_Z_PIXMAP, data.shm_data.be_shm_z_whole->info.shmseg, 0),
                    &err.pop()));
                // on error, retry using ::IMAGE codepath, which has failover to use core get_image
                if (!err.get()) {
                    dip_opaquify((uint32_t*)data.shm_data.be_shm_z_whole->info.shmaddr, window_area);
                    break;
                }
                // yes. on error, switch to the next case ...
            }
            // switch to the next case ...
        }
        case states::SHMMode::IMAGE: {
            // Ask data from backend's window as Z image, and interpret that data to frontend's render picture's regular
            // backing pixmap.
            // On custom scaler, we need the whole image to be updated, because custom scalers will need pixel values
            // beyond the updated area's edge.
            xcb_shm_segment_info_t shm_none{0, 0, nullptr};
            shm_get_image_failover(p->be_conn, data.bwid, area.x, area.y, area.width, area.height, data.size.first,
                data.size.second, data.shm_data.be_shm_z_img->info,
                use_custom_scaler ? data.shm_data.be_shm_z_whole->info : shm_none);
            if (use_custom_scaler) {
                pxdip_raa(p->pxdip_instance, area.x, area.y, area.width, area.height, data.size.first, data.size.second,
                    (uint32_t*)data.shm_data.be_shm_z_whole->info.shmaddr,
                    (uint32_t*)data.shm_data.cst_shm_dst->info.shmaddr);
                // on PIXMAP mode, cst_shm_dst is directly linked to frontend's render picture.
                if (data.shm_mode == states::SHMMode::IMAGE) {
                    err.pop() = xcb_request_check(
                        p->fe_conn, xcb_shm_put_image_checked(p->fe_conn, data.fe_regular_rp_src->id, data.fe_gc->id,
                                        data.size.first * scfactor, data.size.second * scfactor, area.x * scfactor,
                                        area.y * scfactor, area.width * scfactor, area.height * scfactor,
                                        area.x * scfactor, area.y * scfactor, data.fe_depth, XCB_IMAGE_FORMAT_Z_PIXMAP,
                                        0, data.shm_data.cst_shm_dst->info.shmseg, 0));
                }
            } else {
                dip_opaquify((uint32_t*)data.shm_data.be_shm_z_img->info.shmaddr, area.width * area.height);
                err.pop() = xcb_request_check(p->fe_conn,
                    xcb_shm_put_image_checked(p->fe_conn,
                        (data.shm_mode == states::SHMMode::PIXMAP ? data.shm_data.fe_pxm_rp_src->id :
                                                                    data.fe_regular_rp_src->id),
                        data.fe_gc->id, area.width, area.height, 0, 0, area.width, area.height, area.x, area.y,
                        data.fe_depth, XCB_IMAGE_FORMAT_Z_PIXMAP, 0, data.shm_data.fe_shm_z_img->info.shmseg, 0));
            }
            if (err.get()) {
                LOG(ERROR) << string_printf("Error shm_put_image: %d\n", err.get()->error_code);
            }
            break;
        }
        case states::SHMMode::NONE: {
            auto img = make_unique_malloc(
                xcb_get_image_reply(p->be_conn, xcb_get_image(p->be_conn, XCB_IMAGE_FORMAT_Z_PIXMAP, data.bwid, area.x,
                                                    area.y, area.width, area.height, plane_mask),
                    &err.pop()));
            if (!img.get()) {
                LOG(ERROR) << "Cannot retrieve image";
                break;
            }
            int img_bmp_sz = xcb_get_image_data_length(img.get());
            uint8_t* img_bmp_data = xcb_get_image_data(img.get());
            if (use_custom_scaler) {
                if (size_changed) {
                    memcpy(data.cst_src_buff.get(), img_bmp_data, area.width * area.height * 4);
                } else {
                    p->area_copy((uint32_t*)img_bmp_data, data.cst_src_buff.get(), area.width, area.height, area.x,
                        area.y, data.size.first, data.size.second);
                }
                pxdip_raa(p->pxdip_instance, area.x, area.y, area.width, area.height, data.size.first, data.size.second,
                    data.cst_src_buff.get(), data.cst_dst_buff.get());
                // we're very sad to announce that X core put image cannot copy only a region of src. (we have to copy
                // the whole image)
                xcb_put_image(p->fe_conn, XCB_IMAGE_FORMAT_Z_PIXMAP, data.fe_regular_rp_src->id, data.fe_gc->id,
                    data.size.first * scfactor, data.size.second * scfactor, 0, 0, 0, data.fe_depth,
                    (data.size.first * scfactor * data.size.second * scfactor * 4), (uint8_t*)data.cst_dst_buff.get());
            } else {
                dip_opaquify((uint32_t*)img_bmp_data, area.width * area.height);
                xcb_put_image(p->fe_conn, XCB_IMAGE_FORMAT_Z_PIXMAP, data.fe_regular_rp_src->id, data.fe_gc->id,
                    area.width, area.height, area.x, area.y, 0, data.fe_depth, img_bmp_sz, img_bmp_data);
            }
            break;
        }
    }

    // and xrender!
    xcb_render_composite(p->fe_conn, XCB_RENDER_PICT_OP_SRC, data.xrender_data.rp_src->id, 0,
        data.xrender_data.rp_dst->id, area.x * scfactor, area.y * scfactor, 0, 0, area.x * scfactor, area.y * scfactor,
        area.width * scfactor, area.height * scfactor);
    xcb_flush(p->fe_conn);
}

void Renderer::rescale(uint16_t scfactor) {
    p->scale_factor = scfactor;
    p->update_xrender_transform();
}

Renderer::Priv::Priv() {
    // initialize non-dependent objects here (don't forget to destroy them also in Priv's destructor)
    pxdip_instance = pxdip_init(0);
}

Renderer::Priv::~Priv() {
    pxdip_destroy(pxdip_instance);
}

states::SHMMode Renderer::Priv::get_shm_mode(xcb_connection_t* conn, unordered_set<uint8_t> supported_pixmap_depth) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto shm_ver = make_unique_malloc(xcb_shm_query_version_reply(conn, xcb_shm_query_version(conn), &err.pop()));
    if (err.get()) {
        LOG(WARNING) << "Failed to query SHM version: " << xcb_event_get_error_label(err.get()->error_code);
        return states::SHMMode::NONE;
    }
    // check version
    if (shm_ver->major_version < 1) {
        LOG(INFO) << "SHM version unsupported. Disabling SHM for this server.";
        return states::SHMMode::NONE;
    }
    // check depth format (we're looking for depth 24 and/or 32, both must have 32bit scanline_pad and 32bit per pixel).
    // the caller is assumed to gave the correct information for this parameter.
    if (supported_pixmap_depth.size() < 1) {
        return states::SHMMode::NONE;
    }

    // check support
    states::SHMMode support = states::SHMMode::IMAGE;
    if (shm_ver->shared_pixmaps && shm_ver->pixmap_format == XCB_IMAGE_FORMAT_Z_PIXMAP) {
        // condition for pixmap SHM fullfilled!
        support = states::SHMMode::PIXMAP;
    }
    // check if server can use our SHM segment
    xcb_shm_segment_info_t shminfo;
    shminfo.shmid = shmget(IPC_PRIVATE, 32, IPC_CREAT | 0777);
    shminfo.shmaddr = (uint8_t*)shmat(shminfo.shmid, 0, 0);
    // let the XCB attach the SHM to X11 server
    shminfo.shmseg = xcb_generate_id(conn);
    err.pop() = xcb_request_check(conn, xcb_shm_attach_checked(conn, shminfo.shmseg, shminfo.shmid, 0));
    if (err.get()) {
        LOG(INFO)
            << "X server cannot access this instance's SHM segment. Very likely the server resides in other host.";
        return states::SHMMode::NONE;
    }
    // success!
    return support;
}

unordered_set<uint8_t> Renderer::Priv::get_supported_pixmap_depth(common::x11::VisualMap* visualmap) {
    unordered_set<uint8_t> ret;
    for (uint8_t depth : {24, 32}) {
        auto& bpps = visualmap->get_pixmap_depth_bpp_format(depth);
        auto& scanline_pads = visualmap->get_pixmap_scanline_pad(depth);
        if (bpps.size() > 1 || scanline_pads.size() > 1) {
            // format may be ambiguous. better we don't use SHM at all.
            LOG(INFO) << string_printf("Server has ambiguous pixmap format for depth %d.", depth);
            continue;
        } else if (bpps.size() == 1 && scanline_pads.size() == 1) {
            if (*bpps.begin() == 32 && *scanline_pads.begin() == 32) {
                ret.insert(depth);
            }
        }
    }
    return ret;
}

states::SHMMode Renderer::Priv::get_max_shm_mode() {
    auto mode =
        min(usercfg_max_shm, min(get_shm_mode(be_conn, be_supported_depth), get_shm_mode(fe_conn, fe_supported_depth)));
    auto mode_str = states::Consts::values.vmap_shm_modes.at(mode);
    LOG(INFO) << string_printf(
        "Maximum SHM level for backend display %s is %s.", display_name.c_str(), mode_str.c_str());
    printf(_("Maximum SHM level for backend display %1$s is '%2$s'."), display_name.c_str(), mode_str.c_str());
    putchar('\n');
    return mode;
}

void Renderer::Priv::update_xrender_transform() {
    // nearest and bilinear is RENDER's built-in, so we simply ask RENDER to rescale inputs.
    uint16_t transform_scfactor;
    switch (scale_algorithm) {
        case states::ScalingAlgorithm::NEAREST:
        case states::ScalingAlgorithm::BILINEAR:
            transform_scfactor = scale_factor;
            break;
        default:
            transform_scfactor = 1;
            break;
    }
    // inverted, because it is transformation from destination to source
    fixpt fscale = fixpt_div(FIXPT_ONE, fixpt_fromint(transform_scfactor));
    // 2D transformation/affine matrix
    xrender_data.src_transform = {fscale, 0, 0, 0, fscale, 0, 0, 0, FIXPT_ONE};
}

void Renderer::Priv::resize_xrender_pixmap(WindowData& data, bool force) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    // only resize if necessary
    if (data.xrender_data.rp_size != data.size || force) {
        uint32_t pixmap_size = (uint32_t)data.size.first * (uint32_t)data.size.second * 4;
        // for RENDER's supported scaling algorithms, let this be 1.
        uint32_t final_scfactor, final_scfactor_area;
        bool use_custom_scaler;
        switch (scale_algorithm) {
            case states::ScalingAlgorithm::NEAREST:
            case states::ScalingAlgorithm::BILINEAR:
                use_custom_scaler = false;
                final_scfactor = 1;
                break;
            default:
                // no need to invoke custom scaler if scale_factor is 1
                use_custom_scaler = scale_factor > 1;
                final_scfactor = scale_factor;
                break;
        }
        final_scfactor_area = final_scfactor * final_scfactor;
        switch (data.shm_mode) {
            case states::SHMMode::PIXMAP: {
                // pixmap must be destroyed first before it's backing SHM
                data.shm_data.fe_pxm_rp_src.reset();
                xcb_pixmap_t shm_pxm = xcb_generate_id(fe_conn);
                if (!use_custom_scaler) {
                    // on default scaler, be_shm_z_whole is also for frontend's destination. It doesn't act as
                    // frontend's backing pixmap when in custom scaler mode, so it'll be initialized elsewhere.
                    data.shm_data.fe_shm_pxm_rp_src = make_unique<xraii::SHM>(fe_conn, pixmap_size);
                    data.shm_data.be_shm_z_whole =
                        make_unique<xraii::SHM>(be_conn, pixmap_size, data.shm_data.fe_shm_pxm_rp_src->info.shmid);
                    // SHM backed pixmap for src render picture backing - directly "connected" to z_whole
                    err.pop() = xcb_request_check(
                        fe_conn, xcb_shm_create_pixmap_checked(fe_conn, shm_pxm, data.fwid, data.size.first,
                                     data.size.second, data.fe_depth, data.shm_data.fe_shm_pxm_rp_src->info.shmseg, 0));
                } else {
                    // SHM backend pixmap for custom scaler's destination
                    data.shm_data.cst_shm_dst = make_unique<xraii::SHM>(fe_conn, pixmap_size * final_scfactor_area);
                    err.pop() = xcb_request_check(
                        fe_conn, xcb_shm_create_pixmap_checked(fe_conn, shm_pxm, data.fwid,
                                     data.size.first * final_scfactor, data.size.second * final_scfactor, data.fe_depth,
                                     data.shm_data.cst_shm_dst->info.shmseg, 0));
                }
                if (err.get()) {
                    LOG(ERROR) << string_printf(
                        "Cannot create frontend SHM pixmap: %s\n", xcb_event_get_error_label(err.get()->error_code));
                    return;
                }
                data.shm_data.fe_pxm_rp_src = make_unique<xraii::Pixmap>(fe_conn, shm_pxm);
                data.xrender_data.fe_pxm_rp_src = data.shm_data.fe_pxm_rp_src.get();
                // switch to the next case ...
            }
            case states::SHMMode::IMAGE: {
                if (use_custom_scaler) {
                    // custom scaler needs full frame for "sliding window" extends/edges
                    data.shm_data.be_shm_z_whole = make_unique<xraii::SHM>(be_conn, pixmap_size);
                    // IMAGE mode also needs custom scaler's destination, but this time, it is not connected to the
                    // frontend's render picture.
                    if (data.shm_mode == states::SHMMode::IMAGE) {
                        data.shm_data.cst_shm_dst = make_unique<xraii::SHM>(fe_conn, pixmap_size * final_scfactor_area);
                    }
                }
                // new SHM (assume 32 bpp) for IMAGE mode (or partial image in PIXMAP mode / custom scaler mode)
                data.shm_data.be_shm_z_img = make_unique<xraii::SHM>(be_conn, pixmap_size);
                if (!use_custom_scaler) {
                    data.shm_data.fe_shm_z_img =
                        make_unique<xraii::SHM>(fe_conn, pixmap_size, data.shm_data.be_shm_z_img->info.shmid);
                }
                break;
            }
            case states::SHMMode::NONE: {
                data.cst_src_buff = make_unique<uint32_t[]>(pixmap_size);
                data.cst_dst_buff = make_unique<uint32_t[]>(pixmap_size * final_scfactor_area);
                break;
            }
        }
        // SHM mode IMAGE and NONE will use regular pixmap (obviously)
        switch (data.shm_mode) {
            case states::SHMMode::IMAGE:
            case states::SHMMode::NONE:
                data.fe_regular_rp_src = make_unique<xraii::Pixmap>(fe_conn, data.fe_depth, fe_scr->root,
                    data.size.first * final_scfactor, data.size.second * final_scfactor);
                data.xrender_data.fe_pxm_rp_src = data.fe_regular_rp_src.get();
                break;
            default:
                break;
        }

        // update render picture
        data.xrender_data.rp_src = make_unique<xraii::XRenderPicture>(
            fe_conn, data.xrender_data.fe_pxm_rp_src->id, data.xrender_data.format.id, 0, nullptr);
        data.xrender_data.rp_size = data.size;
        // xrender's picture transform applies to src
        if (!use_custom_scaler) {
            xcb_render_set_picture_transform(fe_conn, data.xrender_data.rp_src->id, xrender_data.src_transform);
        }
        // xrender's picture filter also applies to src
        string filtername;
        switch (scale_algorithm) {
            case states::ScalingAlgorithm::BILINEAR:
                filtername = "bilinear";
                break;
            default:
                filtername = "nearest";
                break;
        }
        xcb_render_set_picture_filter(
            fe_conn, data.xrender_data.rp_src->id, filtername.size(), filtername.c_str(), 0, 0);
    }
}

void Renderer::Priv::area_copy(uint32_t* __restrict__ src,
    uint32_t* __restrict__ dst,
    uint16_t src_w,
    uint16_t src_h,
    uint16_t dst_x,
    uint16_t dst_y,
    uint16_t dst_w,
    uint16_t dst_h) {
    uint32_t* c_src = src;
    uint32_t* c_dst = dst + (dst_y * dst_w + dst_x);
    for (int row = 0; row < src_h; ++row) {
        copy(c_src, c_src + src_w, c_dst);
        c_src += src_w;
        c_dst += dst_w;
    }
}