#include "frontend/x11/propmgr.h"
#include "common/x11.h"
#include "common/xraii/gc.h"
#include "common/xraii/pixmap.h"
#include "shared/memory.h"
#include "shared/util.h"
#include <functional>
#include <glog/logging.h>
#include <map>
#include <unordered_set>
#include <xcb/xcb.h>
#include <xcb/xcb_ewmh.h>
#include <xcb/xcb_icccm.h>
#include <xcb/xcb_util.h>

using namespace std;
using namespace pxwm;
using namespace pxwm::common;
using namespace pxwm::common::x11;
using namespace pxwm::frontend::x11;
using namespace px::memory;
using namespace px::util;

class PropMgr::Priv {
public:
    WIDTrans* wid_trans;
    xcb_connection_t* fconn;
    xcb_connection_t* bconn;
    xcb_window_t froot;
    uint16_t scfactor = 1;
    /**
     * The associated functions will return true if the property is valid and should be forwarded, or false if the
     * property is detected as invalid or should not be forwarded.
     */
    typedef map<xcb_atom_t, function<bool(PropMgr::Priv*, PropData&)>> hdlr_map_t;
    hdlr_map_t reg_hdlrs_forward;
    hdlr_map_t reg_hdlrs_backward;
    hdlr_map_t root_hdlrs_backward;

    // These properties will be re-forwarded if scale is changed.
    // Contents: [backend window ID][backend property atom] = unaltered property data and value
    map<xcb_window_t, map<xcb_atom_t, PropData>> resizable_properties_store;

    Priv() : fw_wm_hints_helper{this}, fw_wm_protocols_helper{this}, bw_net_supported_helper{this} {};
    Priv(const Priv&) = delete;
    Priv& operator=(const Priv&) = delete;
    void fill_functions();
    shared_ptr<void> copy_data(int len, void* src);
    void delete_resizable_properties_store(xcb_window_t bwin_id, xcb_atom_t bprop_id);
    // forward
    bool hdl_fw_any(PropData& prop);
    /**
     * @brief Applicable to all properties whose content is solely consist of an atom array.
     */
    bool hdl_fw_any_atom_list(PropData& prop);
    bool hdl_fw_wm_normal_hints(PropData& prop);
    bool hdl_fw_wm_hints(PropData& prop);
    bool hdl_fw_transient_for(PropData& prop);
    bool hdl_fw_wm_protocols(PropData& prop);
    bool hdl_fw_net_wm_strut(PropData& prop, int card_count);
    // backward
    bool hdl_bw_any(PropData& prop);
    bool hdl_bw_any_atom_list(PropData& prop);
    bool hdl_bw_wm_state(PropData& prop);
    bool hdl_bw_net_wm_supported(PropData& prop);
    // helpers
    struct fw_wm_hints_helper {
        Priv* p;
        shared_ptr<xraii::GC> fe_gc_p1, fe_gc_p24;

        // first == icon, second == icon_mask
        map<xcb_window_t, pair<shared_ptr<xraii::Pixmap>, shared_ptr<xraii::Pixmap>>> icon_pxm;

        typedef map<xcb_window_t, xcb_window_t> win_map_t;
        win_map_t be_fe_leader_map;
        // to ease d'tor's work
        set<xcb_window_t> fe_leaders;

        fw_wm_hints_helper(Priv* p) : p{p} {}
        ~fw_wm_hints_helper();
        shared_ptr<xraii::Pixmap> copy_save_pixmap(xcb_window_t fwid, xcb_pixmap_t bpxm);
        /**
         * Create a new blank, unmapped window, for use with 'window_group' field
         * @param bleader Backend's window ID that acts as the group's leader
         * @param ref_fwid Frontend's window ID that is going to act as one of the group's follower
         * @return
         */
        xcb_window_t get_wingroup_id(xcb_window_t bleader, xcb_window_t ref_fwid);
        /**
         * Free or remove ref count of additional frontend's resource(s) associated with the given window.
         */
        void remove(xcb_window_t fwid);
    } fw_wm_hints_helper;
    struct fw_wm_protocols_helper {
        Priv* p;
        set<xcb_atom_t> accepted_protocols;
        fw_wm_protocols_helper(Priv* p) : p{p} {}
        void init();
    } fw_wm_protocols_helper;
    struct bw_net_supported_helper {
        Priv* p;
        unordered_set<xcb_atom_t> net_supp_fe;
        bw_net_supported_helper(Priv* p) : p{p} {}
        void init();
    } bw_net_supported_helper;
};

PropMgr::PropMgr(WIDTrans* wid_trans, xcb_connection_t* fconn, xcb_connection_t* bconn) {
    p = make_unique<Priv>();
    p->wid_trans = wid_trans;
    p->fconn = fconn;
    p->bconn = bconn;
    p->froot = wid_trans->get_fe_root_id();
    p->fill_functions();
    // initialize helpers
    p->fw_wm_protocols_helper.init();
    p->bw_net_supported_helper.init();
}

PropMgr::~PropMgr() {}

bool PropMgr::forward_property(PropData& prop) {
    Priv::hdlr_map_t::iterator it;
    if (prop.target && p->wid_trans->get_fw_id(prop.target) &&
        check_and_get_mapped_value(p->reg_hdlrs_forward, prop.property, it)) {
        return it->second(p.get(), prop);
    }
    return false;
}

bool PropMgr::backward_property(PropData& prop) {
    Priv::hdlr_map_t::iterator it;
    if (prop.target && ((p->wid_trans->get_bw_id(prop.target) &&
                            check_and_get_mapped_value(p->reg_hdlrs_backward, prop.property, it)) ||
                           (prop.target == p->wid_trans->get_fe_root_id() &&
                               check_and_get_mapped_value(p->root_hdlrs_backward, prop.property, it)))) {
        return it->second(p.get(), prop);
    }
    return false;
}

void PropMgr::delete_frontend_window(xcb_window_t wid) {
    p->fw_wm_hints_helper.remove(wid);
    p->resizable_properties_store.erase(p->wid_trans->get_bw_id(wid));
}

vector<PropData> PropMgr::set_fe_scaling_factor(uint16_t scale_factor) {
    p->scfactor = scale_factor;
    // return raw properties that have to be reforwarded (for rescaling)
    vector<PropData> ret;
    for (auto kvp1 : p->resizable_properties_store) {
        for (auto kvp2 : kvp1.second) {
            ret.push_back(kvp2.second);
        }
    }
    return ret;
}

PropData PropMgr::convert_from_event(xcb_connection_t* conn, xcb_property_notify_event_t* evt) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    PropData ret;
    ret.target = 0;
    ret.property = evt->atom;
    ret.state = evt->state;
    if (ret.state == XCB_PROPERTY_NEW_VALUE) {
        auto prop_rep = make_unique_malloc(xcb_get_property_reply(conn,
            xcb_get_property(conn, 0, evt->window, ret.property, XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE),
            &err.pop()));
        if (err.get() || prop_rep.get() == nullptr) {
            LOG(WARNING) << string_printf(
                "Cannot retrieve property value on PropMgr (%s)", xcb_event_get_error_label(err.get()->error_code));
            return ret;
        }
        if (prop_rep->format == 0) {
            LOG(INFO) << string_printf("Incomplete property received");
            return ret;
        }
        ret.target = evt->window;
        ret.prop_val.prop_type = prop_rep->type;
        ret.prop_val.format_bpp = prop_rep->format;
        ret.prop_val.data_len = xcb_get_property_value_length(prop_rep.get());
        ret.prop_val.data_ptr = make_unique_malloc(malloc(ret.prop_val.data_len));
        memcpy(ret.prop_val.data_ptr.get(), xcb_get_property_value(prop_rep.get()), ret.prop_val.data_len);
    } else if (ret.state == XCB_PROPERTY_DELETE) {
        ret.target = evt->window;
    }
    return ret;
}

PropData PropMgr::convert_from_xproperty(xcb_window_t target, sharedds::XProperty const& prop) {
    PropData ret;
    ret.target = target;
    ret.property = prop.prop_atom;
    ret.state = XCB_PROPERTY_NEW_VALUE;
    ret.prop_val = prop.value;
    return ret;
}

PropData PropMgr::get_from_window(xcb_connection_t* conn, xcb_window_t window, xcb_atom_t prop) {
    PropData ret;
    ret.target = 0;
    ret.property = prop;
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto prop_rep = make_unique_malloc(xcb_get_property_reply(conn,
        xcb_get_property(conn, 0, window, ret.property, XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE),
        &err.pop()));
    if (err.get() || prop_rep.get() == nullptr) {
        LOG(WARNING) << string_printf(
            "Cannot retrieve property value on PropMgr (%s)", xcb_event_get_error_label(err.get()->error_code));
        return ret;
    }
    if (prop_rep->format == 0) {
        LOG(WARNING) << string_printf("Incomplete property received");
        return ret;
    }
    ret.target = window;
    ret.state = XCB_PROPERTY_NEW_VALUE;
    ret.prop_val.prop_type = prop_rep->type;
    ret.prop_val.format_bpp = prop_rep->format;
    ret.prop_val.data_len = xcb_get_property_value_length(prop_rep.get());
    ret.prop_val.data_ptr = make_unique_malloc(malloc(ret.prop_val.data_len));
    memcpy(ret.prop_val.data_ptr.get(), xcb_get_property_value(prop_rep.get()), ret.prop_val.data_len);
    return ret;
}

void PropMgr::Priv::fill_functions() {
    // forward
    reg_hdlrs_forward[get_atom(bconn, false, "WM_NAME")] = &Priv::hdl_fw_any;
    reg_hdlrs_forward[get_atom(bconn, false, "WM_ICON_NAME")] = &Priv::hdl_fw_any;
    reg_hdlrs_forward[get_atom(bconn, false, "WM_NORMAL_HINTS")] = &Priv::hdl_fw_wm_normal_hints;
    reg_hdlrs_forward[get_atom(bconn, false, "WM_HINTS")] = &Priv::hdl_fw_wm_hints;
    reg_hdlrs_forward[get_atom(bconn, false, "WM_CLASS")] = &Priv::hdl_fw_any;
    reg_hdlrs_forward[get_atom(bconn, false, "WM_TRANSIENT_FOR")] = &Priv::hdl_fw_transient_for;
    reg_hdlrs_forward[get_atom(bconn, false, "WM_PROTOCOLS")] = &Priv::hdl_fw_wm_protocols;
    reg_hdlrs_forward[get_atom(bconn, false, "_NET_WM_NAME")] = &Priv::hdl_fw_any;
    reg_hdlrs_forward[get_atom(bconn, false, "_NET_WM_ICON_NAME")] = &Priv::hdl_fw_any;
    reg_hdlrs_forward[get_atom(bconn, false, "_NET_WM_WINDOW_TYPE")] = &Priv::hdl_fw_any_atom_list;
    reg_hdlrs_forward[get_atom(bconn, false, "_NET_WM_STRUT")] =
        bind(&Priv::hdl_fw_net_wm_strut, placeholders::_1, placeholders::_2, 4);
    reg_hdlrs_forward[get_atom(bconn, false, "_NET_WM_STRUT_PARTIAL")] =
        bind(&Priv::hdl_fw_net_wm_strut, placeholders::_1, placeholders::_2, 12);
    reg_hdlrs_forward[get_atom(bconn, false, "_NET_WM_ICON_GEOMETRY")] = &Priv::hdl_fw_any;
    reg_hdlrs_forward[get_atom(bconn, false, "_NET_WM_ICON")] = &Priv::hdl_fw_any;
    // backward
    reg_hdlrs_backward[get_atom(fconn, false, "WM_STATE")] = &Priv::hdl_bw_wm_state;
    reg_hdlrs_backward[get_atom(fconn, false, "_NET_WM_VISIBLE_NAME")] = &Priv::hdl_bw_any;
    reg_hdlrs_backward[get_atom(fconn, false, "_NET_WM_VISIBLE_ICON_NAME")] = &Priv::hdl_bw_any;
    reg_hdlrs_backward[get_atom(fconn, false, "_NET_WM_STATE")] = &Priv::hdl_bw_any_atom_list;
    root_hdlrs_backward[get_atom(fconn, false, "WM_ICON_SIZE")] = &Priv::hdl_bw_any;
    root_hdlrs_backward[get_atom(fconn, false, "_NET_SUPPORTED")] = &Priv::hdl_bw_net_wm_supported;
}

shared_ptr<void> PropMgr::Priv::copy_data(int len, void* src) {
    auto ret = make_shared_malloc(malloc(len));
    memcpy(ret.get(), src, len);
    return ret;
}

void PropMgr::Priv::delete_resizable_properties_store(xcb_window_t bwin_id, xcb_atom_t bprop_id) {
    if (resizable_properties_store.count(bwin_id)) {
        if (resizable_properties_store.count(bprop_id)) {
            resizable_properties_store.erase(bprop_id);
        }
    }
}

bool PropMgr::Priv::hdl_fw_any(PropData& prop) {
    prop.property = get_atom(fconn, false, get_atom_name(bconn, prop.property));
    prop.target = wid_trans->get_fw_id(prop.target);
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        prop.prop_val.prop_type = get_atom(fconn, false, get_atom_name(bconn, prop.prop_val.prop_type));
    }
    return true;
}

bool PropMgr::Priv::hdl_fw_any_atom_list(PropData& prop) {
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        // check
        if (prop.prop_val.format_bpp != 32 || prop.prop_val.prop_type != XCB_ATOM_ATOM) {
            return false;
        }
        prop.prop_val.data_ptr = copy_data(prop.prop_val.data_len, prop.prop_val.data_ptr.get());
        // forward atom translate
        int count = prop.prop_val.data_len / 4;
        xcb_atom_t* atoms = (xcb_atom_t*)prop.prop_val.data_ptr.get();
        for (int i = 0; i < count; ++i) {
            atoms[i] = get_atom(fconn, false, get_atom_name(bconn, atoms[i]));
        }
    }
    return hdl_fw_any(prop);
}

bool PropMgr::Priv::hdl_fw_wm_normal_hints(PropData& prop) {
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        // check
        if (prop.prop_val.format_bpp != 32 || prop.prop_val.prop_type != get_atom(bconn, true, "WM_SIZE_HINTS") ||
            prop.prop_val.data_len < 72) {
            return false;
        }
        // save to resizable store (unaltered data)
        prop.prop_val.data_ptr = copy_data(prop.prop_val.data_len, prop.prop_val.data_ptr.get());
        resizable_properties_store[prop.target][prop.property] = prop;

        // copy data for alteration
        prop.prop_val.data_ptr = copy_data(prop.prop_val.data_len, prop.prop_val.data_ptr.get());
        auto propobj = (xcb_size_hints_t*)prop.prop_val.data_ptr.get();
        // zero out pad fields (4*4 bytes)
        memset(&propobj->x, 0, 16);
        // set minimum increment
        if (!(propobj->flags & XCB_ICCCM_SIZE_HINT_P_RESIZE_INC)) {
            propobj->flags = propobj->flags | XCB_ICCCM_SIZE_HINT_P_RESIZE_INC;
            propobj->width_inc = propobj->height_inc = 1;
        }
        // scale
        propobj->min_width *= scfactor;
        propobj->min_height *= scfactor;
        propobj->max_width *= scfactor;
        propobj->max_height *= scfactor;
        propobj->width_inc *= scfactor;
        propobj->height_inc *= scfactor;
        propobj->base_width *= scfactor;
        propobj->base_height *= scfactor;
    } else if (prop.state == XCB_PROPERTY_DELETE) {
        delete_resizable_properties_store(prop.target, prop.property);
    }
    return hdl_fw_any(prop);
}

bool PropMgr::Priv::hdl_fw_wm_hints(PropData& prop) {
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        // check
        if (prop.prop_val.format_bpp != 32 || prop.prop_val.prop_type != get_atom(bconn, true, "WM_HINTS") ||
            prop.prop_val.data_len < 36) {
            return false;
        }
        prop.prop_val.data_ptr = copy_data(prop.prop_val.data_len, prop.prop_val.data_ptr.get());
        auto cdata = (xcb_icccm_wm_hints_t*)prop.prop_val.data_ptr.get();
        // clear 'state', 'message' and 'icon_window' flag
        cdata->flags = cdata->flags & (0x175);
        cdata->initial_state = XCB_ICCCM_WM_STATE_NORMAL;
        cdata->icon_window = 0;

        // we like to refer things using frontend's ID
        xcb_window_t fw_id = wid_trans->get_fw_id(prop.target);

        // window group
        if (cdata->flags & XCB_ICCCM_WM_HINT_WINDOW_GROUP) {
            if (!(cdata->window_group = wid_trans->get_fw_id(cdata->window_group))) {
                // create a new, blank, unmapped window
                if (!(cdata->window_group = fw_wm_hints_helper.get_wingroup_id(cdata->window_group, fw_id))) {
                    cdata->flags = cdata->flags ^ XCB_ICCCM_WM_HINT_WINDOW_GROUP;
                }
            }
        }
        // icon
        xraii::Pixmap* pxm_wrapper;
        if (cdata->flags & XCB_ICCCM_WM_HINT_ICON_PIXMAP) {
            if ((pxm_wrapper = (fw_wm_hints_helper.icon_pxm[fw_id].first =
                                    fw_wm_hints_helper.copy_save_pixmap(fw_id, cdata->icon_pixmap))
                                   .get())) {
                cdata->icon_pixmap = pxm_wrapper->id;
            } else {
                cdata->icon_pixmap = 0;
                cdata->flags = cdata->flags ^ XCB_ICCCM_WM_HINT_ICON_PIXMAP;
            }
        }
        if (cdata->flags & XCB_ICCCM_WM_HINT_ICON_MASK) {
            if ((pxm_wrapper = (fw_wm_hints_helper.icon_pxm[fw_id].second =
                                    fw_wm_hints_helper.copy_save_pixmap(fw_id, cdata->icon_mask))
                                   .get())) {
                cdata->icon_mask = pxm_wrapper->id;
            } else {
                cdata->icon_mask = 0;
                cdata->flags = cdata->flags ^ XCB_ICCCM_WM_HINT_ICON_MASK;
            }
        }
    }
    return hdl_fw_any(prop);
}

bool PropMgr::Priv::hdl_fw_transient_for(PropData& prop) {
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        // check
        if (prop.prop_val.format_bpp != 32 || prop.prop_val.prop_type != get_atom(bconn, true, "WINDOW") ||
            prop.prop_val.data_len < 4) {
            return false;
        }
        prop.prop_val.data_ptr = copy_data(prop.prop_val.data_len, prop.prop_val.data_ptr.get());
        xcb_window_t* propval = (xcb_window_t*)prop.prop_val.data_ptr.get();
        if (!(*propval = wid_trans->get_fw_id(*propval))) {
            return false;
        }
    }
    return hdl_fw_any(prop);
}

bool PropMgr::Priv::hdl_fw_wm_protocols(PropData& prop) {
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        // check
        if (prop.prop_val.format_bpp != 32 || prop.prop_val.prop_type != XCB_ATOM_ATOM || prop.prop_val.data_len < 4) {
            return false;
        }
        // filter and convert filtered atom(s)
        vector<xcb_atom_t> filtered_protocs;
        xcb_atom_t* atom_vals = (xcb_atom_t*)prop.prop_val.data_ptr.get();
        int atom_len = prop.prop_val.data_len / 4;
        for (int i = 0; i < atom_len; ++i) {
            if (fw_wm_protocols_helper.accepted_protocols.count(atom_vals[i])) {
                filtered_protocs.push_back(get_atom(fconn, false, get_atom_name(bconn, atom_vals[i])));
            }
        }
        // write! (atoms are uint32)
        prop.prop_val.data_len = (filtered_protocs.size() + 1) * 4;
        prop.prop_val.data_ptr = make_shared_malloc(malloc(prop.prop_val.data_len));
        // always make sure that WM_DELETE_WINDOW is still here
        xcb_atom_t* dst = (xcb_atom_t*)prop.prop_val.data_ptr.get();
        dst[0] = get_atom(fconn, false, "WM_DELETE_WINDOW");
        memcpy(&dst[1], &filtered_protocs[0], filtered_protocs.size() * 4);
    }
    return hdl_fw_any(prop);
}

bool PropMgr::Priv::hdl_fw_net_wm_strut(PropData& prop, int card_count) {
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        // check
        if (prop.prop_val.format_bpp != 32 || prop.prop_val.prop_type != XCB_ATOM_CARDINAL ||
            prop.prop_val.data_len < card_count * 4) {
            return false;
        }
        // save to resizable store (unaltered data)
        prop.prop_val.data_ptr = copy_data(prop.prop_val.data_len, prop.prop_val.data_ptr.get());
        resizable_properties_store[prop.target][prop.property] = prop;

        // copy data for alteration
        prop.prop_val.data_ptr = copy_data(prop.prop_val.data_len, prop.prop_val.data_ptr.get());
        // scale
        uint32_t* strut = (uint32_t*)prop.prop_val.data_ptr.get();
        for (int i = 0; i < card_count; ++i) {
            strut[i] *= scfactor;
        }
    } else if (prop.state == XCB_PROPERTY_DELETE) {
        delete_resizable_properties_store(prop.target, prop.property);
    }
    return hdl_fw_any(prop);
}

bool PropMgr::Priv::hdl_bw_any(PropData& prop) {
    prop.property = get_atom(bconn, false, get_atom_name(fconn, prop.property));
    // also translate root window
    prop.target =
        prop.target == wid_trans->get_fe_root_id() ? wid_trans->get_be_root_id() : wid_trans->get_bw_id(prop.target);
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        prop.prop_val.prop_type = get_atom(bconn, false, get_atom_name(fconn, prop.prop_val.prop_type));
    }
    return true;
}

bool PropMgr::Priv::hdl_bw_any_atom_list(PropData& prop) {
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        // check
        if (prop.prop_val.format_bpp != 32 || prop.prop_val.prop_type != XCB_ATOM_ATOM) {
            return false;
        }
        prop.prop_val.data_ptr = copy_data(prop.prop_val.data_len, prop.prop_val.data_ptr.get());
        // backward atom translate
        int count = prop.prop_val.data_len / 4;
        xcb_atom_t* atoms = (xcb_atom_t*)prop.prop_val.data_ptr.get();
        for (int i = 0; i < count; ++i) {
            atoms[i] = get_atom(bconn, false, get_atom_name(fconn, atoms[i]));
        }
    }
    return hdl_bw_any(prop);
}

bool PropMgr::Priv::hdl_bw_wm_state(PropData& prop) {
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        // check
        if (prop.prop_val.format_bpp != 32 || prop.prop_val.prop_type != get_atom(fconn, true, "WM_STATE") ||
            prop.prop_val.data_len < 2) {
            return false;
        }
        prop.prop_val.data_ptr = copy_data(prop.prop_val.data_len, prop.prop_val.data_ptr.get());
        // we don't support icon windows
        uint32_t* propval = (uint32_t*)prop.prop_val.data_ptr.get();
        propval[1] = 0;
    }
    return hdl_bw_any(prop);
}

bool PropMgr::Priv::hdl_bw_net_wm_supported(PropData& prop) {
    if (prop.state == XCB_PROPERTY_NEW_VALUE) {
        // check
        if (prop.prop_val.format_bpp != 32 || prop.prop_val.prop_type != XCB_ATOM_ATOM) {
            return false;
        }
        // intersect
        int fe_count = prop.prop_val.data_len / 4;
        vector<xcb_atom_t> mutual_support;
        xcb_atom_t* fe_supp = (xcb_atom_t*)prop.prop_val.data_ptr.get();
        for (int i = 0; i < fe_count; ++i) {
            if (bw_net_supported_helper.net_supp_fe.count(fe_supp[i])) {
                mutual_support.push_back(get_atom(bconn, false, get_atom_name(fconn, fe_supp[i])));
            }
        }
        // set value
        prop.prop_val.data_len = mutual_support.size() * 4;
        prop.prop_val.data_ptr = make_unique_malloc(malloc(prop.prop_val.data_len));
        memcpy(prop.prop_val.data_ptr.get(), &mutual_support[0], prop.prop_val.data_len);
    }
    return hdl_bw_any(prop);
}

PropMgr::Priv::fw_wm_hints_helper::~fw_wm_hints_helper() {
    if (!xcb_connection_has_error(p->fconn)) {
        // release all frontend's dummy windows (ex group leader) - because we are responsible (X11) citizens!
        for (auto wid : fe_leaders) {
            xcb_destroy_window(p->fconn, wid);
        }
        // this time, we have to sync, to make sure we release all resources ASAP, before the conn die.
        xcb_flush(p->fconn);
    }
}

shared_ptr<xraii::Pixmap> PropMgr::Priv::fw_wm_hints_helper::copy_save_pixmap(xcb_window_t fwid, xcb_pixmap_t bpxm) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    // pull bitmap data from backend server
    auto geom = make_unique_malloc(xcb_get_geometry_reply(p->bconn, xcb_get_geometry(p->bconn, bpxm), &err.pop()));
    if (err.get()) {
        LOG(WARNING) << "Cannot retrieve icon pixmap geometry.";
        return 0;
    }
    auto img = make_unique_malloc(xcb_get_image_reply(p->bconn,
        xcb_get_image(p->bconn, XCB_IMAGE_FORMAT_Z_PIXMAP, bpxm, 0, 0, geom->width, geom->height, 0xFFFFFFFF),
        &err.pop()));
    if (err.get()) {
        LOG(WARNING) << "Cannot retrieve icon pixmap bitmap data.";
        return 0;
    }
    const int img_len = xcb_get_image_data_length(img.get()); // in bytes
    const int img_bpp = (img_len * 8) / (geom->width * geom->height); // bit per pixel
    shared_ptr<xraii::GC>* fe_gc = nullptr;
    if (img->depth == 1 && img_bpp == 1) {
        fe_gc = &fe_gc_p1;
    } else if (img->depth == 24 && img_bpp == 32) {
        fe_gc = &fe_gc_p24;
    } else {
        LOG(WARNING) << string_printf(
            "The pixmap to be copied has unsupported depth (%d planes) and bpp (%d bpp).", img->depth, img_bpp);
        return 0;
    }
    // create pixmap @ FE
    auto fpxm = make_shared<xraii::Pixmap>(p->fconn, img->depth, fwid, geom->width, geom->height);
    // create FE's GC
    if (!(*fe_gc).get() || !(*fe_gc)->id) {
        *fe_gc = make_shared<xraii::GC>(p->fconn, fpxm->id, 0, nullptr);
    }
    // write bitmap to frontend server's pixmap
    err.pop() = xcb_request_check(
        p->fconn, xcb_put_image_checked(p->fconn, XCB_IMAGE_FORMAT_Z_PIXMAP, fpxm->id, (*fe_gc)->id, geom->width,
                      geom->height, 0, 0, 0, img->depth, img_len, xcb_get_image_data(img.get())));
    if (err.get()) {
        // log
        LOG(ERROR) << string_printf("Cannot write bitmap data to frontend's icon pixmap (%s)",
            xcb_event_get_error_label(err.get()->error_code));
        return nullptr;
    } else {
        return fpxm;
    }
}

xcb_window_t PropMgr::Priv::fw_wm_hints_helper::get_wingroup_id(xcb_window_t bleader, xcb_window_t ref_fwid) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    win_map_t::iterator it;

    if (check_and_get_mapped_value(be_fe_leader_map, bleader, it)) {
        return it->second;
    } else {
        // these windows will be erased on this class d'tor
        xcb_window_t fleader = xcb_generate_id(p->fconn);
        err.pop() =
            xcb_request_check(p->fconn, xcb_create_window_checked(p->fconn, 24, fleader, p->froot, 10, 10, 10, 10, 0,
                                            XCB_WINDOW_CLASS_INPUT_OUTPUT, XCB_COPY_FROM_PARENT, 0, nullptr));
        if (err.get()) {
            LOG(ERROR) << string_printf(
                "Cannot create window_group leader (%s)", xcb_event_get_error_label(err.get()->error_code));
            return 0;
        }
        be_fe_leader_map.insert(pair<xcb_window_t, xcb_window_t>(bleader, fleader));
        fe_leaders.insert(fleader);
        return fleader;
    }
}

void PropMgr::Priv::fw_wm_hints_helper::remove(xcb_window_t fwid) {
    icon_pxm.erase(fwid);
}

void PropMgr::Priv::fw_wm_protocols_helper::init() {
    string ac_atom_str[] = {"WM_TAKE_FOCUS"};
    for (string& str : ac_atom_str) {
        accepted_protocols.insert(get_atom(p->bconn, false, str));
    }
}

void PropMgr::Priv::bw_net_supported_helper::init() {
    // note: since C++11, static scoped variables are thread safe!
    static unordered_set<string> supported = {"_NET_SUPPORTED", "_NET_ACTIVE_WINDOW", "_NET_WM_NAME",
        "_NET_WM_VISIBLE_NAME", "_NET_WM_WINDOW_TYPE", "_NET_WM_WINDOW_TYPE_DOCK", "_NET_WM_WINDOW_TYPE_TOOLBAR",
        "_NET_WM_WINDOW_TYPE_MENU", "_NET_WM_WINDOW_TYPE_UTILITY", "_NET_WM_WINDOW_TYPE_SPLASH",
        "_NET_WM_WINDOW_TYPE_DIALOG", "_NET_WM_WINDOW_TYPE_DROPDOWN_MENU", "_NET_WM_WINDOW_TYPE_POPUP_MENU",
        "_NET_WM_WINDOW_TYPE_TOOLTIP", "_NET_WM_WINDOW_TYPE_NOTIFICATION", "_NET_WM_WINDOW_TYPE_COMBO",
        "_NET_WM_WINDOW_TYPE_DND", "_NET_WM_WINDOW_TYPE_NORMAL", "_NET_WM_STATE", "_NET_WM_STATE_MODAL",
        "_NET_WM_STATE_STICKY", "_NET_WM_STATE_MAXIMIZED_VERT", "_NET_WM_STATE_MAXIMIZED_HORZ", "_NET_WM_STATE_SHADED",
        "_NET_WM_STATE_SKIP_TASKBAR", "_NET_WM_STATE_SKIP_PAGER", "_NET_WM_STATE_HIDDEN", "_NET_WM_STATE_FULLSCREEN",
        "_NET_WM_STATE_ABOVE", "_NET_WM_STATE_BELOW", "_NET_WM_STATE_DEMANDS_ATTENTION", "_NET_WM_STATE_FOCUSED",
        "_NET_WM_STRUT", "_NET_WM_STRUT_PARTIAL", "_NET_WM_ICON_GEOMETRY", "_NET_WM_ICON", "_NET_WM_FULL_PLACEMENT"};
    for (string atomstr : supported) {
        net_supp_fe.insert(get_atom(p->fconn, false, atomstr));
    }
}