#include "app.h"
#include <cstdio>
#include <glog/logging.h>
#include <signal.h>
#include <sys/syscall.h>

int main(int argc, char** argv) {
    // by default, don't log anything except 'FATAL' to stderr
    FLAGS_stderrthreshold = 3;
    google::InitGoogleLogging(argv[0]);
    LOG(INFO) << "app started.";
    pxwm::MainApp app;
    app.run(argc, argv);
    LOG(INFO) << "app exited gracefully.";
    return 0;
}
