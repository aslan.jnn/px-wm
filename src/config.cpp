#include "config.h"
#include "shared/i18n.h"
#include "shared/util.h"
#include <cstdlib>

// intialize hardcoded values in this program

using namespace pxwm::states;
using namespace px::util;
using namespace std;

struct ConstsData::Priv {
    map<string, ScalingAlgorithm> get_scale_algorithms_string_map();
    map<string, FrameRateMode> get_framerate_mode_string_map();
    map<string, SHMMode> get_shm_mode_string_map();
    map<ScalingAlgorithm, pair<uint8_t, uint8_t>> get_scale_alg_scale_limits();

    template<typename K>
    map<K, string> get_value_map_from_string_map(map<string, K> strmap) {
        map<K, string> ret;
        for (auto kvp1 : strmap) {
            ret[kvp1.second] = kvp1.first;
        }
        return ret;
    };

    Priv() = default;
    Priv(const Priv&) = delete;
    Priv& operator=(const Priv&) = delete;
};

ConstsData::ConstsData()
    : p(new Priv), scale_alg_scale_limits(p->get_scale_alg_scale_limits()),
      avail_scale_algorithms(p->get_scale_algorithms_string_map()),
      avail_framerate_modes(p->get_framerate_mode_string_map()), avail_shm_modes(p->get_shm_mode_string_map()),
      vmap_scale_algorithms(p->get_value_map_from_string_map(avail_scale_algorithms)),
      vmap_framerate_modes(p->get_value_map_from_string_map(avail_framerate_modes)),
      vmap_shm_modes(p->get_value_map_from_string_map(avail_shm_modes)),
      default_socketfile_path(get_default_socketfile_path(appname)) {}

const ConstsData Consts::values;

map<string, ScalingAlgorithm> ConstsData::Priv::get_scale_algorithms_string_map() {
    return {{"nearest", ScalingAlgorithm::NEAREST}, {"bilinear", ScalingAlgorithm::BILINEAR},
        {"raa", ScalingAlgorithm::RAA}};
}

map<string, FrameRateMode> ConstsData::Priv::get_framerate_mode_string_map() {
    return {{"low", FrameRateMode::LOW}, {"normal", FrameRateMode::NORMAL}, {"high", FrameRateMode::HIGH}};
}

map<string, SHMMode> ConstsData::Priv::get_shm_mode_string_map() {
    return {{"none", SHMMode::NONE}, {"image", SHMMode::IMAGE}, {"pixmap", SHMMode::PIXMAP}};
}

map<ScalingAlgorithm, pair<uint8_t, uint8_t>> ConstsData::Priv::get_scale_alg_scale_limits() {
    return {{ScalingAlgorithm::RAA, {1, 2}}};
}