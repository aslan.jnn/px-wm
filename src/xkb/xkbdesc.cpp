#include "xkb/xkbdesc.h"
#include "common/customxcb/customxcb.h"
#include "common/x11.h"
#include "shared/memory.h"
#include "shared/util.h"
#include <glog/logging.h>
#include <vector>
#include <xcb/xcb_util.h>

using namespace std;
using namespace px::util;
using namespace px::memory;
using namespace pxwm::common;
using namespace pxwm::common::customxcb;

namespace pxwm {
    namespace xkb {
        namespace desc {
            namespace priv {
                bool convert_kt_to_set_kt(xcb_xkb_get_map_reply_t* rep,
                    xcb_xkb_get_map_map_t& rep_data,
                    xcb_xkb_set_map_request_t& req,
                    xcb_xkb_set_map_values_t& send_data);
            }
        }
    }
}

/**
 * @brief convert xcb_xkb_key_type_t in rep_data types_rtrn and assign it to send_data types, which is
 * xcb_xkb_set_key_type_t.
 * send_data->types will be allocated regardless of the success status of this function. It is the caller's
 * responsibility to free send_data->types after use.
 */
bool pxwm::xkb::desc::priv::convert_kt_to_set_kt(xcb_xkb_get_map_reply_t* rep,
    xcb_xkb_get_map_map_t& rep_data,
    xcb_xkb_set_map_request_t& req,
    xcb_xkb_set_map_values_t& send_data) {
    // allocate set_key_types based on the size of received key_types (key_types is guaranteed to be the same size or
    // larger than set_key_types)
    auto rep_kt_it = xcb_xkb_get_map_map_types_rtrn_iterator(rep, &rep_data);
    auto rep_kt_it_end = xcb_xkb_key_type_end(rep_kt_it);
    size_t len = (size_t)rep_kt_it_end.data - (size_t)rep_data.types_rtrn;
    send_data.types = (xcb_xkb_set_key_type_t*)calloc(len, 1);
    // begin copy
    xcb_xkb_set_key_type_iterator_t req_kt_it;
    rep_kt_it = xcb_xkb_get_map_map_types_rtrn_iterator(rep, &rep_data);
    req_kt_it = xcb_xkb_set_map_values_types_iterator(&req, &send_data);
    // make sure what we get is what we expect
    if (rep_kt_it.rem != req_kt_it.rem) {
        return false;
    }
    // do the copying
    while (rep_kt_it.rem) {
        // base data
        req_kt_it.data->mask = rep_kt_it.data->mods_mask;
        req_kt_it.data->realMods = rep_kt_it.data->mods_mods;
        req_kt_it.data->virtualMods = rep_kt_it.data->mods_vmods;
        req_kt_it.data->numLevels = rep_kt_it.data->numLevels;
        req_kt_it.data->nMapEntries = rep_kt_it.data->nMapEntries;
        req_kt_it.data->preserve = rep_kt_it.data->hasPreserve;
        // map entries and preserves data
        if (req_kt_it.data->nMapEntries) {
            auto send_kt_ents = xcb_xkb_set_key_type_entries(req_kt_it.data);
            auto rep_kt_ents = xcb_xkb_key_type_map(rep_kt_it.data);
            xcb_xkb_kt_set_map_entry_t* send_kt_ps =
                req_kt_it.data->preserve ? xcb_xkb_set_key_type_preserve_entries(req_kt_it.data) : nullptr;
            xcb_xkb_mod_def_t* rep_kt_ps =
                req_kt_it.data->preserve ? xcb_xkb_key_type_preserve(rep_kt_it.data) : nullptr;
            for (int i = 0; i < req_kt_it.data->nMapEntries; ++i) {
                send_kt_ents[i].level = rep_kt_ents[i].level;
                send_kt_ents[i].realMods = rep_kt_ents[i].mods_mods;
                send_kt_ents[i].virtualMods = rep_kt_ents[i].mods_vmods;
                if (req_kt_it.data->preserve) {
                    send_kt_ps[i].level = rep_kt_ents[i].level;
                    send_kt_ps[i].realMods = rep_kt_ps[i].realMods;
                    send_kt_ps[i].virtualMods = rep_kt_ps[i].vmods;
                }
            }
        }
        // OK, move on!
        xcb_xkb_key_type_next(&rep_kt_it);
        xcb_xkb_set_key_type_next(&req_kt_it);
    }
    return true;
}

bool pxwm::xkb::desc::forward_xkb_map(xcb_connection_t* src_conn,
    xcb_connection_t* dst_conn,
    xcb_xkb_get_map_reply_t** out) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    UniquePointerWrapper_free<xcb_xkb_get_map_reply_t> rep_ctr;
    // retreive
    auto rep = xcb_xkb_get_map_reply(src_conn,
        xcb_xkb_get_map(src_conn, XCB_XKB_ID_USE_CORE_KBD, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        &err.pop());
    if (out != nullptr) {
        // it's the caller responsibility to free it
        *out = rep;
    } else {
        // if caller doesn't want the result, it's OUR responsibility!
        rep_ctr.pop() = rep;
    }
    if (err.get()) {
        LOG(ERROR) << "Cannot retrieve frontend's XKB map.";
        return false;
    }
    auto rep_buf = xcb_xkb_get_map_map(rep);
    xcb_xkb_get_map_map_t rep_data;
    xcb_xkb_get_map_map_unpack(rep_buf, rep->nTypes, rep->nKeySyms, rep->nKeyActions, rep->totalActions,
        rep->totalKeyBehaviors, rep->virtualMods, rep->totalKeyExplicit, rep->totalModMapKeys, rep->totalVModMapKeys,
        rep->present, &rep_data);

    // re-assemble
    xcb_xkb_set_map_values_t send_data;
    //    send_data.types = (xcb_xkb_set_key_type_t*)rep_data.types_rtrn;
    send_data.syms = rep_data.syms_rtrn;
    send_data.actionsCount = rep_data.acts_rtrn_count;
    send_data.actions = rep_data.acts_rtrn_acts;
    send_data.behaviors = rep_data.behaviors_rtrn;
    send_data.vmods = rep_data.vmods_rtrn;
    send_data.explicit1 = rep_data.explicit_rtrn;
    send_data.modmap = rep_data.modmap_rtrn;
    send_data.vmodmap = rep_data.vmodmap_rtrn;

    // build request header
    xcb_xkb_set_map_request_t reqb;
    auto req = &reqb;
    req->deviceSpec = XCB_XKB_ID_USE_CORE_KBD;
    req->length = rep->length;
    req->present = rep->present;
    req->minKeyCode = rep->minKeyCode;
    req->maxKeyCode = rep->maxKeyCode;
    req->firstType = rep->firstType;
    req->nTypes = rep->nTypes;
    req->firstKeySym = rep->firstKeySym;
    req->nKeySyms = rep->nKeySyms;
    req->totalSyms = rep->totalSyms;
    req->firstKeyAction = rep->firstKeyAction;
    req->nKeyActions = rep->nKeyActions;
    req->totalActions = rep->totalActions;
    req->firstKeyBehavior = rep->firstKeyBehavior;
    req->nKeyBehaviors = rep->nKeyBehaviors;
    req->totalKeyBehaviors = rep->totalKeyBehaviors;
    req->firstKeyExplicit = rep->firstKeyExplicit;
    req->nKeyExplicit = rep->nKeyExplicit;
    req->totalKeyExplicit = rep->totalKeyExplicit;
    req->firstModMapKey = rep->firstModMapKey;
    req->nModMapKeys = rep->nModMapKeys;
    req->totalModMapKeys = rep->totalModMapKeys;
    req->firstVModMapKey = rep->firstVModMapKey;
    req->nVModMapKeys = rep->nVModMapKeys;
    req->totalVModMapKeys = rep->totalVModMapKeys;
    req->virtualMods = rep->virtualMods;

    bool gst_status = priv::convert_kt_to_set_kt(rep, rep_data, reqb, send_data);
    // for automatic deletion of send_data->types after usage
    UniquePointerWrapper_free<xcb_xkb_set_key_type_t> send_data_types_cnt;
    send_data_types_cnt.pop() = send_data.types;
    if (!gst_status) {
        LOG(ERROR) << "Server returned invalid XKB map.";
        return false;
    }

    UniquePointerWrapper_free<void> send_buf;
    int serlen;
    serlen = xcb_custom_xkb_set_map_values_serialize(&send_buf.pop(), rep->nTypes, rep->nKeySyms, rep->nKeyActions,
        rep->totalActions, rep->totalKeyBehaviors, rep->virtualMods, rep->totalKeyExplicit, rep->totalModMapKeys,
        rep->totalVModMapKeys, rep->present, &send_data);
    auto req_cookie = xcb_custom_xkb_set_map_checked(dst_conn, reqb, send_buf.get(), serlen);
    err.pop() = xcb_request_check(dst_conn, req_cookie);
    if (err.get()) {
        LOG(ERROR) << string_printf(
            "Cannot set backend's XKB map (%s).", xcb_event_get_error_label(err.get()->error_code));
        return false;
    }
    return true;
}

bool pxwm::xkb::desc::forward_xkb_compat_map(xcb_connection_t* src_conn, xcb_connection_t* dst_conn) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    // retreive
    auto rep = make_unique_malloc(xcb_xkb_get_compat_map_reply(
        src_conn, xcb_xkb_get_compat_map(src_conn, XCB_XKB_ID_USE_CORE_KBD, 15, true, 0, 0), &err.pop()));
    if (err.get()) {
        LOG(ERROR) << "Cannot retrieve frontend's XKB compat map.";
        return false;
    }
    auto cm_si = xcb_xkb_get_compat_map_si_rtrn(rep.get());
    auto cm_gm = xcb_xkb_get_compat_map_group_rtrn(rep.get());
    // send
    err.pop() = xcb_request_check(dst_conn, xcb_xkb_set_compat_map_checked(dst_conn, XCB_XKB_ID_USE_CORE_KBD, true,
                                                true, rep->groupsRtrn, rep->firstSIRtrn, rep->nSIRtrn, cm_si, cm_gm));
    if (err.get()) {
        LOG(ERROR) << string_printf(
            "Cannot set backend's XKB compat map (%s).", xcb_event_get_error_label(err.get()->error_code));
        return false;
    }
    return true;
}

bool pxwm::xkb::desc::forward_xkb_names(xcb_connection_t* fconn, xcb_connection_t* bconn, int firstType) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    // retreive
    auto rep = make_unique_malloc(
        xcb_xkb_get_names_reply(fconn, xcb_xkb_get_names(fconn, XCB_XKB_ID_USE_CORE_KBD, 16383), &err.pop()));
    if (err.get()) {
        LOG(ERROR) << "Cannot retrieve frontend's XKB names.";
        return false;
    }
    xcb_xkb_get_names_value_list_t rep_data;
    xcb_xkb_get_names_value_list_unpack(xcb_xkb_get_names_value_list(rep.get()), rep->nTypes, rep->indicators,
        rep->virtualMods, rep->groupNames, rep->nKeys, rep->nKeyAliases, rep->nRadioGroups, rep->which, &rep_data);

    // prepare to send
    xcb_xkb_set_names_values_t send_data;
    send_data.keycodesName = rep_data.keycodesName;
    send_data.geometryName = rep_data.geometryName;
    send_data.symbolsName = rep_data.symbolsName;
    send_data.physSymbolsName = rep_data.physSymbolsName;
    send_data.typesName = rep_data.typesName;
    send_data.compatName = rep_data.compatName;
    send_data.typeNames = rep_data.typeNames;
    send_data.nLevelsPerType = rep_data.nLevelsPerType;
    send_data.ktLevelNames = rep_data.ktLevelNames;
    send_data.indicatorNames = rep_data.indicatorNames;
    send_data.virtualModNames = rep_data.virtualModNames;
    send_data.groups = rep_data.groups;
    send_data.keyNames = rep_data.keyNames;
    send_data.keyAliases = rep_data.keyAliases;
    send_data.radioGroupNames = rep_data.radioGroupNames;
    if (!convert_xkb_names_atoms(fconn, bconn, *rep, send_data)) {
        LOG(ERROR) << "Error while converting XKB names atoms.";
        return false;
    }

    UniquePointerWrapper_free<void> send_buf;

    // first 4 types can't be set
    int firstLevelType = firstType;
    int nTypes = rep->nTypes;
    constexpr int LAST_REQUIRED_TYPE_ID = 3;
    if (firstType <= LAST_REQUIRED_TYPE_ID) {
        int adjust;

        adjust = LAST_REQUIRED_TYPE_ID - firstType + 1;
        firstType += adjust;
        send_data.typeNames += adjust;
        nTypes -= adjust;
        if (nTypes < 1) {
            nTypes = firstType = 0;
            if (rep->which & XCB_XKB_NAME_DETAIL_KEY_TYPE_NAMES) {
                rep->which = rep->which ^ XCB_XKB_NAME_DETAIL_KEY_TYPE_NAMES;
            }
        }
    }

    // do the sending
    int serlen = xcb_custom_xkb_set_names_values_serialize(&send_buf.pop(), nTypes, rep->nTypes, rep->indicators,
        rep->virtualMods, rep->groupNames, rep->nKeys, rep->nKeyAliases, rep->nRadioGroups, rep->which, &send_data);
    err.pop() = xcb_request_check(
        bconn, xcb_custom_xkb_set_names_checked(bconn, XCB_XKB_ID_USE_CORE_KBD, rep->virtualMods, rep->which, firstType,
                   nTypes, firstLevelType, rep->nTypes, rep->indicators, rep->groupNames, rep->nRadioGroups,
                   rep->firstKey, rep->nKeys, rep->nKeyAliases, rep->nKTLevels, send_buf.get(), serlen));
    if (err.get()) {
        string_printf("Cannot set backend's XKB names (%s).", xcb_event_get_error_label(err.get()->error_code));
        return false;
    }
    return true;
}

bool pxwm::xkb::desc::convert_xkb_names_atoms(xcb_connection_t* src_conn,
    xcb_connection_t* dst_conn,
    xcb_xkb_get_names_reply_t& src_rep,
    xcb_xkb_set_names_values_t& dst_data) {
    static constexpr int NUM_VMODS = 16, NUM_INDICATORS = 32, NUM_KB_GROUPS = 4;
    // pointer of what to be changed
    vector<xcb_atom_t*> to_change;
    to_change.push_back(&dst_data.keycodesName);
    to_change.push_back(&dst_data.geometryName);
    to_change.push_back(&dst_data.symbolsName);
    to_change.push_back(&dst_data.physSymbolsName);
    to_change.push_back(&dst_data.typesName);
    to_change.push_back(&dst_data.compatName);
    for (int i = 0; i < src_rep.nTypes; ++i) {
        to_change.push_back(&dst_data.typeNames[i]);
    }
    for (int i = 0; i < src_rep.nKTLevels; ++i) {
        to_change.push_back(&dst_data.ktLevelNames[i]);
    }
    // only include those specified in flags
    uint32_t flag_sel, i;
    for (i = 0, flag_sel = 1; i < NUM_INDICATORS; ++i, flag_sel <<= 1) {
        if (src_rep.indicators & flag_sel) {
            to_change.push_back(&dst_data.indicatorNames[i]);
        }
    }
    for (i = 0, flag_sel = 1; i < NUM_VMODS; ++i, flag_sel <<= 1) {
        if (src_rep.virtualMods & flag_sel) {
            to_change.push_back(&dst_data.virtualModNames[i]);
        }
    }
    for (i = 0, flag_sel = 1; i < NUM_KB_GROUPS; ++i, flag_sel <<= 1) {
        if (src_rep.groupNames & flag_sel) {
            to_change.push_back(&dst_data.groups[i]);
        }
    }
    for (int i = 0; i < src_rep.nRadioGroups; ++i) {
        to_change.push_back(&dst_data.radioGroupNames[i]);
    }

    // Note that this will be a little bit slow due to the sheer amount of requests. But subsequent requests will be
    // cached by common::x11 :)
    int atom_count = to_change.size();
    for (int i = 0; i < atom_count; ++i) {
        xcb_atom_t& atom = *to_change[i];
        if (atom) {
            atom = x11::get_atom(dst_conn, false, x11::get_atom_name(src_conn, atom));
            // something MUST be wrong if previously nonzero atom became zero
            if (atom == XCB_ATOM_NONE) {
                return false;
            }
        }
    }

    return true;
}