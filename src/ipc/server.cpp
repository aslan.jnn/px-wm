#include "ipc/server.h"
#include "app.h"
#include "common/configparsers.h"
#include "common/stringconsts.h"
#include "config.h"
#include "ipc/consts.h"
#include "shared/dynamicnulldelimiterbuilder.h"
#include "shared/i18n.h"
#include "shared/nulldelimiterbuilder.h"
#include "shared/nulltoken.h"
#include "shared/util.h"
#include <algorithm>
#include <assert.h>
#include <atomic>
#include <functional>
#include <glog/logging.h>
#include <signal.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/un.h>
#include <syscall.h>
#include <thread>
#include <unistd.h>
#include <unordered_map>

using namespace std;
using namespace pxwm::ipc;
using namespace pxwm::common::configparsers;
using namespace px;
using namespace px::util;

class Server::Priv {
public:
    string socketfile;
    const shared_ptr<pxwm::backend::Backend> be_inst;
    int socket_fd;
    thread server_thread_hdl;
    struct {
        atomic<bool> req_stop;
        atomic<int> tid;
    } itc_server_thread;
    unique_ptr<MsgHandlers> msg_hdlrs;

    Priv(Server* rm, shared_ptr<pxwm::backend::Backend> be_inst);
    Priv(const Priv&) = delete;
    Priv& operator=(const Priv&) = delete;

    unordered_map<string, function<NullDelimiterBuilder(shared_ptr<NullToken>)>> generate_fn_mapping();

    void start_server_thread();
    void server_thread();
    void handle_comm_msg(int comm_fd);

private:
    Server* parent;
};

class Server::MsgHandlers {
public:
    MsgHandlers(Server::Priv* parent);
    ~MsgHandlers() {}
    MsgHandlers(const MsgHandlers&) = delete;
    MsgHandlers& operator=(const MsgHandlers&) = delete;

    NullDelimiterBuilder h_get_info(shared_ptr<NullToken> tkz);
    NullDelimiterBuilder h_get_bk_list(shared_ptr<NullToken> tkz);
    NullDelimiterBuilder h_get_bk_info(shared_ptr<NullToken> tkz);
    NullDelimiterBuilder h_add_bk(shared_ptr<NullToken> tkz);
    NullDelimiterBuilder h_dc_bk(shared_ptr<NullToken> tkz);
    NullDelimiterBuilder h_get_windows(shared_ptr<NullToken> tkz);
    NullDelimiterBuilder h_rescale_bk(shared_ptr<NullToken> tkz);

private:
    shared_ptr<pxwm::backend::Backend> be;
    const Server::Priv* parent;
};

Server::Server(string socketfile, shared_ptr<pxwm::backend::Backend> be_inst) : server_running(false) {
    p = make_unique<Priv>(this, be_inst);
    p->socketfile = socketfile;

    // open a nonblocking socket file
    p->socket_fd = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if (p->socket_fd < 0) {
        perror(_("Cannot open socket"));
        LOG(ERROR) << "Cannot open socket: " << errno;
        return;
    }

    sockaddr_un addr;
    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, p->socketfile.c_str());

    // bind
    for (int i = 0; i <= 1; ++i) {
        int bind_stat = bind(p->socket_fd, (sockaddr*)&addr, sizeof(sockaddr_un));
        if (bind_stat < 0) {
            if (i) {
                // unrecoverable failure. cannot bind at all.
                perror(_("Cannot bind socket"));
                LOG(ERROR) << "Cannot bind socket: " << errno;
                return;
            } else {
                // retry by unlink the file first
                if (unlink(p->socketfile.c_str()) < 0) {
                    perror(_("Cannot bind socket"));
                    LOG(ERROR) << "Cannot bind socket after unlinking and retrying: " << errno;
                    return;
                }
            }
        } else {
            // bind success!
            break;
        }
    }

    // listen
    if (listen(p->socket_fd, 2) < 0) {
        perror(_("Cannot listen to socket"));
        LOG(ERROR) << "Cannot listen to socket: " << errno;
        return;
    }

    // bind success if code past this point. start a new listener thread.
    p->start_server_thread();
    server_running = true;
}

Server::~Server() {
    // tell the thread that we want to stop
    p->itc_server_thread.req_stop.store(true, memory_order_relaxed);
    // send the signal so that accept() would unblock
    syscall(SYS_tgkill, getpid(), p->itc_server_thread.tid.load(memory_order_relaxed), SIGUSR1);
    // wait
    if (p->server_thread_hdl.joinable()) {
        p->server_thread_hdl.join();
    }
    // close and delete socket file
    close(p->socket_fd);
    unlink(p->socketfile.c_str());
}

Server::Priv::Priv(Server* rm, shared_ptr<pxwm::backend::Backend> be_inst) : parent(rm), be_inst(be_inst) {
    msg_hdlrs = make_unique<MsgHandlers>(this);
}

unordered_map<string, function<NullDelimiterBuilder(shared_ptr<NullToken>)>> Server::Priv::generate_fn_mapping() {
    unordered_map<string, function<NullDelimiterBuilder(shared_ptr<NullToken>)>> ret;
    ret["info"] = std::bind(&MsgHandlers::h_get_info, msg_hdlrs.get(), placeholders::_1);
    ret["get_bk_list"] = std::bind(&MsgHandlers::h_get_bk_list, msg_hdlrs.get(), placeholders::_1);
    ret["get_bk_info"] = std::bind(&MsgHandlers::h_get_bk_info, msg_hdlrs.get(), placeholders::_1);
    ret["add_bk"] = std::bind(&MsgHandlers::h_add_bk, msg_hdlrs.get(), placeholders::_1);
    ret["dc_bk"] = std::bind(&MsgHandlers::h_dc_bk, msg_hdlrs.get(), placeholders::_1);
    ret["get_windows"] = std::bind(&MsgHandlers::h_get_windows, msg_hdlrs.get(), placeholders::_1);
    ret["rescale_bk"] = std::bind(&MsgHandlers::h_rescale_bk, msg_hdlrs.get(), placeholders::_1);
    return ret;
}

void Server::Priv::server_thread() {
    // for signaling purposes
    itc_server_thread.tid.store(syscall(SYS_gettid), memory_order_relaxed);

    //(socket) fds to listen to
    fd_set fdset;
    timespec timeout;
    timeout.tv_nsec = 0;
    timeout.tv_sec = 30;

    // the main loop
    int fdsel;
    while (!itc_server_thread.req_stop.load(memory_order_relaxed)) {
        // wait
        FD_ZERO(&fdset);
        FD_SET(socket_fd, &fdset);
        fdsel = pselect(socket_fd + 1, &fdset, nullptr, nullptr, &timeout, nullptr);
        if (fdsel == -1) {
            LOG(ERROR) << "Error while waiting for socket.";
            sleep(1);
        }
        // open
        int comm_fd = accept(socket_fd, nullptr, nullptr);
        if (comm_fd < 0) {
            // errno == EWOULDBLOCK implies that the queue is empty.
            if (errno != EWOULDBLOCK) {
                LOG(WARNING) << string_printf("Error accepting socket: %d (%s)", errno, strerror(errno));
            }
        } else {
            handle_comm_msg(comm_fd);
            close(comm_fd);
            continue;
        }
        // does I wake up because something wants me to exit?
        if (itc_server_thread.req_stop.load(memory_order_relaxed)) {
            break;
        }
    }
}

void Server::Priv::start_server_thread() {
    itc_server_thread.req_stop.store(false, memory_order_relaxed);
    server_thread_hdl = thread(&Priv::server_thread, this);
}

void Server::Priv::handle_comm_msg(int comm_fd) {
    static constexpr int MAX_CMD_SIZE = 256;
    static auto parse_fnmap = generate_fn_mapping();
    // expect to read
    uint32_t expectrd;
    char ibuff[MAX_CMD_SIZE];
    // First read may be interrupted by a signal. Retry until we can read the first 4 bytes.
    int nrd = -1;
    do {
        nrd = read(comm_fd, &expectrd, 4);
    } while (nrd < 0 && errno == EINTR);
    if (nrd != 4) {
        LOG(ERROR) << string_printf("Invalid socket expectrd read size. Expect 4, got %d", nrd);
        return;
    }
    if (expectrd > MAX_CMD_SIZE) {
        LOG(ERROR) << "expectrd too big";
        return;
    }

    // read
    nrd = read(comm_fd, ibuff, sizeof(ibuff));
    if (nrd != expectrd) {
        LOG(ERROR) << "Invalid socket input buffer read size";
        return;
    }
    // avoid parser or string reader overrun
    ibuff[sizeof(ibuff) - 1] = 0;

    shared_ptr<NullToken> tkzer = make_shared<NullToken>(ibuff, nrd);
    NullDelimiterBuilder ret_bld;

    const char* token = tkzer->next_token();
    if (token != nullptr) {
        auto fn_it = parse_fnmap.find(token);
        if (fn_it == parse_fnmap.end()) {
            LOG(WARNING) << "Unknown command received from socket";
            ret_bld = NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::UNKNOWN_CMD]);
        } else {
            ret_bld = fn_it->second(tkzer);
        }
    }

    // make sure we don't return empty response
    assert(ret_bld.size() > 0);
    // send response to client
    uint32_t ret_sz = ret_bld.size();
    if (write(comm_fd, &ret_sz, 4) < 4) {
        puts(stringconsts::values().RESPONSE_TRUNCATED);
        LOG(WARNING) << "IPC server output truncated";
    }
    if (write(comm_fd, ret_bld.get_buffer(), ret_bld.size()) < ret_bld.size()) {
        puts(stringconsts::values().RESPONSE_TRUNCATED);
        LOG(WARNING) << "IPC server output truncated";
    }
}

Server::MsgHandlers::MsgHandlers(Server::Priv* parent) : parent{parent}, be{parent->be_inst} {}

NullDelimiterBuilder Server::MsgHandlers::h_get_info(shared_ptr<NullToken> tkz) {
    DynamicNullDelimiterBuilder ret_bld;
    ret_bld.add_token("prerelease");
    ret_bld.add_token(to_string(VERSION_ORD));
    ret_bld.add_token(to_string(ConstData::IPC_API_VERSION));
    return ret_bld.build();
}

NullDelimiterBuilder Server::MsgHandlers::h_get_bk_list(shared_ptr<NullToken> tkz) {
    // returns: <no of displays>,display 1,display 2,...,display n
    DynamicNullDelimiterBuilder ret_bld;
    auto dpys = be->get_managed_bk_displays();
    ret_bld.add_token(to_string(dpys.size()));
    for (auto dpy : dpys) {
        ret_bld.add_token(dpy);
    }
    return ret_bld.build();
}

NullDelimiterBuilder Server::MsgHandlers::h_get_bk_info(shared_ptr<NullToken> tkz) {
    auto dpy_name = tkz->next_token();
    if (dpy_name == nullptr) {
        return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::INVALID_PARAM]);
    }
    auto options = be->get_bk_display_info(dpy_name);
    if (!options.first) {
        return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::NO_DISPLAY]);
    }
    DynamicNullDelimiterBuilder ret_bld;
    ret_bld.add_token(to_string(options.second.scale_factor));
    ret_bld.add_token(states::Consts::values.vmap_scale_algorithms.at(options.second.scale_algorithm));
    ret_bld.add_token(states::Consts::values.vmap_framerate_modes.at(options.second.framerate_mode));
    return ret_bld.build();
}

NullDelimiterBuilder Server::MsgHandlers::h_add_bk(shared_ptr<NullToken> tkz) {
    puts(_("IPC requested to add display."));
    auto dpy_name = tkz->next_token();
    if (dpy_name == nullptr) {
        return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::INVALID_PARAM]);
    } else {
        // format of the value follows command line argument.
        pxwm::states::BkFrontEndOptions addbk_opts;
        // parse arguments based on order specified at BkFrontEndOptions.
        for (int i = 0; i <= 2; ++i) {
            auto arg = tkz->next_token();
            if (arg == nullptr) {
                return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::INVALID_PARAM]);
            }
            bool subp_status = false;
            switch (i) {
                case 0:
                    subp_status = BkFrontEndOptionsParser::parse_default_scale_factor(arg, addbk_opts.scale_factor);
                    break;
                case 1:
                    subp_status = BkFrontEndOptionsParser::parse_scale_algorithm(arg, addbk_opts.scale_algorithm);
                    break;
                case 2:
                    subp_status = BkFrontEndOptionsParser::parse_framerate_mode(arg, addbk_opts.framerate_mode);
                    break;
            }
            if (!subp_status) {
                return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::INVALID_VALUE]);
            }
        }
        if (!OptionsValidator::check_scaler_scale(addbk_opts.scale_algorithm, addbk_opts.scale_factor)) {
            return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::INVALID_VALUE]);
        }
        bool stat = be->request_add_display(dpy_name, addbk_opts);
        return NullDelimiterBuilder(
            ipc_consts().serverret_value_map[stat ? ConstData::ServerRet::SUCCESS : ConstData::ServerRet::FAIL]);
    }
}

NullDelimiterBuilder Server::MsgHandlers::h_dc_bk(shared_ptr<NullToken> tkz) {
    puts(_("IPC requested to disconnect display."));
    auto param = tkz->next_token();
    auto param2 = tkz->next_token();
    if (param == nullptr) {
        return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::INVALID_PARAM]);
    } else if (strcmp(param, "all") == 0 && param2 != nullptr && strcmp(param2, "all") == 0) {
        // disconnect all
        bool rs = true;
        for (auto dpy : be->get_managed_bk_displays()) {
            rs = be->request_remove_bk_display(dpy) && rs;
        }
        return NullDelimiterBuilder(
            ipc_consts().serverret_value_map[rs ? ConstData::ServerRet::SUCCESS : ConstData::ServerRet::FAIL]);
    } else {
        bool stat = be->request_remove_bk_display(param);
        return NullDelimiterBuilder(
            ipc_consts().serverret_value_map[stat ? ConstData::ServerRet::SUCCESS : ConstData::ServerRet::NO_DISPLAY]);
    }
}

NullDelimiterBuilder Server::MsgHandlers::h_get_windows(shared_ptr<NullToken> tkz) {
    auto param = tkz->next_token();
    if (!param) {
        return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::INVALID_PARAM]);
    }
    auto ret = be->get_managed_windows(param);
    if (ret.first) {
        DynamicNullDelimiterBuilder ret_bld;
        ret_bld.add_token(to_string(ret.second.size()));
        for (auto p : ret.second) {
            ret_bld.add_token(to_string(p.first));
            ret_bld.add_token(to_string(p.second));
        }
        return ret_bld.build();
    } else {
        return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::NO_DISPLAY]);
    }
}

NullDelimiterBuilder Server::MsgHandlers::h_rescale_bk(shared_ptr<NullToken> tkz) {
    puts(_("IPC requested to rescale display."));
    auto bkdpy = tkz->next_token();
    auto scfactor_str = tkz->next_token();
    if (!bkdpy || !scfactor_str) {
        return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::INVALID_PARAM]);
    }
    uint16_t scfactor;
    if (!BkFrontEndOptionsParser::parse_default_scale_factor(scfactor_str, scfactor)) {
        return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::INVALID_VALUE]);
    }
    // retrieve existing backend configuration, and compare if it's scaling algorithm is compatible with the new scale
    // factor.
    auto options = be->get_bk_display_info(bkdpy);
    if (!options.first) {
        return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::NO_DISPLAY]);
    }
    if (!OptionsValidator::check_scaler_scale(options.second.scale_algorithm, scfactor)) {
        return NullDelimiterBuilder(ipc_consts().serverret_value_map[ConstData::ServerRet::INVALID_VALUE]);
    }
    // OK
    bool stat = be->request_rescale_bk_display(bkdpy, scfactor);
    return NullDelimiterBuilder(
        ipc_consts().serverret_value_map[stat ? ConstData::ServerRet::SUCCESS : ConstData::ServerRet::NO_DISPLAY]);
}