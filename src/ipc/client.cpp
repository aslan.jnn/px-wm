#include "ipc/client.h"
#include "common/stringconsts.h"
#include "config.h"
#include "ipc/consts.h"
#include "shared/arraytoken.h"
#include "shared/dynamicnulldelimiterbuilder.h"
#include "shared/i18n.h"
#include "shared/memory.h"
#include "shared/nulldelimiterbuilder.h"
#include "shared/nulltoken.h"
#include "shared/util.h"
#include <cstdio>
#include <functional>
#include <map>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

using namespace std;
using namespace px;
using namespace px::util;
using namespace px::memory;
using namespace pxwm;
using namespace pxwm::ipc;

class Client::Priv {
public:
    enum class OutputMode { RAW, INTERACTIVE };
    OutputMode output_mode = OutputMode::INTERACTIVE;
    string socketfile;

    // returns pair of size and data pointer
    pair<uint32_t, shared_ptr<char>> request_server(NullDelimiterBuilder bld);
    bool parse_mgmt_output_mode(ArrayToken* tkzer);
    static void print_raw_null_token(NullToken* tkzer);
    static void interactive_serverret_print(ConstData::ServerRet ret);

    void cmd_info(ArrayToken* tkzer);
    void cmd_backend_list(ArrayToken* tkzer);
    void cmd_backend_info(ArrayToken* tkzer);
    void cmd_add_backend(ArrayToken* tkzer);
    void cmd_remove_backend(ArrayToken* tkzer, bool all);
    void cmd_get_windows(ArrayToken* tkzer);
    void cmd_rescale_backend(ArrayToken* tkzer);
};

Client::Client(string socketfile) {
    p = make_unique<Priv>();
    p->socketfile = socketfile;
}

Client::~Client() {}

void Client::run_cmdline_args(int argc, char** argv) {
    ArrayToken tkzer(argc, argv);
    // skip the executable name
    tkzer.next_token();

    // token list
    static constexpr int T_OUTPUT_OPT = 1, T_CMD_INFO = 2, T_CMD_BACKEND_LIST = 3, T_CMD_BACKEND_INFO = 4,
                         T_CMD_ADD_BACKEND = 5, T_CMD_REMOVE_BACKEND = 6, T_CMD_REMOVE_ALL_BACKEND = 7,
                         T_CMD_GET_WINDOWS = 8, T_CMD_RESCALE_BACKEND = 9;
    typedef map<string, int> token_table_t;
    static token_table_t token_table{{"--outputmode", T_OUTPUT_OPT}, {"info", T_CMD_INFO},
        {"backend-list", T_CMD_BACKEND_LIST}, {"backend-info", T_CMD_BACKEND_INFO}, {"add-backend", T_CMD_ADD_BACKEND},
        {"remove-backend", T_CMD_REMOVE_BACKEND}, {"remove-all-backend", T_CMD_REMOVE_ALL_BACKEND},
        {"windows-list", T_CMD_GET_WINDOWS}, {"rescale-backend", T_CMD_RESCALE_BACKEND}};
    // command list
    typedef map<int, function<void(Client::Priv*, ArrayToken*)>> command_handler_t;
    static command_handler_t command_handlers{{T_CMD_INFO, &Priv::cmd_info},
        {T_CMD_BACKEND_LIST, &Priv::cmd_backend_list}, {T_CMD_BACKEND_INFO, &Priv::cmd_backend_info},
        {T_CMD_ADD_BACKEND, &Priv::cmd_add_backend},
        {T_CMD_REMOVE_BACKEND, std::bind(&Priv::cmd_remove_backend, placeholders::_1, placeholders::_2, false)},
        {T_CMD_REMOVE_ALL_BACKEND, std::bind(&Priv::cmd_remove_backend, placeholders::_1, placeholders::_2, true)},
        {T_CMD_GET_WINDOWS, &Priv::cmd_get_windows}, {T_CMD_RESCALE_BACKEND, &Priv::cmd_rescale_backend}};

    // state machine
    char* token;
    bool cmd_run = false;
    bool has_error = false;
    while ((token = tkzer.next_token())) {
        token_table_t::iterator tk_iter;
        if (check_and_get_mapped_value(token_table, string(token), tk_iter)) {
            switch (tk_iter->second) {
                case T_OUTPUT_OPT:
                    if (!p->parse_mgmt_output_mode(&tkzer)) {
                        has_error = true;
                    }
                    break;
                default: {
                    // run command
                    command_handler_t::iterator cmhd_iter;
                    if (check_and_get_mapped_value(command_handlers, tk_iter->second, cmhd_iter)) {
                        cmd_run = true;
                        cmhd_iter->second(p.get(), &tkzer);
                    }
                    break;
                }
            }
            // run command only once. let the parser handlers tell the errors.
            if (has_error || cmd_run) {
                break;
            }
        }
    }
    // must run cmd once
    if (!cmd_run && !has_error) {
        fputs(_("Please specify a command for management mode."), stdout);
        fputc(' ', stdout);
        fputsn(stringconsts::values().PLEASE_READ_HELP, stdout);
    }
}

pair<uint32_t, shared_ptr<char>> Client::Priv::request_server(NullDelimiterBuilder bld) {
    const pair<uint32_t, shared_ptr<char>> NONE_RET = {0, nullptr};
    // initialize socket
    int socket_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (socket_fd < 0) {
        perror(_("Error opening socket"));
        return NONE_RET;
    }
    // RAII socket free-er
    DeleteInvoker socket_closer([&socket_fd]() { close(socket_fd); });
    // connect to server
    sockaddr_un client_addr;
    client_addr.sun_family = AF_UNIX;
    strcpy(client_addr.sun_path, socketfile.c_str());
    if (connect(socket_fd, (sockaddr*)&client_addr, sizeof(sockaddr_un)) < 0) {
        perror(_("Cannot connect to IPC server"));
        return NONE_RET;
    }
    // send
    int written;
    uint32_t wsz = bld.size();
    written = write(socket_fd, &wsz, 4);
    if (written < 4) {
        fputsn(stringconsts::values().SOCKET_INCOMPLETE_WRITE_CLIENT, stderr);
    }
    written = write(socket_fd, bld.get_buffer(), wsz);
    if (written < wsz) {
        fputsn(stringconsts::values().SOCKET_INCOMPLETE_WRITE_CLIENT, stderr);
    }
    // server's reply (size)
    int amm_read;
    uint32_t rep_sz;
    amm_read = read(socket_fd, &rep_sz, 4);
    if (amm_read < 4) {
        fputsn(stringconsts::values().INVALID_RESPONSE, stderr);
        return NONE_RET;
    }
    // full response
    auto rep_buff_ctr = make_shared_malloc((char*)malloc(rep_sz + 1));
    amm_read = read(socket_fd, rep_buff_ctr.get(), rep_sz);
    if (amm_read < rep_sz) {
        fputsn(stringconsts::values().RESPONSE_TRUNCATED, stderr);
    }
    // return the raw server reply to caller
    return {amm_read, rep_buff_ctr};
}

bool Client::Priv::parse_mgmt_output_mode(ArrayToken* tkzer) {
    if (tkzer->is_end()) {
        puts(_("Please specify output mode."));
        return false;
    }
    typedef map<string, OutputMode> token_table_t;
    static token_table_t token_table{{"raw", OutputMode::RAW}, {"interactive", OutputMode::INTERACTIVE}};
    token_table_t::iterator tk_iter;
    if (check_and_get_mapped_value(token_table, tkzer->next_token(), tk_iter)) {
        output_mode = tk_iter->second;
        return true;
    } else {
        puts(_("Invalid output mode."));
        return false;
    }
}

void Client::Priv::print_raw_null_token(NullToken* tkzer) {
    while (!tkzer->is_end()) {
        puts(tkzer->next_token());
    }
}

void Client::Priv::interactive_serverret_print(ConstData::ServerRet ret) {
    switch (ret) {
        case ConstData::ServerRet::SUCCESS:
            puts(_("Operation completed successfully."));
            break;
        case ConstData::ServerRet::FAIL:
            fputs(_("Operation failed."), stdout);
            fputc(' ', stdout);
            puts("Check server's output for more details.");
            break;
        case ConstData::ServerRet::UNKNOWN_CMD:
            puts(_("Server doesn't recognize command."));
            break;
        case ConstData::ServerRet::INVALID_PARAM:
            puts(_("Invalid parameter specified."));
            break;
        case ConstData::ServerRet::NO_DISPLAY:
            puts(_("Specified target display doesn't exists."));
            break;
        case ConstData::ServerRet::INVALID_VALUE:
            fputs(_("Specified value isn't valid."), stdout);
            fputc(' ', stdout);
            puts("Check server's output for more details.");
            break;
        default:
            // don't know
            break;
    }
}

void Client::Priv::cmd_info(ArrayToken* tkzer) {
    DynamicNullDelimiterBuilder bld;
    bld.add_token("info");
    auto ret = request_server(bld.build());
    NullToken tkret(ret.second.get(), ret.first);
    if (output_mode == OutputMode::RAW) {
        print_raw_null_token(&tkret);
    } else {
        puts(_("Server Info"));
        printf(_("Version: %1$s"), (tkret.is_end() ? _("(Undefined)") : tkret.next_token()));
        puts("");
        printf(_("Build: %1$s"), (tkret.is_end() ? _("(Undefined)") : tkret.next_token()));
        puts("");
        printf(_("IPC API version: %1$s"), (tkret.is_end() ? _("(Undefined)") : tkret.next_token()));
        puts("");
        puts(_("To view this client's info, please specify '--version' argument."));
    }
}

void Client::Priv::cmd_backend_list(ArrayToken* tkzer) {
    DynamicNullDelimiterBuilder bld;
    bld.add_token("get_bk_list");
    auto ret = request_server(bld.build());
    NullToken tkret(ret.second.get(), ret.first);
    if (output_mode == OutputMode::RAW) {
        print_raw_null_token(&tkret);
    } else if (tkret.is_end()) {
        puts(_("No backend display is currently managed."));
    } else {
        printf(_("Currently managing %1$s backend displays:"), tkret.next_token());
        putchar('\n');
        while (!tkret.is_end()) {
            puts(tkret.next_token());
        }
    }
}

void Client::Priv::cmd_backend_info(ArrayToken* tkzer) {
    auto display = tkzer->next_token();
    DynamicNullDelimiterBuilder bld;
    bld.add_token("get_bk_info");
    if (display) {
        bld.add_token(display);
    }
    auto ret = request_server(bld.build());
    NullToken tkret(ret.second.get(), ret.first);
    if (output_mode == OutputMode::RAW) {
        print_raw_null_token(&tkret);
    } else {
        auto resp = tkret.next_token();
        if (resp == nullptr) {
            puts(stringconsts::values().EMPTY_RESPONSE);
        } else if (ipc_consts().serverret_string_map.count(resp)) {
            interactive_serverret_print(ipc_consts().serverret_string_map[resp]);
        } else {
            printf(_("Information for managed display %1$s"), display);
            putchar('\n');
            printf(_("Scaling factor: %1$sx"), resp);
            putchar('\n');
            printf(_("Scaling algorithm: %1$s"), tkret.is_end() ? _("(Undefined)") : tkret.next_token());
            putchar('\n');
            printf(_("Framerate mode: %1$s"), tkret.is_end() ? _("(Undefined)") : tkret.next_token());
            putchar('\n');
        }
    }
}

void Client::Priv::cmd_add_backend(ArrayToken* tkzer) {
    pxwm::states::BkFrontEndOptions default_opts;
    auto display = tkzer->next_token();
    // prevent deallocation on access
    string default_scale_factor = to_string(default_opts.scale_factor);
    const char* scale_factor = tkzer->is_end() ? default_scale_factor.c_str() : tkzer->next_token();
    const char* scale_alg = tkzer->is_end() ?
        pxwm::states::Consts::values.vmap_scale_algorithms.find(default_opts.scale_algorithm)->second.c_str() :
        tkzer->next_token();
    const char* framerate_mode = tkzer->is_end() ?
        pxwm::states::Consts::values.vmap_framerate_modes.find(default_opts.framerate_mode)->second.c_str() :
        tkzer->next_token();

    if (output_mode == OutputMode::INTERACTIVE && display) {
        printf(_("Request to add display '%1$s' with the following options:"), display);
        putchar('\n');
        printf(_("Scaling factor: %1$sx"), scale_factor);
        putchar('\n');
        printf(_("Scaling algorithm: %1$s"), scale_alg);
        putchar('\n');
        printf(_("Framerate mode: %1$s"), framerate_mode);
        putchar('\n');
    }

    DynamicNullDelimiterBuilder bld;
    bld.add_token("add_bk");
    if (display) {
        bld.add_token(display);
        bld.add_token(scale_factor);
        bld.add_token(scale_alg);
        bld.add_token(framerate_mode);
    }
    auto ret = request_server(bld.build());
    NullToken tkret(ret.second.get(), ret.first);
    if (output_mode == OutputMode::RAW) {
        print_raw_null_token(&tkret);
    } else {
        auto resp = tkret.next_token();
        if (ipc_consts().serverret_string_map.count(resp)) {
            interactive_serverret_print(ipc_consts().serverret_string_map[resp]);
        } else {
            puts(stringconsts::values().INVALID_RESPONSE);
        }
    }
}

void Client::Priv::cmd_remove_backend(ArrayToken* tkzer, bool all) {
    auto display = tkzer->next_token();
    DynamicNullDelimiterBuilder bld;
    bld.add_token("dc_bk");
    if (all) {
        bld.add_token("all");
        bld.add_token("all");
    } else if (display) {
        bld.add_token(display);
    }
    auto ret = request_server(bld.build());
    NullToken tkret(ret.second.get(), ret.first);
    if (output_mode == OutputMode::RAW) {
        print_raw_null_token(&tkret);
    } else {
        auto resp = tkret.next_token();
        if (ipc_consts().serverret_string_map.count(resp)) {
            interactive_serverret_print(ipc_consts().serverret_string_map[resp]);
        } else {
            puts(stringconsts::values().INVALID_RESPONSE);
        }
    }
}

void Client::Priv::cmd_get_windows(ArrayToken* tkzer) {
    auto display = tkzer->next_token();
    DynamicNullDelimiterBuilder bld;
    bld.add_token("get_windows");
    if (display) {
        bld.add_token(display);
    }
    auto ret = request_server(bld.build());
    NullToken tkret(ret.second.get(), ret.first);
    if (output_mode == OutputMode::RAW) {
        print_raw_null_token(&tkret);
    } else {
        auto resp = tkret.next_token();
        if (ipc_consts().serverret_string_map.count(resp)) {
            interactive_serverret_print(ipc_consts().serverret_string_map[resp]);
        } else {
            if (!resp) {
                puts(stringconsts::values().INVALID_RESPONSE);
                return;
            }
            printf(_("Currently managing %1$s windows on backend display '%2$s'."), resp, display);
            putchar('\n');
            int amm = atoi(resp);
            if (amm) {
                printf(_("The windows are as follows: (backend ID,frontend ID)"));
                putchar('\n');
                for (int i = 0; i < amm; ++i) {
                    const char* backend_id = tkret.next_token();
                    const char* frontend_id = tkret.next_token();
                    printf("%s,%s\n", backend_id, frontend_id);
                }
            }
        }
    }
}

void Client::Priv::cmd_rescale_backend(ArrayToken* tkzer) {
    auto display = tkzer->next_token();
    auto nscf = tkzer->next_token();
    DynamicNullDelimiterBuilder bld;
    bld.add_token("rescale_bk");
    if (display) {
        bld.add_token(display);
    }
    if (nscf) {
        bld.add_token(nscf);
    }
    auto ret = request_server(bld.build());
    NullToken tkret(ret.second.get(), ret.first);
    if (output_mode == OutputMode::RAW) {
        print_raw_null_token(&tkret);
    } else {
        auto resp = tkret.next_token();
        if (ipc_consts().serverret_string_map.count(resp)) {
            interactive_serverret_print(ipc_consts().serverret_string_map[resp]);
        } else {
            puts(stringconsts::values().INVALID_RESPONSE);
        }
    }
}