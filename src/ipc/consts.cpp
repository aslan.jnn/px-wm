#include "ipc/consts.h"

using namespace pxwm::ipc;

ConstData::ConstData() {
    // generate the other consts
    for (auto kvp : serverret_value_map) {
        serverret_string_map[kvp.second] = kvp.first;
    }
}

ConstData& ::pxwm::ipc::ipc_consts() {
    static ConstData instance;
    return instance;
}