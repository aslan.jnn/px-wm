#include "backend/eventloop.h"
#include "3rdparty/xcbxkb_compat.h"
#include "backend/common.h"
#include "common/eventpooler.h"
#include "common/handlerhelpers.h"
#include "common/selmgr.h"
#include "common/x11.h"
#include "frontend/x11.h"
#include "intermsg/frontend_eventloop.h"
#include "shared/bitwiseenumclass.h"
#include "shared/i18n.h"
#include "shared/memory.h"
#include "shared/util.h"
#include "sharedds/clientmsgdata.h"
#include "sharedds/cursordata.h"
#include "sharedds/windowupdatedata.h"
#include "sharedds/xkb.h"
#include "xkb/backend.h"
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdio>
#include <functional>
#include <glog/logging.h>
#include <thread>
#include <unistd.h>
#include <unordered_map>
#include <xcb/damage.h>
#include <xcb/xcb.h>
#include <xcb/xcb_util.h>
#include <xcb/xtest.h>

using namespace px::bitwiseenumclass;
using namespace px::util;
using namespace px::memory;
using namespace pxwm;
using namespace pxwm::common;
using namespace pxwm::common::x11;
using namespace pxwm::states;
using namespace pxwm::backend;
using namespace pxwm::backend::eventloop;
using namespace pxwm::intermsg;
using namespace std;

typedef unordered_map<xcb_window_t, backend::common::ManagedWindowData> managed_windows_map_t;

class EventLoop::Handlers {
public:
    EventLoop* parent;
    EventLoop::Priv* p;
    unique_ptr<EventPooler> evt_pooler;

    Handlers(EventLoop* parent, EventLoop::Priv* p, map<xcb_extension_t*, XExtensionInfo> ext_first_events);
    Handlers(const Handlers&) = delete;
    Handlers& operator=(const Handlers&) = delete;

    bool poll_and_handle_events();

private:
    // helpers
    handlerhelpers::BEShapeNotifyHelper shape_notify_helper;

    void handle_error(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_property_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_configure_request(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_configure_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_map_request(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_map_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_unmap_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_destroy_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_client_message(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_focus_in(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_selection_request(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_selection_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_ext_damage_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_xkb_events(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_xfixes_cursor_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_xfixes_selection_notify(xcb_generic_event_t* evt, int ord, bool is_last);
    void handle_shape_notify(xcb_generic_event_t* evt, int ord, bool is_last);
};

class EventLoop::FrontendHandlers {
public:
    EventLoop* parent;
    EventLoop::Priv* p;
    FrontendHandlers(EventLoop* parent,
        EventLoop::Priv* p,
        queue<intermsg::frontendeventloop::message_store_t>& msg_queue);
    FrontendHandlers(const FrontendHandlers&) = delete;
    FrontendHandlers& operator=(const FrontendHandlers&) = delete;

    void handle_events();

private:
    queue<intermsg::frontendeventloop::message_store_t>& msg_queue;

    void handle_geom_cfg(intermsg::frontendeventloop::GeomCfgMessage* cmsg);
    void handle_mouse_motion(intermsg::frontendeventloop::MouseMotionMessage* cmsg);
    void handle_keybutton(intermsg::frontendeventloop::KeyButtonMessage* cmsg);
    void handle_focus_req(intermsg::frontendeventloop::FocusRequestMessage* cmsg);
    void handle_focus_out();
    void handle_xkb_update(intermsg::frontendeventloop::XkbUpdateMessage* cmsg);
    void handle_close_window(intermsg::frontendeventloop::WindowCloseMessage* cmsg);
    void handle_property_backward(intermsg::frontendeventloop::PropertyChangeMessage* cmsg);
    void handle_client_message(intermsg::frontendeventloop::CMMessage* cmsg);
    void handle_selection_reply_message(intermsg::frontendeventloop::SelReplyMessage* cmsg);
    void handle_selection_own_message(intermsg::frontendeventloop::SelOwnMessage* cmsg);
    void handle_selection_request_message(intermsg::frontendeventloop::SelRequestMessage* cmsg);
};

class EventLoop::Priv {
public:
    Priv(EventLoop* p) : parent{p} {}
    Priv(Priv&) = delete;
    Priv& operator=(const Priv&) = delete;

    EventLoop* parent;
    xcb_connection_t* conn;
    xcb_screen_t* be_scr;
    unique_ptr<Handlers> eh;
    unique_ptr<FrontendHandlers> feh;
    thread loop_thrd;
    struct {
        /**
         * Indicates that the thread inside this EventLoop has ever been started.
         */
        bool started = false;
        /**
         * If false, Indicates that the thread has handled any error gracefully (e.g.: no unhandled exception / SIGKILL)
         */
        bool abrupt_stop = true;
        /**
         * Set by main thread if someone request this thread to be stopped.
         */
        atomic<bool> req_stop{false};
    } thread_states;
    managed_windows_map_t managed_windows;
    /**
     * Container that list damaged windows (windows that has updated it's content). Will be shared (referenced) with
     * frontend.
     */
    pxwm::sharedds::WindowUpdateDataContainer wudc;
    BkFrontEndOptions options;
    frontend::X11Frontend* frontend;
    unique_ptr<xkb::Backend> xkb_backend;
    unique_ptr<SelMgr> selmgr;
    shared_ptr<VisualMap> visualmap;
    /**
     * @brief A set of key(s) and button(s) that has been pressed, but not released.
     * Stored as uint16_t, where the high 8 bit is a boolean value (true if button, false if key) and low 8 bit is the
     * key or button code.
     */
    unordered_set<uint16_t> pressed_keybuttons;

    void loop_thread(shared_ptr<condition_variable> cv, vector<xcb_window_t> initial_wins);
    void enqueue_message_to_backend(eventloopbackend::Message& msg);
    void manage_window(xcb_window_t window_id, sharedds::WindowGeomProperties& geometries);
    void unmanage_window(xcb_window_t window_id);
    void depress_all_keys();
    vector<sharedds::XProperty> get_window_properties(xcb_window_t wid);
    shared_ptr<xcb_shape_get_rectangles_reply_t> get_shape_rects(xcb_window_t wid);
};

void EventLoop::Priv::loop_thread(shared_ptr<condition_variable> cv, vector<xcb_window_t> initial_wins) {
    // make sure we don't leak when something really bad happens.
    DeleteInvoker dtor([this, &cv]() {
        if (thread_states.abrupt_stop) {
            LOG(DFATAL) << string_printf(
                "Fatal error occured on '%s' EventLoop thread.", parent->display_string.c_str());
            // above statement will only assert on debug mode, so ...
            auto msg =
                eventloopbackend::StopMessage{eventloopbackend::Type::STOPPED, eventloopbackend::StopCause::UNKNOWN};
            enqueue_message_to_backend(*((eventloopbackend::Message*)&msg));
        }
        frontend = nullptr;
        cv->notify_one();
    });
    LOG(INFO) << string_printf("EventLoop thread started on '%s'.", parent->display_string.c_str());

    // also start frontend. frontend will populate 'fe_msg_queue' (if there is any important information to pass to this
    // EventLoop) after a call to it's 'pool_event'.
    queue<intermsg::frontendeventloop::message_store_t> fe_msg_queue;
    frontend::X11Frontend fe(conn, parent->display_string, be_scr->root, wudc, fe_msg_queue, visualmap, options);
    // some event processing handlers needs to call the frontend
    frontend = &fe;
    // check frontend connection and server status. report to backend if it doesn't connect / unsupported.
    if (!fe.is_frontend_connected()) {
        thread_states.abrupt_stop = false;
        LOG(WARNING) << "Error connecting to frontend X server.";
        auto msg = eventloopbackend::StopMessage{
            eventloopbackend::Type::STOPPED, eventloopbackend::StopCause::FRONTEND_SERVER_CANNOT_CONNECT};
        enqueue_message_to_backend(*((eventloopbackend::Message*)&msg));
        cv->notify_one();
        return;
    } else if (!fe.frontend_has_exts()) {
        thread_states.abrupt_stop = false;
        LOG(WARNING) << "Frontend doesn't have required extensions.";
        auto msg = eventloopbackend::StopMessage{
            eventloopbackend::Type::STOPPED, eventloopbackend::StopCause::FRONTEND_UNSUPPORTED_EXT};
        enqueue_message_to_backend(*((eventloopbackend::Message*)&msg));
        cv->notify_one();
        return;
    } else if (fe.frontend_init_error()) {
        thread_states.abrupt_stop = false;
        LOG(WARNING) << "Frontend's initialisation error.";
        auto msg = eventloopbackend::StopMessage{
            eventloopbackend::Type::STOPPED, eventloopbackend::StopCause::FRONTEND_INIT_ERROR};
        enqueue_message_to_backend(*((eventloopbackend::Message*)&msg));
        cv->notify_one();
        return;
    } else if (fe.frontend_visual_unsupported()) {
        thread_states.abrupt_stop = false;
        LOG(WARNING) << "Frontend's visual unsupported.";
        auto msg = eventloopbackend::StopMessage{
            eventloopbackend::Type::STOPPED, eventloopbackend::StopCause::FRONTEND_VISUAL_UNSUPPORTED};
        enqueue_message_to_backend(*((eventloopbackend::Message*)&msg));
        cv->notify_one();
        return;
    }
    // initialize XKB
    if (!xkb_backend->init(make_shared<xkb::X11Frontend>(fe.get_fconn(), conn))) {
        thread_states.abrupt_stop = false;
        LOG(ERROR) << "Cannot setup XKB.";
        auto msg =
            eventloopbackend::StopMessage{eventloopbackend::Type::STOPPED, eventloopbackend::StopCause::XKB_INIT_ERR};
        enqueue_message_to_backend(*((eventloopbackend::Message*)&msg));
        cv->notify_one();
        return;
    }
    // event handler for frontend's event queue
    feh = make_unique<FrontendHandlers>(parent, this, fe_msg_queue);

    // map existing backend windows to frontend
    for (auto window : initial_wins) {
        // request window geometry
        auto wgeom = make_unique_malloc(xcb_get_geometry_reply(conn, xcb_get_geometry(conn, window), nullptr));
        auto wprop =
            make_unique_malloc(xcb_get_window_attributes_reply(conn, xcb_get_window_attributes(conn, window), nullptr));
        sharedds::WindowGeomProperties geometries{
            {wgeom->width, wgeom->height}, {wgeom->x, wgeom->y}, wgeom->border_width, wprop->override_redirect};
        if (!wprop->override_redirect) {
            common::add_window_default_event_listeners(conn, window);
        }
        manage_window(window, geometries);
    }

    while (true) {
        // delay for multiple of 1/60 sec, depending on user's request
        auto begin_loop_time = chrono::steady_clock::now();
        auto end_loop_time = begin_loop_time + std::chrono::duration<int, ratio<1, 60>>((int)options.framerate_mode);

        // does someone request stop?
        if (thread_states.req_stop.load(memory_order_seq_cst)) {
            // tell the destructor that we've handled this disconnect gracefully.
            thread_states.abrupt_stop = false;

            // request frontend to stop
            fe.stop();

            LOG(INFO) << string_printf("Requested disconnect on '%s'.", parent->display_string.c_str());

            auto msg =
                eventloopbackend::StopMessage{eventloopbackend::Type::STOPPED, eventloopbackend::StopCause::REQ_STOP};
            enqueue_message_to_backend(*((eventloopbackend::Message*)&msg));
            cv->notify_one();

            break;
        }

        // if poll_and_handle_events return false, then the connection is disconnected
        if (!eh->poll_and_handle_events()) {
            // tell the destructor
            thread_states.abrupt_stop = false;

            // request frontend to stop
            fe.stop();

            LOG(WARNING) << "Connection to backend X Server abruptly aborted.";

            // tell the backend p
            auto msg = eventloopbackend::StopMessage{
                eventloopbackend::Type::STOPPED, eventloopbackend::StopCause::SERVER_DISCONNECT};
            enqueue_message_to_backend(*((eventloopbackend::Message*)&msg));

            break;
        }

        // stop if a frontend error occurs
        if (!fe.poll_event()) {
            // tell the destructor
            thread_states.abrupt_stop = false;

            LOG(WARNING) << "Disconnected from frontend X Server.";

            // tell the backend p
            auto msg = eventloopbackend::StopMessage{
                eventloopbackend::Type::STOPPED, eventloopbackend::StopCause::FRONTEND_SERVER_DISCONNECT};
            enqueue_message_to_backend(*((eventloopbackend::Message*)&msg));
            cv->notify_one();

            break;
        }
        // handle event from frontend
        feh->handle_events();
        // sync all unsync-ed request from frontend event handler
        xcb_flush(conn);
        // request frontend to refresh windows
        fe.req_refresh_all_windows();

        this_thread::sleep_until(end_loop_time);
    }
}

void EventLoop::Priv::enqueue_message_to_backend(eventloopbackend::Message& msg) {
    parent->msg_queue.enqueue(msg);
    parent->has_queue.store(true, memory_order_release);
}

void EventLoop::Priv::manage_window(xcb_window_t window_id, sharedds::WindowGeomProperties& geometries) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    wudc.data[window_id] = pxwm::sharedds::WindowUpdateData();
    common::ManagedWindowData windata;

    // clear border width
    if (geometries.border_size) {
        uint32_t border_width = 0;
        xcb_configure_window(conn, window_id, XCB_CONFIG_WINDOW_BORDER_WIDTH, &border_width);
    }
    // populate geometry info for this window
    windata.geometries = geometries;
    // caller has guaranteed that this window is mapped and viewable
    windata.is_mapped = 1;
    // non override_redirected windows should be attached w/ event listeners @ maprequest time
    if (geometries.override_redirect) {
        common::add_window_default_event_listeners(conn, window_id);
    }
    common::create_damage_listener(conn, window_id, windata);
    managed_windows[window_id] = windata;
    // forward window properties (e.g. ICCCM/EWMH)
    auto win_properties = get_window_properties(window_id);
    // forward shape
    auto shape_rects = get_shape_rects(window_id);
    pair<uint32_t, xcb_rectangle_t*> shape_rects_fwd;
    if (shape_rects) {
        shape_rects_fwd = {xcb_shape_get_rectangles_rectangles_length(shape_rects.get()),
            xcb_shape_get_rectangles_rectangles(shape_rects.get())};
    }
    // visual
    VisualInfo visualinfo;
    auto window_attr = xcb_get_window_attributes_reply(conn, xcb_get_window_attributes(conn, window_id), &err.pop());
    if (err.get()) {
        visualinfo = visualmap->default_visual();
        LOG(ERROR) << "Cannot get window attr for visual inspection: "
                   << xcb_event_get_error_label(err.get()->error_code) << ". Window may not rendered correctly.";
    } else {
        visualinfo = visualmap->get_visual_type(window_attr->visual);
    }
    // tell frontend
    frontend->new_window(window_id, windata.geometries, win_properties, shape_rects_fwd, visualinfo);
}

void EventLoop::Priv::unmanage_window(xcb_window_t window_id) {
    wudc.updated.erase(window_id);
    if (!wudc.data.erase(window_id)) {
        LOG(INFO) << "Removed window doesn't exist in Window Update List";
    }
    common::remove_window_default_event_listener(parent->p->conn, window_id, managed_windows[window_id]);
    managed_windows.erase(window_id);
    frontend->delete_window(window_id);
}

void EventLoop::Priv::depress_all_keys() {
    if (pressed_keybuttons.size()) {
        for (uint16_t ckb_code : pressed_keybuttons) {
            uint8_t type = (ckb_code & (1 << 9)) ? XCB_BUTTON_RELEASE : XCB_KEY_RELEASE;
            xcb_test_fake_input(conn, type, ckb_code & 0xff, XCB_CURRENT_TIME, XCB_NONE, 0, 0, 0);
        }
        xcb_flush(conn);
        pressed_keybuttons.clear();
    }
}

vector<sharedds::XProperty> EventLoop::Priv::get_window_properties(xcb_window_t wid) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto props = make_unique_malloc(xcb_list_properties_reply(conn, xcb_list_properties(conn, wid), &err.pop()));
    if (err.get()) {
        LOG(ERROR) << string_printf("Cannot retrieve properties list for backend window %#x (%s)", wid,
            xcb_event_get_error_label(err.get()->error_code));
        return vector<sharedds::XProperty>();
    }
    // request
    auto count = xcb_list_properties_atoms_length(props.get());
    auto atoms_arr = xcb_list_properties_atoms(props.get());
    vector<xcb_get_property_cookie_t> requests(count);
    for (int i = 0; i < count; ++i) {
        requests[i] = xcb_get_property(conn, 0, wid, atoms_arr[i], XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE);
    }
    // reply
    vector<sharedds::XProperty> ret(count);
    for (int i = 0; i < count; ++i) {
        auto prop_rep = make_unique_malloc(xcb_get_property_reply(conn, requests[i], &err.pop()));
        if (err.get() || prop_rep.get() == nullptr) {
            LOG(ERROR) << string_printf("Cannot retrieve property '%s' for backend window %#x (%s)",
                get_atom_name(conn, atoms_arr[i]).c_str(), wid, xcb_event_get_error_label(err.get()->error_code));
        }
        sharedds::XProperty& prop = ret[i];
        prop.prop_atom = atoms_arr[i];
        prop.value.prop_type = prop_rep->type;
        prop.value.format_bpp = prop_rep->format;
        prop.value.data_len = xcb_get_property_value_length(prop_rep.get());
        prop.value.data_ptr = make_shared_malloc(malloc(prop.value.data_len));
        memcpy(prop.value.data_ptr.get(), xcb_get_property_value(prop_rep.get()), prop.value.data_len);
    }
    return ret;
}

shared_ptr<xcb_shape_get_rectangles_reply_t> EventLoop::Priv::get_shape_rects(xcb_window_t wid) {
    managed_windows_map_t::iterator it;
    if (check_and_get_mapped_value(managed_windows, wid, it)) {
        // query
        UniquePointerWrapper_free<xcb_generic_error_t> err;
        auto srs = make_shared_malloc(xcb_shape_get_rectangles_reply(
            conn, xcb_shape_get_rectangles(conn, wid, XCB_SHAPE_SK_BOUNDING), &err.pop()));
        if (err.get()) {
            LOG(WARNING) << "Cannot get shape data: " << xcb_event_get_error_label(err.get()->response_type);
            return nullptr;
        }
        // check if the shape isn't actually a shape
        auto rects = xcb_shape_get_rectangles_rectangles(srs.get());
        auto rects_len = xcb_shape_get_rectangles_rectangles_length(srs.get());
        if (rects_len == 0 ||
            (rects_len == 1 && max((int16_t)0, rects->x) == 0 && max((int16_t)0, rects->x) == 0 &&
                it->second.geometries.size.first == min(rects->width, it->second.geometries.size.first) &&
                it->second.geometries.size.second == min(rects->height, it->second.geometries.size.second))) {
            return nullptr;
        }
        // OK, this window is actually shaped!
        return srs;
    }
    // unmanaged windows won't be queried
    return nullptr;
}

EventLoop::EventLoop(xcb_connection_t* conn,
    xcb_screen_t* scr,
    string display_string,
    map<xcb_extension_t*, XExtensionInfo> ext_first_events,
    std::shared_ptr<pxwm::common::x11::VisualMap> visualmap,
    BkFrontEndOptions options)
    : display_string{display_string} {
    p = make_unique<Priv>(this);
    p->conn = conn;
    p->be_scr = scr;
    p->eh = make_unique<Handlers>(this, p.get(), ext_first_events);
    p->options = options;
    p->xkb_backend = make_unique<xkb::Backend>(conn);
    p->selmgr = make_unique<SelMgr>(conn, scr);
    p->visualmap = visualmap;
}

EventLoop::~EventLoop() {
    request_stop();
    if (p->loop_thrd.joinable()) {
        p->loop_thrd.join();
    }
    //... so that associated object's destructors are properly called
    auto conn = p->conn;
    p = nullptr;
    clear_atom_cache(conn);
    disconnect_display(conn);
}

void EventLoop::start_run_loop(shared_ptr<condition_variable> p_cv, vector<xcb_window_t>& initial_windows) {
    if (!p->thread_states.started) {
        p->thread_states.started = true;
        p->loop_thrd = thread(&Priv::loop_thread, p.get(), p_cv, initial_windows);
    } else {
        // should never ever be called
        LOG(FATAL) << "Attempt to start again an event loop thread.";
    }
}

void EventLoop::request_stop() {
    // indicates the main loop that we want to stop
    p->thread_states.req_stop = true;
}

void EventLoop::request_rescale(uint16_t scfactor) {
    p->options.scale_factor = scfactor;
    p->frontend->req_rescale(scfactor);
}

vector<pair<xcb_window_t, xcb_window_t>> EventLoop::get_managed_windows() {
    return p->frontend->get_managed_windows();
}

pxwm::states::BkFrontEndOptions EventLoop::get_applied_options() {
    return p->options;
}

EventLoop::Handlers::Handlers(EventLoop* parent,
    EventLoop::Priv* p,
    map<xcb_extension_t*, XExtensionInfo> ext_first_events)
    : parent{parent}, p{p} {
    map<uint8_t, function<void(xcb_generic_event_t*, int, bool)>> event_handlers;
    event_handlers[0] = bind(&Handlers::handle_error, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_PROPERTY_NOTIFY] =
        bind(&Handlers::handle_property_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_CONFIGURE_REQUEST] =
        bind(&Handlers::handle_configure_request, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_CONFIGURE_NOTIFY] =
        bind(&Handlers::handle_configure_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_MAP_REQUEST] =
        bind(&Handlers::handle_map_request, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_MAP_NOTIFY] =
        bind(&Handlers::handle_map_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_UNMAP_NOTIFY] =
        bind(&Handlers::handle_unmap_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_DESTROY_NOTIFY] =
        bind(&Handlers::handle_destroy_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_CLIENT_MESSAGE] =
        bind(&Handlers::handle_client_message, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_FOCUS_IN] =
        bind(&Handlers::handle_focus_in, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_SELECTION_NOTIFY] =
        bind(&Handlers::handle_selection_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[XCB_SELECTION_REQUEST] =
        bind(&Handlers::handle_selection_request, this, placeholders::_1, placeholders::_2, placeholders::_3);
    // extension event handler
    event_handlers[ext_first_events[&xcb_damage_id].first_event] =
        bind(&Handlers::handle_ext_damage_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[ext_first_events[&xcb_xkb_id].first_event] =
        bind(&Handlers::handle_xkb_events, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[ext_first_events[&xcb_xfixes_id].first_event + XCB_XFIXES_CURSOR_NOTIFY] =
        bind(&Handlers::handle_xfixes_cursor_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[ext_first_events[&xcb_xfixes_id].first_event + XCB_XFIXES_SELECTION_NOTIFY] =
        bind(&Handlers::handle_xfixes_selection_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);
    event_handlers[ext_first_events[&xcb_shape_id].first_event] =
        bind(&Handlers::handle_shape_notify, this, placeholders::_1, placeholders::_2, placeholders::_3);

    evt_pooler = make_unique<EventPooler>(p->conn, event_handlers);
}

bool EventLoop::Handlers::poll_and_handle_events() {
    return evt_pooler->poll_for_events();
}

void EventLoop::Handlers::handle_error(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_generic_error_t* cevt = (xcb_generic_error_t*)evt;
    // logging only
    LOG(INFO) << string_printf(
        "Error %d (%s) occured on backend X server.", cevt->error_code, xcb_event_get_error_label(cevt->error_code));
}

void EventLoop::Handlers::handle_property_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_property_notify_event_t* cevt = (xcb_property_notify_event_t*)evt;
    // frontend will check if the window is managed
    p->frontend->hdlbe_property_change(cevt);
}

void EventLoop::Handlers::handle_configure_request(xcb_generic_event_t* evt, int ord, bool is_last) {
    // all normal, non overide-redirect windows should trigger this event
    xcb_configure_request_event_t* cevt = (xcb_configure_request_event_t*)evt;
    managed_windows_map_t::iterator wit;
    if (check_and_get_mapped_value(p->managed_windows, cevt->window, wit) && wit->second.is_mapped) {
        // Confirm to frontend if WM agree/disagree with the size change. If WM agrees, frontend should send a geom_cfg.
        sharedds::WindowGeomProperties geom;
        geom.size = {cevt->width, cevt->height};
        // don't mess around w/ positions
        geom.pos = wit->second.geometries.pos;
        geom.border_size = geom.override_redirect = 0;
        p->frontend->hdlbe_configure_notify(cevt->window, geom);
    } else {
        // unmanaged windows are free to do anything that they wishes
        uint32_t values[5] = {(uint32_t)cevt->x, (uint32_t)cevt->y, (uint32_t)cevt->width, (uint32_t)cevt->height, 0};
        xcb_configure_window(p->conn, cevt->window, 31, values);
        xcb_flush(p->conn);
    }
}

void EventLoop::Handlers::handle_configure_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_configure_notify_event_t* cevt = (xcb_configure_notify_event_t*)evt;
    managed_windows_map_t::iterator wit;

    if (check_and_get_mapped_value(p->managed_windows, cevt->window, wit)) {
        // only handle if this window is override-redirect
        if (cevt->override_redirect) {
            sharedds::WindowGeomProperties geometries{
                {cevt->width, cevt->height}, {cevt->x, cevt->y}, cevt->border_width, cevt->override_redirect};
            p->frontend->hdlbe_configure_notify(cevt->window, geometries);
            // update our internal data structure
            wit->second.geometries = geometries;
        }
    }
}

void EventLoop::Handlers::handle_map_request(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_map_request_event_t* cevt = (xcb_map_request_event_t*)evt;

    // listen to these changes ASAP, to detect any immediate focus requests, etc.
    common::add_window_default_event_listeners(p->conn, cevt->window);

    // always grant every window map request to backend X server (this doesn't always means that the window will be
    // mapped in frontend. This will be handled at 'handle_map_notify')
    xcb_map_window(p->conn, cevt->window);
    xcb_flush(p->conn);
}

void EventLoop::Handlers::handle_map_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_map_notify_event_t* cevt = (xcb_map_notify_event_t*)evt;

    // To avoid checking for preconditions. Windows that don't pass the conditions is very likely to not having a
    // geometry.
    auto get_geom = [&](xcb_window_t w) -> sharedds::WindowGeomProperties {
        auto wg = make_unique_malloc<xcb_get_geometry_reply_t>(
            xcb_get_geometry_reply(p->conn, xcb_get_geometry(p->conn, cevt->window), nullptr));
        sharedds::WindowGeomProperties geometries{
            {wg->width, wg->height}, {wg->x, wg->y}, wg->border_width, cevt->override_redirect};
        return geometries;
    };

    // A client may wishes to reuse it's unmapped window, change override redirect, etc.
    managed_windows_map_t::iterator it;
    if (check_and_get_mapped_value(p->managed_windows, cevt->window, it)) {
        auto geometries = get_geom(cevt->window);
        p->frontend->map_window(cevt->window, geometries);
        it->second.geometries = geometries;
        it->second.is_mapped = 1;
    } else if (common::accept_window_for_redir(p->conn, cevt->window, p->visualmap.get()) &&
        !p->managed_windows.count(cevt->window)) {
        auto geometries = get_geom(cevt->window);
        p->manage_window(cevt->window, geometries);
    }
}

void EventLoop::Handlers::handle_unmap_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_unmap_notify_event_t* cevt = (xcb_unmap_notify_event_t*)evt;
    // an unmap doesn't always mean destruction. Client may want to reuse the window / simply hiding it.
    managed_windows_map_t::iterator it;
    if (check_and_get_mapped_value(p->managed_windows, cevt->window, it)) {
        it->second.is_mapped = 0;
        p->frontend->unmap_window(cevt->window);
    }
}

void EventLoop::Handlers::handle_destroy_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    auto cevt = (xcb_destroy_notify_event_t*)evt;
    p->selmgr->destroy_requestor_data(cevt->window);
    if (p->managed_windows.count(cevt->window)) {
        p->unmanage_window(cevt->window);
    }
}

void EventLoop::Handlers::handle_client_message(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_client_message_event_t* cevt = (xcb_client_message_event_t*)evt;
    // frontend will check if the window is managed
    p->frontend->hdlbe_client_message(cevt);
}

void EventLoop::Handlers::handle_focus_in(xcb_generic_event_t* evt, int ord, bool is_last) {
    auto cevt = (xcb_focus_in_event_t*)evt;
    // frontend will check if the window is managed
    p->frontend->hdlbe_focus_in(cevt->event);
}

void EventLoop::Handlers::handle_selection_request(xcb_generic_event_t* evt, int ord, bool is_last) {
    auto cevt = (xcb_selection_request_event_t*)evt;
    // just to make sure
    if (cevt->owner != p->selmgr->proxy_wid) {
        return;
    }
    auto selreqs = p->selmgr->handle_request(cevt);
    if (selreqs.second.size()) {
        // tell the frontend
        if (!p->frontend->hdlbe_selection_request(cevt->requestor, selreqs.first, selreqs.second)) {
            // reject the request if frontend doesn't ack.
            vector<sharedds::XProperty> empty_props;
            p->selmgr->supply_data(evt_pooler.get(), cevt->requestor, selreqs.first, empty_props);
        }
    }
}

void EventLoop::Handlers::handle_selection_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    auto cevt = (xcb_selection_notify_event_t*)evt;
    auto result = p->selmgr->parse_selection_notify(p->eh->evt_pooler.get(), cevt);
    // send to frontend
    p->frontend->hdlbe_selection_reply(get<0>(result), get<1>(result), get<2>(result));
}

void EventLoop::Handlers::handle_ext_damage_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    xcb_damage_notify_event_t* cevt = (xcb_damage_notify_event_t*)evt;
    sharedds::window_update_data_map_t::iterator dmg_data_it;
    if (check_and_get_mapped_value(p->wudc.data, cevt->drawable, dmg_data_it)) {
        auto& dmg_data = dmg_data_it->second;
        // fix for whole damage
        if (cevt->area.x < 0 || cevt->area.y < 0) {
            dmg_data.add_box(0, 0, cevt->geometry.width, cevt->geometry.height);
        } else {
            dmg_data.add_box(cevt->area.x, cevt->area.y, cevt->area.width, cevt->area.height);
        }

        if (!dmg_data.updated) {
            dmg_data.updated = true;
            p->wudc.updated.insert(cevt->drawable);
        }
    }
}

void EventLoop::Handlers::handle_xkb_events(xcb_generic_event_t* evt, int ord, bool is_last) {
    switch (evt->pad0) {
        case XCB_XKB_NEW_KEYBOARD_NOTIFY: {
            auto cevt = (xcb_xkb_new_keyboard_notify_event_t*)evt;
            if (cevt->changed & XCB_XKB_NKN_DETAIL_KEYCODES) {
                if (!p->xkb_backend->update_backend(
                        sharedds::XkbUpdatedInfo{sharedds::XkbUpdateFlag::DESC, sharedds::XkbDescUpdateFlag::ALL})) {
                    // there's nothing we can't do here, but keep going :(
                    LOG(WARNING) << "Cannot update backend XKB DESC on backend's NKN event.";
                }
            }
            break;
        }
    }
}

void EventLoop::Handlers::handle_xfixes_cursor_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    auto cevt = (xcb_xfixes_cursor_notify_event_t*)evt;
    sharedds::CursorData cur;
    // make sure our image data isn't destroyed before passed to frontend
    unique_ptr<xcb_xfixes_get_cursor_image_reply_t, free_delete> curimg;
    if (cevt->name) {
        cur.semantic_name = get_atom_name(p->conn, cevt->name);
    } else {
        // download the image
        UniquePointerWrapper_free<xcb_generic_error_t> err;
        curimg = make_unique_malloc(
            xcb_xfixes_get_cursor_image_reply(p->conn, xcb_xfixes_get_cursor_image(p->conn), &err.pop()));
        cur.data.format = 32;
        cur.data.image = xcb_xfixes_get_cursor_image_cursor_image(curimg.get());
        cur.data.size = {curimg->width, curimg->height};
        cur.data.hotspot = {curimg->xhot, curimg->yhot};
    }
    // call the frontend
    p->frontend->hdlbe_cursor_change(cur);
}

void EventLoop::Handlers::handle_xfixes_selection_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    auto cevt = (xcb_xfixes_selection_notify_event_t*)evt;
    if (cevt->owner != p->selmgr->proxy_wid) {
        p->frontend->hdlbe_take_own_selection(cevt->selection, cevt->owner);
    }
}

void EventLoop::Handlers::handle_shape_notify(xcb_generic_event_t* evt, int ord, bool is_last) {
    auto cevt = (xcb_shape_notify_event_t*)evt;
    shape_notify_helper.update_list.insert(cevt->affected_window);
    if (is_last) {
        for (xcb_window_t wid : shape_notify_helper.update_list) {
            auto rects = p->get_shape_rects(cevt->affected_window);
            if (rects) {
                p->frontend->hdlbe_shape_change(wid, xcb_shape_get_rectangles_rectangles_length(rects.get()),
                    xcb_shape_get_rectangles_rectangles(rects.get()));
            }
        }
        shape_notify_helper.update_list.clear();
    }
}

EventLoop::FrontendHandlers::FrontendHandlers(EventLoop* parent,
    EventLoop::Priv* p,
    queue<intermsg::frontendeventloop::message_store_t>& msg_queue)
    : parent{parent}, p{p}, msg_queue{msg_queue} {}

void EventLoop::FrontendHandlers::handle_events() {
    while (!msg_queue.empty()) {
        auto& msg_store = msg_queue.front();
        auto msg_ptr = (intermsg::frontendeventloop::Message*)msg_store.data;
        switch (msg_ptr->type) {
            case intermsg::frontendeventloop::FrontEndResultType::GEOM_CFG:
                handle_geom_cfg((intermsg::frontendeventloop::GeomCfgMessage*)msg_store.data);
                break;
            case intermsg::frontendeventloop::FrontEndResultType::MOUSE_MOTION:
                handle_mouse_motion((intermsg::frontendeventloop::MouseMotionMessage*)msg_store.data);
                break;
            case intermsg::frontendeventloop::FrontEndResultType::KEYBUTTON:
                handle_keybutton((intermsg::frontendeventloop::KeyButtonMessage*)msg_store.data);
                break;
            case intermsg::frontendeventloop::FrontEndResultType::FOCUS_REQ:
                handle_focus_req((intermsg::frontendeventloop::FocusRequestMessage*)msg_store.data);
                break;
            case intermsg::frontendeventloop::FrontEndResultType::FOCUS_OUT:
                handle_focus_out();
                break;
            case intermsg::frontendeventloop::FrontEndResultType::XKB_UPDATE:
                handle_xkb_update((intermsg::frontendeventloop::XkbUpdateMessage*)msg_store.data);
                break;
            case intermsg::frontendeventloop::FrontEndResultType::DEPRESS_ALL:
                p->depress_all_keys();
                break;
            case intermsg::frontendeventloop::FrontEndResultType::CLOSE:
                handle_close_window(((intermsg::frontendeventloop::WindowCloseMessage*)msg_store.data));
                break;
            case intermsg::frontendeventloop::FrontEndResultType::PROPERTY_CHANGE:
                handle_property_backward((intermsg::frontendeventloop::PropertyChangeMessage*)msg_store.data);
                break;
            case intermsg::frontendeventloop::FrontEndResultType::CLIENT_MESSAGE:
                handle_client_message((intermsg::frontendeventloop::CMMessage*)msg_store.data);
                break;
            case intermsg::frontendeventloop::FrontEndResultType::SELECTION_REPLY:
                handle_selection_reply_message((intermsg::frontendeventloop::SelReplyMessage*)msg_store.data);
                break;
            case intermsg::frontendeventloop::FrontEndResultType::SELECTION_OWN:
                handle_selection_own_message((intermsg::frontendeventloop::SelOwnMessage*)msg_store.data);
                break;
            case intermsg::frontendeventloop::FrontEndResultType::SELECTION_REQUEST:
                handle_selection_request_message((intermsg::frontendeventloop::SelRequestMessage*)msg_store.data);
                break;
            default:
                break;
        }
        // done with this event
        msg_queue.pop();
    }
}

void EventLoop::FrontendHandlers::handle_geom_cfg(intermsg::frontendeventloop::GeomCfgMessage* cmsg) {
    managed_windows_map_t::iterator wit;
    if (check_and_get_mapped_value(p->managed_windows, cmsg->back_window_id, wit)) {
        if (enumval(cmsg->updated & intermsg::frontendeventloop::GeomCfgUpdated::SIZE)) {
            uint32_t cfgwin_values[] = {cmsg->new_geoms.size.first, cmsg->new_geoms.size.second};
            xcb_configure_window(
                p->conn, cmsg->back_window_id, XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, cfgwin_values);
            wit->second.geometries.size = {cmsg->new_geoms.size.first, cmsg->new_geoms.size.second};
        }
        if (enumval(cmsg->updated & intermsg::frontendeventloop::GeomCfgUpdated::POS)) {
            uint32_t cfgwin_values[] = {
                (uint32_t)((int32_t)cmsg->new_geoms.pos.first), (uint32_t)((int32_t)cmsg->new_geoms.pos.second)};
            xcb_configure_window(
                p->conn, cmsg->back_window_id, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, cfgwin_values);
            wit->second.geometries.pos = {cmsg->new_geoms.pos.first, cmsg->new_geoms.pos.second};
        }
        // if frontend called this, it is guaranteed that at least position or size is being configured.
        xcb_flush(p->conn);
    }
}

void EventLoop::FrontendHandlers::handle_mouse_motion(intermsg::frontendeventloop::MouseMotionMessage* cmsg) {
    auto& offset = p->managed_windows[cmsg->back_window_id].geometries.pos;
    pair<uint16_t, uint16_t> target = {offset.first + cmsg->x, offset.second + cmsg->y};
    xcb_test_fake_input(
        p->conn, XCB_MOTION_NOTIFY, false, XCB_CURRENT_TIME, p->be_scr->root, target.first, target.second, 0);
    xcb_flush(p->conn);
}

void EventLoop::FrontendHandlers::handle_keybutton(intermsg::frontendeventloop::KeyButtonMessage* cmsg) {
    uint8_t type = 0;
    uint16_t ckb_code;
    switch (cmsg->kb_type) {
        case intermsg::frontendeventloop::KeyButtonType::BUTTON:
            type = cmsg->pressed ? XCB_BUTTON_PRESS : XCB_BUTTON_RELEASE;
            ckb_code = 1 << 9;
            break;
        case intermsg::frontendeventloop::KeyButtonType::KEY:
            type = cmsg->pressed ? XCB_KEY_PRESS : XCB_KEY_RELEASE;
            ckb_code = 0;
            break;
    }
    ckb_code |= cmsg->kb_code;

    if (cmsg->pressed) {
        // don't press twice
        if (p->pressed_keybuttons.count(ckb_code)) {
            return;
        }
        p->pressed_keybuttons.insert(ckb_code);
    } else {
        p->pressed_keybuttons.erase(ckb_code);
    }

    xcb_test_fake_input(p->conn, type, cmsg->kb_code, XCB_CURRENT_TIME, XCB_NONE, 0, 0, 0);
    xcb_flush(p->conn);
}

void EventLoop::FrontendHandlers::handle_focus_req(intermsg::frontendeventloop::FocusRequestMessage* cmsg) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto focus_state = make_unique_malloc(xcb_get_input_focus_reply(p->conn, xcb_get_input_focus(p->conn), &err.pop()));
    if (err.get() || !focus_state.get()) {
        LOG(ERROR) << "Cannot retrieve focus info on backend";
    } else if (focus_state->focus != cmsg->back_window_id) {
        xcb_set_input_focus(p->conn, XCB_INPUT_FOCUS_PARENT, cmsg->back_window_id, XCB_CURRENT_TIME);
    }
    // always bring upfront, for consistency
    const uint32_t stack_mode = XCB_STACK_MODE_ABOVE;
    xcb_configure_window(p->conn, cmsg->back_window_id, XCB_CONFIG_WINDOW_STACK_MODE, &stack_mode);
    // set _NET_ACTIVE_WINDOW (EWMH)
    xcb_change_property(p->conn, XCB_PROP_MODE_REPLACE, p->be_scr->root, get_atom(p->conn, false, "_NET_ACTIVE_WINDOW"),
        XCB_ATOM_WINDOW, 32, 1, &cmsg->back_window_id);
    xcb_flush(p->conn);
}

void EventLoop::FrontendHandlers::handle_focus_out() {
    static xcb_window_t none_window = XCB_WINDOW_NONE;
    p->depress_all_keys();
    // set focus back to root window (so we can actually track if other app request a focus)
    xcb_set_input_focus(p->conn, XCB_INPUT_FOCUS_NONE, XCB_INPUT_FOCUS_NONE, XCB_CURRENT_TIME);
    // set _NET_ACTIVE_WINDOW (EWMH)
    xcb_change_property(p->conn, XCB_PROP_MODE_REPLACE, p->be_scr->root, get_atom(p->conn, false, "_NET_ACTIVE_WINDOW"),
        XCB_ATOM_WINDOW, 32, 1, &none_window);
    xcb_flush(p->conn);
}

void EventLoop::FrontendHandlers::handle_xkb_update(intermsg::frontendeventloop::XkbUpdateMessage* cmsg) {
    if (!p->xkb_backend->update_backend(cmsg->updated, &cmsg->state_evt)) {
        // we have to inform the user!
        fputs(_("Error updating XKB. Keyboard may not function correctly."), stderr);
        fputc(' ', stderr);
        fputs(_("Please check log for details."), stderr);
        fputc('\n', stderr);
    }
}

void EventLoop::FrontendHandlers::handle_close_window(intermsg::frontendeventloop::WindowCloseMessage* cmsg) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    bool protoc_wm_del = false;
    // does the target window participates in WM_PROTOCOLS WM_DELETE_WINDOW?
    auto prop = make_unique_malloc(xcb_get_property_reply(p->conn,
        xcb_get_property(p->conn, false, cmsg->back_window_id, get_atom(p->conn, false, "WM_PROTOCOLS"),
            XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE),
        &err.pop()));
    auto prop_tes = *prop.get();
    // check for errors and format matchness before attempt
    if (!err.get() && prop.get() && prop->type == XCB_ATOM_ATOM && prop->format == 32 && prop->length) {
        xcb_atom_t delwindow = get_atom(p->conn, false, "WM_DELETE_WINDOW");
        xcb_atom_t* protocol_atoms = (xcb_atom_t*)xcb_get_property_value(prop.get());
        for (int i = 0; i < prop->length; ++i) {
            if (protocol_atoms[i] == delwindow) {
                // windata.participate_wm_delete = true;
                protoc_wm_del = true;
                break;
            }
        }
    }
    // do the closing routine, based on what a common WM do
    if (protoc_wm_del) {
        // send the client message to the aforementioned window
        xcb_client_message_event_t clientmsg;
        clientmsg.response_type = XCB_CLIENT_MESSAGE;
        clientmsg.format = 32;
        clientmsg.window = cmsg->back_window_id;
        clientmsg.type = get_atom(p->conn, false, "WM_PROTOCOLS");
        clientmsg.data.data32[0] = get_atom(p->conn, false, "WM_DELETE_WINDOW");
        clientmsg.data.data32[1] = 0;
        err.pop() = xcb_request_check(p->conn, xcb_send_event_checked(p->conn, false, cmsg->back_window_id,
                                                   XCB_EVENT_MASK_NO_EVENT, (const char*)&clientmsg));
        if (err.get()) {
            LOG(ERROR) << "Cannot send WM_DELETE_WINDOW to backend";
            fputs(_("Error requesting backend server to close window."), stderr);
            fputc('\n', stderr);
        }
    } else {
        // kill the connection
        err.pop() = xcb_request_check(p->conn, xcb_kill_client_checked(p->conn, cmsg->back_window_id));
        if (err.get()) {
            LOG(ERROR) << "Cannot request connection close to backend";
            fputs(_("Error requesting backend server to close the client connection."), stderr);
            fputc('\n', stderr);
        }
    }
}

void EventLoop::FrontendHandlers::handle_property_backward(intermsg::frontendeventloop::PropertyChangeMessage* cmsg) {
    if (cmsg->state == XCB_PROPERTY_NEW_VALUE) {
        auto exdata = p->frontend->get_prop_from_msg(cmsg->prop_id);
        int data_count = exdata->data_len / (exdata->format_bpp / 8);
        xcb_change_property(p->conn, XCB_PROP_MODE_REPLACE, cmsg->target, cmsg->atom, exdata->prop_type,
            exdata->format_bpp, data_count, exdata->data_ptr.get());
    } else if (cmsg->state == XCB_PROPERTY_DELETE) {
        xcb_delete_property(p->conn, cmsg->target, cmsg->atom);
    } else {
        LOG(WARNING) << "Unknown case on EventLoop's PropertyChangeMessage";
    }
}

void EventLoop::FrontendHandlers::handle_client_message(intermsg::frontendeventloop::CMMessage* cmsg) {
    // do as instructed (everything should've been translated by frontend)
    xcb_client_message_event_t send_evt;
    memset(&send_evt, 0, sizeof(xcb_client_message_event_t));
    auto cmdata = p->frontend->get_cm_data_from_msg(cmsg->cm_id);
    send_evt.response_type = XCB_CLIENT_MESSAGE;
    send_evt.window = cmdata->window;
    send_evt.type = cmdata->msg_type;
    send_evt.format = cmdata->format;
    send_evt.data = cmdata->data;
    xcb_send_event(p->conn, cmdata->send_info.propagate, cmdata->send_info.destination, cmdata->send_info.event_mask,
        (char*)&send_evt);
}

void EventLoop::FrontendHandlers::handle_selection_reply_message(intermsg::frontendeventloop::SelReplyMessage* cmsg) {
    auto data = p->frontend->get_sel_reply_data_from_msg(cmsg->requestor);
    p->selmgr->supply_data(p->eh->evt_pooler.get(), cmsg->requestor, data.first, data.second);
}

void EventLoop::FrontendHandlers::handle_selection_own_message(intermsg::frontendeventloop::SelOwnMessage* cmsg) {
    if (cmsg->takeown) {
        p->selmgr->acquire_selection(cmsg->selection);
    } else {
        p->selmgr->release_selection(cmsg->selection);
    }
}

void EventLoop::FrontendHandlers::handle_selection_request_message(
    intermsg::frontendeventloop::SelRequestMessage* cmsg) {
    auto selreq = p->frontend->get_sel_request_refdata_from_msg(cmsg->requestor);
    if (selreq.first.selection) {
        if (!p->selmgr->request_selection(cmsg->requestor, selreq.first, selreq.second)) {
            // inform frontend that selection has been rejected
            vector<sharedds::XProperty> empty_prop;
            sharedds::SelectionInfo empty_selinfo;
            empty_selinfo.selection = selreq.first.selection;
            p->frontend->hdlbe_selection_reply(cmsg->requestor, empty_selinfo, empty_prop);
        }
    }
}