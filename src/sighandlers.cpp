#include "sighandlers.h"
#include "backend/backend.h"
#include <atomic>
#include <cstdio>
#include <cstring>
#include <execinfo.h>
#include <memory>
#include <signal.h>
#include <sys/syscall.h>
#include <thread>
#include <unistd.h>
#include <utility>

using namespace std;

namespace pxwm {
    namespace sighandlers {
        std::shared_ptr<pxwm::backend::Backend> backend_inst;
        atomic<bool> sighandler_running(false);
        atomic_flag sighdl_child_lock;

        void empty_signal_handler(int sig) {}

        void termination_signal_handler(int sig) {
            // make sure this is called only once
            static bool has_called = false;
            if (!has_called) {
                has_called = true;
                sighdl_child_lock.test_and_set();
                // CV and other waiting mechanism won't work properly when a signal is 'still being handled'.
                thread t([sig]() {
                    // spin lock while waiting for creator to finish
                    while (sighdl_child_lock.test_and_set()) {
                    }
                    // do the business
                    sighandler_running.store(true, memory_order_release);
                    auto dpys = backend_inst->get_managed_bk_displays();
                    for (auto dpy : dpys) {
                        backend_inst->request_remove_bk_display(dpy);
                    }
                    sighandler_running.store(false, memory_order_release);
                });
                // we have to detach so that main thread can continue.
                if (t.joinable()) {
                    t.detach();
                }
                // OK. Child thread may continue.
                sighdl_child_lock.clear();
            }
        }

        void segfault_signal_handler(int sig) {
            const char* sig_msg;
            switch (sig) {
                case SIGSEGV:
                    sig_msg = "Segmentation Fault";
                    break;
                case SIGILL:
                    sig_msg = "Illegal Instruction";
                    break;
                case SIGFPE:
                    sig_msg = "FP Exception";
                    break;
            }
            fprintf(stderr, "%s\n", sig_msg);
// stop this process (if not compiled in release mode), so that we can attach a debugger.
#ifndef NDEBUG
            fputs("Process will be paused. Please attach the debugger.\n", stderr);
            fprintf(stderr, "PID: %d\n", getpid());
            raise(SIGSTOP);
#endif
            exit(1);
        }

        void install_signal_handlers(shared_ptr<pxwm::backend::Backend> p_backend_inst) {
            atomic_store(&backend_inst, p_backend_inst);
            struct sigaction sa;
            memset(&sa, 0, sizeof(sa));
            // SIGUSR1, for interthread comms.
            sa.sa_handler = empty_signal_handler;
            sigaction(SIGUSR1, &sa, nullptr);
            // everything that stops (not pause)
            for (int i : {SIGINT, SIGTERM, SIGQUIT}) {
                sa.sa_handler = termination_signal_handler;
                sigaction(i, &sa, nullptr);
            }
            // signals for diagnosing errors
            sa.sa_handler = segfault_signal_handler;
            sigaction(SIGSEGV, &sa, nullptr);
            sigaction(SIGILL, &sa, nullptr);
            sigaction(SIGFPE, &sa, nullptr);
        }

        bool is_sighandler_running() {
            return sighandler_running.load(memory_order_acquire);
        }
    }
}