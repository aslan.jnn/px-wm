#include "common/handlerhelpers.h"
#include "common/stringconsts.h"
#include "common/xraii/gc.h"
#include "common/xraii/pixmap.h"
#include "common/xraii/xrenderpicture.h"
#include "glog/logging.h"
#include "intermsg/frontend_eventloop.h"
#include "shared/bitwiseenumclass.h"
#include "shared/i18n.h"
#include "shared/memory.h"
#include "shared/util.h"
#include <xcb/render.h>
#include <xcb/xcb_cursor.h>
#include <xcb/xcb_renderutil.h>
#include <xcb/xcb_util.h>

using namespace std;
using namespace px::memory;
using namespace px::util;
using namespace px::bitwiseenumclass;
using namespace pxwm;
using namespace pxwm::common::xraii;
using namespace pxwm::common::handlerhelpers;

class FocusIOHelper::Priv {
public:
    queue<intermsg::frontendeventloop::message_store_t>& be_msg_queue;
    frontend::WIDTrans* wid_trans;
    xcb_connection_t* fconn;
    pair<xcb_window_t, xcb_window_t>& fe_focused_state;
    Priv(queue<intermsg::frontendeventloop::message_store_t>& be_msg_queue,
        pair<xcb_window_t, xcb_window_t>& fe_focused_state);
    Priv(const Priv&) = delete;
    Priv& operator=(const Priv&) = delete;
};

class CursorHelper::Priv {
public:
    static constexpr int MAX_CURSOR_OBJ = 16;
    xcb_connection_t* fconn;
    xcb_screen_t* fscr;
    struct {
        unique_ptr<GC> gc;
        unique_ptr<Pixmap> pxm_base;
        unique_ptr<Pixmap> pxm_curr;
        xcb_render_pictforminfo_t format;
        unique_ptr<XRenderPicture> rp_curr;
        pair<uint16_t, uint16_t> curr_sz = {0, 0};
    } renderdata;
    xcb_cursor_context_t* xcursor_ctx;
    xcb_cursor_t curr_cursor = 0;
    vector<xcb_cursor_t> cursor_objs;
    bool init_success = true;
    // generated cursor ID will reside in 'curr_cursor'
    void new_cursor_id();
    void new_cursor_id(int existing_id);
    void free_cursors();
};

FocusIOHelper::FocusIOHelper(pair<xcb_window_t, xcb_window_t>& fe_focused_state,
    std::queue<intermsg::frontendeventloop::message_store_t>& be_msg_queue,
    frontend::WIDTrans* wid_trans,
    xcb_connection_t* fconn) {
    p = make_unique<Priv>(be_msg_queue, fe_focused_state);
    p->wid_trans = wid_trans;
    p->fconn = fconn;
}

FocusIOHelper::~FocusIOHelper() {}

void FocusIOHelper::forward_be() {
    if (focus_changed) {
        focus_changed = false;
        UniquePointerWrapper_free<xcb_generic_error_t> err;
        auto focus_rep =
            make_shared_malloc(xcb_get_input_focus_reply(p->fconn, xcb_get_input_focus(p->fconn), &err.pop()));
        if (err.get() || !focus_rep.get()) {
            LOG(ERROR) << "Cannot retrieve focus info on frontend";
        }
        // determine if current focus is in one of our managed windows.
        xcb_window_t be_focus = p->wid_trans->get_bw_id(focus_rep->focus);
        // if isn't, then assume focus out
        intermsg::frontendeventloop::message_store_t msg_store;
        if (be_focus) {
            p->fe_focused_state.first = be_focus;
            p->fe_focused_state.second = focus_rep->focus;

            // depress all keys
            new (msg_store.data) intermsg::frontendeventloop::DepressAllRequestMessage;
            p->be_msg_queue.push(msg_store);

            // ask backend to switch focus
            auto msg = new (msg_store.data) intermsg::frontendeventloop::FocusRequestMessage;
            msg->back_window_id = be_focus;
        } else {
            // ask backend to defocus everything
            p->fe_focused_state.first = p->fe_focused_state.second = 0;
            auto msg = new (msg_store.data) intermsg::frontendeventloop::FocusOutMessage;
        }
        // send to backend
        p->be_msg_queue.push(msg_store);
    }
}

FocusIOHelper::Priv::Priv(queue<intermsg::frontendeventloop::message_store_t>& be_msg_queue,
    pair<xcb_window_t, xcb_window_t>& fe_focused_state)
    : be_msg_queue{be_msg_queue}, fe_focused_state{fe_focused_state} {}

void ConfigureNotifyHelper::insert(xcb_window_t id, fd_geom_t& new_data) {
    if (!store.count(id)) {
        store[id] = new_data;
    } else {
        auto& stored_data = store[id];
        stored_data.first |= new_data.first;
        if (enumval(new_data.first & intermsg::frontendeventloop::GeomCfgUpdated::POS)) {
            stored_data.second.pos = new_data.second.pos;
        }
        if (enumval(new_data.first & intermsg::frontendeventloop::GeomCfgUpdated::SIZE)) {
            stored_data.second.size = new_data.second.size;
        }
    }
}

std::map<xcb_window_t, ConfigureNotifyHelper::fd_geom_t>& ConfigureNotifyHelper::get() {
    return store;
}

void ConfigureNotifyHelper::clear() {
    store.clear();
}

CursorHelper::CursorHelper(xcb_connection_t* fconn, xcb_screen_t* fscr) {
    p = make_unique<Priv>();
    p->fconn = fconn;
    p->fscr = fscr;
    // cursor context for retreiving semantic_name from theme
    if (xcb_cursor_context_new(fconn, fscr, &p->xcursor_ctx) < 0) {
        fputs(_("Cannot initialize XCursor context."), stdout);
        putchar(' ');
        puts(stringconsts::values().WARN_POINTER_FWD);
        p->init_success = false;
        return;
    }
    // XRender format
    if (auto xrender_format_sel_p =
            xcb_render_util_find_standard_format(xcb_render_util_query_formats(fconn), XCB_PICT_STANDARD_ARGB_32)) {
        p->renderdata.format = *xrender_format_sel_p;
    } else {
        LOG(ERROR) << "Cannot select RENDER format";
        printf(stringconsts::values().ERROR_XRENDER_FMT_NOT_AVAIL, "ARGB_32");
        putchar(' ');
        puts(stringconsts::values().WARN_POINTER_FWD);
        p->init_success = false;
        return;
    }
    // GC, for drawing to frontend pixmap
    p->renderdata.pxm_base = make_unique<Pixmap>(p->fconn, 32, p->fscr->root, 1, 1);
    p->renderdata.gc = make_unique<GC>(p->fconn, p->renderdata.pxm_base->id, 0, nullptr);
}

CursorHelper::~CursorHelper() {
    xcb_cursor_context_free(p->xcursor_ctx);
    p->free_cursors();
}

void CursorHelper::change_cursor(sharedds::CursorData cur) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    // prefer data to semantic
    if (cur.data.format) {
        if (cur.data.format == 32) {
            // recreate backing pixmap and rp if diff size
            if (p->renderdata.curr_sz != cur.data.size) {
                p->renderdata.curr_sz = cur.data.size;
                p->renderdata.pxm_curr =
                    make_unique<Pixmap>(p->fconn, 32, p->fscr->root, cur.data.size.first, cur.data.size.second);
                p->renderdata.rp_curr = make_unique<XRenderPicture>(
                    p->fconn, p->renderdata.pxm_curr->id, p->renderdata.format.id, 0, nullptr);
            }
            // draw
            xcb_put_image(p->fconn, XCB_IMAGE_FORMAT_Z_PIXMAP, p->renderdata.pxm_curr->id, p->renderdata.gc->id,
                cur.data.size.first, cur.data.size.second, 0, 0, 0, 32, cur.data.size.first * cur.data.size.second * 4,
                (uint8_t*)cur.data.image);
            // to cursor
            p->new_cursor_id();
            err.pop() = xcb_request_check(
                p->fconn, xcb_render_create_cursor_checked(p->fconn, p->curr_cursor, p->renderdata.rp_curr->id,
                              cur.data.hotspot.first, cur.data.hotspot.second));
            if (err.get()) {
                LOG(WARNING) << "Cannot create RENDER cursor: " << xcb_event_get_error_label(err.get()->error_code);
            }
        } else {
            LOG(FATAL) << "Received non 32bit ARGB cursor data.";
        }
    } else {
        p->new_cursor_id(xcb_cursor_load_cursor(p->xcursor_ctx, cur.semantic_name.c_str()));
    }
}

void CursorHelper::assign_to_window(xcb_window_t fwinid) {
    if (p->curr_cursor) {
        uint32_t cw_flag = XCB_CW_CURSOR;
        uint32_t cw_vals[] = {p->curr_cursor};
        xcb_change_window_attributes_checked(p->fconn, fwinid, cw_flag, cw_vals);
    }
}

void CursorHelper::Priv::new_cursor_id() {
    new_cursor_id(xcb_generate_id(fconn));
}

void CursorHelper::Priv::new_cursor_id(int existing_id) {
    if (cursor_objs.size() >= MAX_CURSOR_OBJ) {
        free_cursors();
    }
    curr_cursor = existing_id;
    cursor_objs.push_back(curr_cursor);
}

void CursorHelper::Priv::free_cursors() {
    // no need to sync
    for (xcb_cursor_t cursor : cursor_objs) {
        xcb_free_cursor(fconn, cursor);
    }
    cursor_objs.clear();
}