#include "common/customxcb/customxcb.h"
#include <cstdlib>
#include <cstring>

xcb_void_cookie_t pxwm::common::customxcb::xcb_custom_xkb_latch_lock_state_checked(xcb_connection_t* c,
    xcb_xkb_device_spec_t deviceSpec,
    uint8_t affectModLocks,
    uint8_t modLocks,
    uint8_t lockGroup,
    uint8_t groupLock,
    uint8_t affectModLatches,
    uint8_t modLatches,
    uint8_t latchGroup,
    uint16_t groupLatch) {
    static const xcb_protocol_request_t xcb_req = {/* count */ 2,
        /* ext */ &xcb_xkb_id,
        /* opcode */ XCB_XKB_LATCH_LOCK_STATE,
        /* isvoid */ 1};

    struct iovec xcb_parts[4];
    xcb_void_cookie_t xcb_ret;
    xcb_xkb_latch_lock_state_request_t xcb_out;

    xcb_out.deviceSpec = deviceSpec;
    xcb_out.affectModLocks = affectModLocks;
    xcb_out.modLocks = modLocks;
    xcb_out.lockGroup = lockGroup;
    xcb_out.groupLock = groupLock;
    xcb_out.affectModLatches = affectModLatches;
    xcb_out.pad0 = modLatches;
    xcb_out.pad1 = 0;
    xcb_out.latchGroup = latchGroup;
    xcb_out.groupLatch = groupLatch;

    xcb_parts[2].iov_base = (char*)&xcb_out;
    xcb_parts[2].iov_len = sizeof(xcb_out);
    xcb_parts[3].iov_base = 0;
    xcb_parts[3].iov_len = -xcb_parts[2].iov_len & 3;

    xcb_ret.sequence = xcb_send_request(c, XCB_REQUEST_CHECKED, xcb_parts + 2, &xcb_req);
    return xcb_ret;
}

int pxwm::common::customxcb::xcb_custom_xkb_set_map_values_serialize(void** _buffer,
    uint8_t nTypes,
    uint8_t nKeySyms,
    uint8_t nKeyActions,
    uint16_t totalActions,
    uint8_t totalKeyBehaviors,
    uint16_t virtualMods,
    uint8_t totalKeyExplicit,
    uint8_t totalModMapKeys,
    uint8_t totalVModMapKeys,
    uint16_t present,
    const xcb_xkb_set_map_values_t* _aux) {
    uint8_t* xcb_out = (uint8_t*)*_buffer;
    unsigned int xcb_buffer_len = 0;
    unsigned int xcb_align_to = 0;

    unsigned int xcb_pad = 0;
    char xcb_pad0[3] = {0, 0, 0};
    struct iovec xcb_parts[19];
    unsigned int xcb_parts_idx = 0;
    unsigned int xcb_block_len = 0;
    unsigned int i;
    uint8_t* xcb_tmp;

    if (present & XCB_XKB_MAP_PART_KEY_TYPES) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* types */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->types;
        xcb_parts[xcb_parts_idx].iov_len = 0;
        xcb_tmp = (uint8_t*)_aux->types;
        for (i = 0; i < nTypes; i++) {
            xcb_block_len = xcb_xkb_set_key_type_sizeof(xcb_tmp);
            xcb_parts[xcb_parts_idx].iov_len += xcb_block_len;
            xcb_tmp += xcb_block_len;
        }
        xcb_block_len = xcb_parts[xcb_parts_idx].iov_len;
        xcb_parts_idx++;
        //        xcb_align_to = ALIGNOF(xcb_xkb_set_key_type_t);
        xcb_align_to = 4;
    }
    if (present & XCB_XKB_MAP_PART_KEY_SYMS) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* syms */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->syms;
        xcb_parts[xcb_parts_idx].iov_len = 0;
        xcb_tmp = (uint8_t*)_aux->syms;
        for (i = 0; i < nKeySyms; i++) {
            xcb_block_len = xcb_xkb_key_sym_map_sizeof(xcb_tmp);
            xcb_parts[xcb_parts_idx].iov_len += xcb_block_len;
            xcb_tmp += xcb_block_len;
        }
        xcb_block_len = xcb_parts[xcb_parts_idx].iov_len;
        xcb_parts_idx++;
        //        xcb_align_to = ALIGNOF(xcb_xkb_key_sym_map_t);
        xcb_align_to = 4;
    }
    if (present & XCB_XKB_MAP_PART_KEY_ACTIONS) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* actionsCount */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->actionsCount;
        xcb_block_len += nKeyActions * sizeof(xcb_keycode_t);
        xcb_parts[xcb_parts_idx].iov_len = nKeyActions * sizeof(xcb_keycode_t);
        xcb_parts_idx++;
        //        xcb_align_to = align_of(uint8_t);
        xcb_align_to = 4;
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* actions */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->actions;
        xcb_block_len += totalActions * sizeof(xcb_xkb_action_t);
        xcb_parts[xcb_parts_idx].iov_len = totalActions * sizeof(xcb_xkb_action_t);
        xcb_parts_idx++;
        //        xcb_align_to = align_of(xcb_xkb_action_t);
        xcb_align_to = 4;
    }
    if (present & XCB_XKB_MAP_PART_KEY_BEHAVIORS) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* behaviors */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->behaviors;
        xcb_block_len += totalKeyBehaviors * sizeof(xcb_xkb_set_behavior_t);
        xcb_parts[xcb_parts_idx].iov_len = totalKeyBehaviors * sizeof(xcb_xkb_set_behavior_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (present & XCB_XKB_MAP_PART_VIRTUAL_MODS) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* vmods */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->vmods;
        xcb_block_len += xcb_popcount(virtualMods) * sizeof(xcb_keycode_t);
        xcb_parts[xcb_parts_idx].iov_len = xcb_popcount(virtualMods) * sizeof(xcb_keycode_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (present & XCB_XKB_MAP_PART_EXPLICIT_COMPONENTS) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* explicit */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->explicit1;
        xcb_block_len += totalKeyExplicit * sizeof(xcb_xkb_set_explicit_t);
        xcb_parts[xcb_parts_idx].iov_len = totalKeyExplicit * sizeof(xcb_xkb_set_explicit_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (present & XCB_XKB_MAP_PART_MODIFIER_MAP) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* modmap */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->modmap;
        xcb_block_len += totalModMapKeys * sizeof(xcb_xkb_key_mod_map_t);
        xcb_parts[xcb_parts_idx].iov_len = totalModMapKeys * sizeof(xcb_xkb_key_mod_map_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (present & XCB_XKB_MAP_PART_VIRTUAL_MOD_MAP) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* vmodmap */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->vmodmap;
        xcb_block_len += totalVModMapKeys * sizeof(xcb_xkb_key_v_mod_map_t);
        xcb_parts[xcb_parts_idx].iov_len = totalVModMapKeys * sizeof(xcb_xkb_key_v_mod_map_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    /* insert padding */
    xcb_pad = -xcb_block_len & (xcb_align_to - 1);
    xcb_buffer_len += xcb_block_len + xcb_pad;
    if (0 != xcb_pad) {
        xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
        xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
        xcb_parts_idx++;
        xcb_pad = 0;
    }
    xcb_block_len = 0;

    if (NULL == xcb_out) {
        /* allocate memory */
        xcb_out = (uint8_t*)calloc(xcb_buffer_len, 1);
        *_buffer = xcb_out;
    }

    xcb_tmp = xcb_out;
    for (i = 0; i < xcb_parts_idx; i++) {
        if (0 != xcb_parts[i].iov_base && 0 != xcb_parts[i].iov_len)
            memcpy(xcb_tmp, xcb_parts[i].iov_base, xcb_parts[i].iov_len);
        if (0 != xcb_parts[i].iov_len)
            xcb_tmp += xcb_parts[i].iov_len;
    }

    return xcb_buffer_len;
}

xcb_void_cookie_t pxwm::common::customxcb::xcb_custom_xkb_set_map_checked(xcb_connection_t* c,
    xcb_xkb_set_map_request_t& xcb_out,
    const void* values,
    int vallen) {
    static const xcb_protocol_request_t xcb_req = {/* count */ 3,
        /* ext */ &xcb_xkb_id,
        /* opcode */ XCB_XKB_SET_MAP,
        /* isvoid */ 1};

    struct iovec xcb_parts[5];
    xcb_void_cookie_t xcb_ret;

    xcb_parts[2].iov_base = (char*)&xcb_out;
    xcb_parts[2].iov_len = sizeof(xcb_out);
    xcb_parts[3].iov_base = 0;
    xcb_parts[3].iov_len = -xcb_parts[2].iov_len & 3;
    /* xcb_xkb_set_map_values_t values */
    xcb_parts[4].iov_base = (char*)values;
    xcb_parts[4].iov_len = vallen;

    xcb_ret.sequence = xcb_send_request(c, XCB_REQUEST_CHECKED, xcb_parts + 2, &xcb_req);
    return xcb_ret;
}

int pxwm::common::customxcb::xcb_custom_xkb_set_names_values_serialize(void** _buffer,
    uint8_t nTypes,
    uint8_t nKTLevels,
    uint32_t indicators,
    uint16_t virtualMods,
    uint8_t groupNames,
    uint8_t nKeys,
    uint8_t nKeyAliases,
    uint8_t nRadioGroups,
    uint32_t which,
    const xcb_xkb_set_names_values_t* _aux) {
    uint8_t* xcb_out = (uint8_t*)*_buffer;
    unsigned int xcb_buffer_len = 0;
    unsigned int xcb_align_to = 0;

    unsigned int xcb_pad = 0;
    char xcb_pad0[3] = {0, 0, 0};
    struct iovec xcb_parts[25];
    unsigned int xcb_parts_idx = 0;
    unsigned int xcb_block_len = 0;
    unsigned int i;
    uint8_t* xcb_tmp;

    if (which & XCB_XKB_NAME_DETAIL_KEYCODES) {
        /* xcb_xkb_set_names_values_t.keycodesName */
        xcb_parts[xcb_parts_idx].iov_base = (char*)&_aux->keycodesName;
        xcb_block_len += sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_GEOMETRY) {
        /* xcb_xkb_set_names_values_t.geometryName */
        xcb_parts[xcb_parts_idx].iov_base = (char*)&_aux->geometryName;
        xcb_block_len += sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_SYMBOLS) {
        /* xcb_xkb_set_names_values_t.symbolsName */
        xcb_parts[xcb_parts_idx].iov_base = (char*)&_aux->symbolsName;
        xcb_block_len += sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_PHYS_SYMBOLS) {
        /* xcb_xkb_set_names_values_t.physSymbolsName */
        xcb_parts[xcb_parts_idx].iov_base = (char*)&_aux->physSymbolsName;
        xcb_block_len += sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_TYPES) {
        /* xcb_xkb_set_names_values_t.typesName */
        xcb_parts[xcb_parts_idx].iov_base = (char*)&_aux->typesName;
        xcb_block_len += sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_COMPAT) {
        /* xcb_xkb_set_names_values_t.compatName */
        xcb_parts[xcb_parts_idx].iov_base = (char*)&_aux->compatName;
        xcb_block_len += sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_KEY_TYPE_NAMES) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* typeNames */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->typeNames;
        xcb_block_len += nTypes * sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = nTypes * sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_KT_LEVEL_NAMES) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* nLevelsPerType */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->nLevelsPerType;
        xcb_block_len += nKTLevels * sizeof(uint8_t);
        xcb_parts[xcb_parts_idx].iov_len = nKTLevels * sizeof(uint8_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* ktLevelNames */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->ktLevelNames;
        xcb_block_len += xcb_sumof(_aux->nLevelsPerType, nKTLevels) * sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = xcb_sumof(_aux->nLevelsPerType, nKTLevels) * sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_INDICATOR_NAMES) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* indicatorNames */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->indicatorNames;
        xcb_block_len += xcb_popcount(indicators) * sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = xcb_popcount(indicators) * sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_VIRTUAL_MOD_NAMES) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* virtualModNames */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->virtualModNames;
        xcb_block_len += xcb_popcount(virtualMods) * sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = xcb_popcount(virtualMods) * sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_GROUP_NAMES) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* groups */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->groups;
        xcb_block_len += xcb_popcount(groupNames) * sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = xcb_popcount(groupNames) * sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_KEY_NAMES) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* keyNames */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->keyNames;
        xcb_block_len += nKeys * sizeof(xcb_xkb_key_name_t);
        xcb_parts[xcb_parts_idx].iov_len = nKeys * sizeof(xcb_xkb_key_name_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_KEY_ALIASES) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* keyAliases */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->keyAliases;
        xcb_block_len += nKeyAliases * sizeof(xcb_xkb_key_alias_t);
        xcb_parts[xcb_parts_idx].iov_len = nKeyAliases * sizeof(xcb_xkb_key_alias_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    if (which & XCB_XKB_NAME_DETAIL_RG_NAMES) {
        /* insert padding */
        xcb_pad = -xcb_block_len & (xcb_align_to - 1);
        xcb_buffer_len += xcb_block_len + xcb_pad;
        if (0 != xcb_pad) {
            xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
            xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
            xcb_parts_idx++;
            xcb_pad = 0;
        }
        xcb_block_len = 0;
        /* radioGroupNames */
        xcb_parts[xcb_parts_idx].iov_base = (char*)_aux->radioGroupNames;
        xcb_block_len += nRadioGroups * sizeof(xcb_atom_t);
        xcb_parts[xcb_parts_idx].iov_len = nRadioGroups * sizeof(xcb_atom_t);
        xcb_parts_idx++;
        xcb_align_to = 4;
    }
    /* insert padding */
    xcb_pad = -xcb_block_len & (xcb_align_to - 1);
    xcb_buffer_len += xcb_block_len + xcb_pad;
    if (0 != xcb_pad) {
        xcb_parts[xcb_parts_idx].iov_base = xcb_pad0;
        xcb_parts[xcb_parts_idx].iov_len = xcb_pad;
        xcb_parts_idx++;
        xcb_pad = 0;
    }
    xcb_block_len = 0;

    if (NULL == xcb_out) {
        /* allocate memory */
        xcb_out = (uint8_t*)malloc(xcb_buffer_len);
        *_buffer = xcb_out;
    }

    xcb_tmp = xcb_out;
    for (i = 0; i < xcb_parts_idx; i++) {
        if (0 != xcb_parts[i].iov_base && 0 != xcb_parts[i].iov_len)
            memcpy(xcb_tmp, xcb_parts[i].iov_base, xcb_parts[i].iov_len);
        if (0 != xcb_parts[i].iov_len)
            xcb_tmp += xcb_parts[i].iov_len;
    }

    return xcb_buffer_len;
}

xcb_void_cookie_t pxwm::common::customxcb::xcb_custom_xkb_set_names_checked(xcb_connection_t* c,
    xcb_xkb_device_spec_t deviceSpec,
    uint16_t virtualMods,
    uint32_t which,
    uint8_t firstType,
    uint8_t nTypes,
    uint8_t firstKTLevelt,
    uint8_t nKTLevels,
    uint32_t indicators,
    uint8_t groupNames,
    uint8_t nRadioGroups,
    xcb_keycode_t firstKey,
    uint8_t nKeys,
    uint8_t nKeyAliases,
    uint16_t totalKTLevelNames,
    const void* values,
    int vallen) {
    static const xcb_protocol_request_t xcb_req = {/* count */ 3,
        /* ext */ &xcb_xkb_id,
        /* opcode */ XCB_XKB_SET_NAMES,
        /* isvoid */ 1};

    struct iovec xcb_parts[5];
    xcb_void_cookie_t xcb_ret;
    xcb_xkb_set_names_request_t xcb_out;

    xcb_out.deviceSpec = deviceSpec;
    xcb_out.virtualMods = virtualMods;
    xcb_out.which = which;
    xcb_out.firstType = firstType;
    xcb_out.nTypes = nTypes;
    xcb_out.firstKTLevelt = firstKTLevelt;
    xcb_out.nKTLevels = nKTLevels;
    xcb_out.indicators = indicators;
    xcb_out.groupNames = groupNames;
    xcb_out.nRadioGroups = nRadioGroups;
    xcb_out.firstKey = firstKey;
    xcb_out.nKeys = nKeys;
    xcb_out.nKeyAliases = nKeyAliases;
    xcb_out.pad0 = 0;
    xcb_out.totalKTLevelNames = totalKTLevelNames;

    xcb_parts[2].iov_base = (char*)&xcb_out;
    xcb_parts[2].iov_len = sizeof(xcb_out);
    xcb_parts[3].iov_base = 0;
    xcb_parts[3].iov_len = -xcb_parts[2].iov_len & 3;
    /* xcb_xkb_set_names_values_t values */
    xcb_parts[4].iov_base = (char*)values;
    xcb_parts[4].iov_len = vallen;

    xcb_ret.sequence = xcb_send_request(c, XCB_REQUEST_CHECKED, xcb_parts + 2, &xcb_req);
    return xcb_ret;
}