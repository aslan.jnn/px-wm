#include "common/selmgr.h"
#include "common/x11.h"
#include "shared/memory.h"
#include "shared/util.h"
#include <algorithm>
#include <deque>
#include <glog/logging.h>
#include <unordered_set>
#include <xcb/xcb_util.h>

using namespace std;
using namespace px;
using namespace px::util;
using namespace px::memory;
using namespace pxwm;
using namespace pxwm::sharedds;
using namespace pxwm::common::x11;

namespace pxwm {
    namespace common {
        namespace x11 {
            struct multiple_atom_pair_t {
                xcb_atom_t target;
                xcb_atom_t property;
            };
            class SelConverterPriv {
            public:
                static uint32_t swap_endian(uint32_t num);
            };
        }
    }
}

class SelMgr::Priv {
public:
    static constexpr uint32_t INCR_SIZE = 262144;
    struct QueueData;
    struct RequestorListenedEvents;
    struct {
        xcb_atom_t multiple;
        xcb_atom_t atom_pair;
    } xa;
    struct reqprop_translate {
        xcb_connection_t* conn;
        const string prefix = "_PX_SELMGR";
        xcb_atom_t generate_name(uint32_t requestor_id, xcb_atom_t prop);
        pair<uint32_t, xcb_atom_t> parse(xcb_atom_t atom);
    } reqprop_translate;

    const SelMgr* parent;
    bool usable = false;
    xcb_connection_t* conn;
    unordered_set<xcb_atom_t> sel_atoms;
    vector<xcb_atom_t> managed_sel_atom_list;
    // these targets are known to request display-specific data, which is very likely irrelevant accross displays.
    unordered_set<xcb_atom_t> blacklist_target_atoms;
    // NOTICE: If selection owner doesn't reply and the requestor window closes, data in this map will be here forever!
    typedef map<xcb_window_t, pair<RequestorListenedEvents, deque<QueueData>>> requestor_map_t;
    requestor_map_t requestor_map;
    deque<uint32_t> requestor_ids;

    Priv(SelMgr* parent) : parent{parent} {}

    bool add_event_listener(xcb_window_t wid, uint32_t add_event_mask);
    void notify_requestor(xcb_window_t requestor, xcb_atom_t sel, xcb_atom_t target, xcb_atom_t prop);

    XProperty get_selection_reply(EventPooler* eventpooler, xcb_atom_t prop);

    void assign_properties(xcb_window_t requestor, vector<XProperty>& properties);
    void set_multiple_property(xcb_window_t requestor,
        xcb_atom_t mult_prop,
        pair<int, multiple_atom_pair_t*> mult_req,
        vector<XProperty>& replies);
    void incr_transfer(EventPooler* eventpooler, xcb_window_t requestor, XProperty& prop);
};

constexpr uint32_t SelMgr::Priv::INCR_SIZE;

struct SelMgr::Priv::QueueData {
    xcb_atom_t sel;
    xcb_atom_t prop;
    xcb_atom_t target;
    struct {
        int pair_count = 0;
        shared_ptr<multiple_atom_pair_t> data;
        int queue = 0;
    } multiple;
};

struct SelMgr::Priv::RequestorListenedEvents {
    bool destroy = false;
    bool property_change = false;
};

SelMgr::SelMgr(xcb_connection_t* conn, xcb_screen_t* scr) : proxy_wid{xcb_generate_id(conn)} {
    static const string sel_atoms_str[] = {"PRIMARY", "SECONDARY", "CLIPBOARD"};
    static const string blacklist_sel_target_atoms_str[] = {
        "INSERT_SELECTION", "INSERT_PROPERTY", "DRAWABLE", "PIXMAP", "COLORMAP", "BITMAP", "TIMESTAMP", "DELETE"};

    p = make_unique<Priv>(this);
    p->reqprop_translate.conn = p->conn = conn;
    // accepted selection redirection
    for (auto& str : sel_atoms_str) {
        auto atom = get_atom(conn, false, str);
        p->sel_atoms.insert(atom);
        p->managed_sel_atom_list.push_back(atom);
    }
    // blacklisted selection targets
    for (auto& str : blacklist_sel_target_atoms_str) {
        p->blacklist_target_atoms.insert(get_atom(conn, false, str));
    }

    UniquePointerWrapper_free<xcb_generic_error_t> err;
    uint32_t cw_flag = XCB_CW_EVENT_MASK;
    uint32_t cw_vals[] = {XCB_EVENT_MASK_PROPERTY_CHANGE};
    err.pop() =
        xcb_request_check(conn, xcb_create_window_checked(conn, XCB_COPY_FROM_PARENT, proxy_wid, scr->root, 0, 0, 1, 1,
                                    0, XCB_WINDOW_CLASS_INPUT_OUTPUT, XCB_COPY_FROM_PARENT, cw_flag, cw_vals));
    if (err.get()) {
        LOG(ERROR) << "Cannot create clipboard proxy window.";
        return;
    }
    // track selection owner change (on 3 fundamental ICCCM selections)
    uint32_t selinput_mask = XCB_XFIXES_SELECTION_EVENT_MASK_SET_SELECTION_OWNER |
        XCB_XFIXES_SELECTION_EVENT_MASK_SELECTION_WINDOW_DESTROY |
        XCB_XFIXES_SELECTION_EVENT_MASK_SELECTION_CLIENT_CLOSE;
    for (auto sel : p->sel_atoms) {
        err.pop() =
            xcb_request_check(conn, xcb_xfixes_select_selection_input_checked(conn, proxy_wid, sel, selinput_mask));
        if (err.get()) {
            LOG(WARNING) << "Cannot select selection owner change events. Clipboard redirection won't work properly";
        }
    }
    // commonly used atoms
    p->xa.multiple = get_atom(p->conn, false, "MULTIPLE");
    p->xa.atom_pair = get_atom(p->conn, false, "ATOM_PAIR");
    // OK!
    p->usable = true;
}

SelMgr::~SelMgr() {
    xcb_destroy_window(p->conn, proxy_wid);
}

bool SelMgr::is_usable() {
    return p->usable;
}

const std::vector<xcb_atom_t> SelMgr::managed_selections() {
    return p->managed_sel_atom_list;
}

void SelMgr::destroy_requestor_data(xcb_window_t wid) {
    if (p->requestor_map.count(wid)) {
        p->requestor_map.erase(wid);
    }
}

void SelMgr::acquire_selection(xcb_atom_t sel) {
    if (!p->sel_atoms.count(sel)) {
        return;
    }
    xcb_set_selection_owner(p->conn, proxy_wid, sel, XCB_CURRENT_TIME);
    xcb_flush(p->conn);
}

void SelMgr::release_selection(xcb_atom_t sel) {
    if (!p->sel_atoms.count(sel)) {
        return;
    }
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto rep = xcb_get_selection_owner_reply(p->conn, xcb_get_selection_owner(p->conn, sel), &err.pop());
    if (rep->owner == proxy_wid) {
        xcb_set_selection_owner(p->conn, XCB_NONE, sel, XCB_CURRENT_TIME);
        xcb_flush(p->conn);
    }
}

pair<SelectionInfo, vector<SelectionRequest>> SelMgr::handle_request(xcb_selection_request_event_t* cevt) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    // reject all blacklisted target and unhandled selections
    if (!p->sel_atoms.count(cevt->selection) || p->blacklist_target_atoms.count(cevt->target)) {
        p->notify_requestor(cevt->requestor, cevt->selection, cevt->target, XCB_NONE);
        return {};
    }
    // prop for legacy requestors (according to ICCCM)
    xcb_atom_t prop = cevt->property == XCB_NONE ? cevt->target : cevt->property;

    auto& requestor_data = p->requestor_map[cevt->requestor];
    // check and assign destroy event listener
    if (!requestor_data.first.destroy) {
        if (!p->add_event_listener(cevt->requestor, XCB_EVENT_MASK_STRUCTURE_NOTIFY)) {
            return {};
        }
        requestor_data.first.destroy = true;
    }

    vector<SelectionRequest> ret;
    // multiple?
    if (cevt->target == get_atom(p->conn, false, "MULTIPLE")) {
        // MULTIPLE w/o properties must be rejected
        if (cevt->property == XCB_NONE) {
            p->notify_requestor(cevt->requestor, cevt->selection, cevt->target, XCB_NONE);
            return {};
        }
        // check if the request is valid
        auto proprep = make_unique_malloc(xcb_get_property_reply(p->conn,
            xcb_get_property(p->conn, 0, cevt->requestor, prop, XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE),
            &err.pop()));
        if (err.get() || proprep->format != 32 || !proprep->value_len || (proprep->value_len % 2)) {
            if (err.get()) {
                LOG(WARNING) << "Error retrieving property: " << xcb_event_get_error_label(err.get()->response_type);
            }
            p->notify_requestor(cevt->requestor, cevt->selection, cevt->target, XCB_NONE);
            return {};
        }
        // loop thru the ATOM_PAIRs
        auto mult_atoms = (multiple_atom_pair_t*)xcb_get_property_value(proprep.get());
        for (int i = 0; i < proprep->value_len / 2; ++i) {
            // don't allow blacklisted targets AND nested multiple
            if (!p->blacklist_target_atoms.count(mult_atoms[i].target) &&
                (mult_atoms[i].target != get_atom(p->conn, false, "MULTIPLE"))) {
                SelectionRequest req;
                req.target = mult_atoms[i].target;
                req.param.prop_atom = mult_atoms[i].property;
                auto imul_param = make_unique_malloc(
                    xcb_get_property_reply(p->conn, xcb_get_property(p->conn, 0, cevt->requestor, req.param.prop_atom,
                                                        XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE),
                        &err.pop()));
                if (err.get()) {
                    LOG(ERROR) << "Cannot retrieve MULTIPLE's component parameter property: "
                               << xcb_event_get_error_label(err.get()->error_code);
                    continue;
                }
                req.param.value.prop_type = imul_param.get()->type;
                req.param.value.format_bpp = imul_param.get()->format;
                req.param.value.data_len = xcb_get_property_value_length(imul_param.get());
                req.param.value.data_ptr = make_shared_malloc(malloc(req.param.value.data_len));
                memcpy(
                    req.param.value.data_ptr.get(), xcb_get_property_value(imul_param.get()), req.param.value.data_len);
                ret.push_back(req);
            }
        }
        // enqueue
        Priv::QueueData mult_queue{cevt->selection, prop, cevt->target};
        mult_queue.multiple.pair_count = proprep->value_len / 2;
        uint32_t atom_pair_sz = mult_queue.multiple.pair_count * sizeof(multiple_atom_pair_t);
        mult_queue.multiple.data = make_shared_malloc((multiple_atom_pair_t*)malloc(atom_pair_sz));
        memcpy(mult_queue.multiple.data.get(), mult_atoms, atom_pair_sz);
        requestor_data.second.push_back(mult_queue);
        // return
        SelectionInfo selinfo;
        selinfo.selection = cevt->selection;
        selinfo.multiple = prop;
        return {selinfo, ret};
    } else {
        // requests return
        SelectionRequest req;
        req.target = cevt->target;
        req.param.prop_atom = prop;
        auto sr_param = make_unique_malloc(
            xcb_get_property_reply(p->conn, xcb_get_property(p->conn, 0, cevt->requestor, cevt->property,
                                                XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE),
                &err.pop()));
        if (err.get()) {
            LOG(ERROR) << "Cannot retrieve parameter property: " << xcb_event_get_error_label(err.get()->error_code);
            p->notify_requestor(cevt->requestor, cevt->selection, cevt->target, XCB_NONE);
            return {};
        }
        req.param.value.prop_type = sr_param.get()->type;
        req.param.value.format_bpp = sr_param.get()->format;
        req.param.value.data_len = xcb_get_property_value_length(sr_param.get());
        req.param.value.data_ptr = make_shared_malloc(malloc(req.param.value.data_len));
        memcpy(req.param.value.data_ptr.get(), xcb_get_property_value(sr_param.get()), req.param.value.data_len);
        ret.push_back(req);
        // enqueue
        Priv::QueueData sr_queue{cevt->selection, prop, cevt->target};
        requestor_data.second.push_back(sr_queue);
        // return
        SelectionInfo selinfo;
        selinfo.selection = cevt->selection;
        selinfo.sr_prop = prop;
        return {selinfo, ret};
    }
}

void SelMgr::supply_data(EventPooler* eventpooler,
    xcb_window_t requestor,
    SelectionInfo sel,
    vector<XProperty>& properties) {
    // bogus?
    if (properties.size() > 1 && !sel.multiple) {
        return;
    }
    // check if the window actually requested a selection
    Priv::requestor_map_t::iterator wit;
    if (check_and_get_mapped_value(p->requestor_map, requestor, wit)) {
        const xcb_atom_t xa_multiple = get_atom(p->conn, false, "MULTIPLE");
        if (wit->second.second.size() && (sel.multiple || sel.sr_prop)) {
            // iterate thru queue for matching request. no matching request means that this reply is bogus, which means
            // we shouldn't reject any requests.
            deque<Priv::QueueData>::iterator req_it;
            int queue_count = 0;
            for (req_it = wit->second.second.begin(); req_it != wit->second.second.end(); ++req_it, ++queue_count) {
                if (req_it->sel == sel.selection) {
                    if (sel.multiple && req_it->prop == sel.multiple && req_it->target == xa_multiple) {
                        break;
                    } else if (sel.sr_prop && req_it->prop == sel.sr_prop && req_it->target != xa_multiple) {
                        break;
                    }
                }
            }
            // this condition check will ignore the operations if no match found
            // we expect selection owner to reply data in the order given by the requestor (from ICCCM spec)
            if (req_it != wit->second.second.end()) {
                // send empty SelectionNotify events for request "skipped" by the selection owner
                // also delete the selected request if reply is a rejection
                if (!properties.size()) {
                    ++queue_count;
                }
                while (queue_count--) {
                    auto& req = wit->second.second.front();
                    p->notify_requestor(requestor, req.sel, req.target, req.prop);
                    wit->second.second.pop_front();
                }
                // handle this selection reply
                if (properties.size()) {
                    auto& req = wit->second.second.front();
                    // assign properties to target windows
                    p->assign_properties(requestor, properties);
                    // notify
                    if (sel.multiple) {
                        // reset MULTIPLE property in requestor and notify requestor
                        p->set_multiple_property(
                            requestor, sel.multiple, {req.multiple.pair_count, req.multiple.data.get()}, properties);
                        p->notify_requestor(
                            requestor, sel.selection, get_atom(p->conn, false, "MULTIPLE"), sel.multiple);
                    } else {
                        p->notify_requestor(requestor, sel.selection, req.target, sel.sr_prop);
                    }
                    // handle those INCR properties (should be done after SelectionNotify)
                    for (auto& property : properties) {
                        if (property.value.data_len > Priv::INCR_SIZE) {
                            // listen to requestor's property change
                            if (!wit->second.first.property_change) {
                                if (!p->add_event_listener(requestor, XCB_EVENT_MASK_PROPERTY_CHANGE)) {
                                    // seriously, we can't do anything about it!
                                    continue;
                                }
                            }
                            p->incr_transfer(eventpooler, requestor, property);
                        }
                    }
                    // POP!
                    wit->second.second.pop_front();
                }
            }
        } else {
            // request rejected
            p->notify_requestor(requestor, sel.selection, wit->second.second.front().target, XCB_NONE);
            wit->second.second.pop_front();
        }
    }
}

bool SelMgr::request_selection(uint32_t id, SelectionInfo selinfo, vector<SelectionRequest>& preqs) {
    // local copy
    vector<SelectionRequest> reqs = preqs;
    // convert properties to (very likely) unique property names
    if (selinfo.multiple) {
        selinfo.multiple = p->reqprop_translate.generate_name(id, selinfo.multiple);
    } else if (selinfo.sr_prop) {
        selinfo.sr_prop = p->reqprop_translate.generate_name(id, selinfo.sr_prop);
    }
    for (auto& req : reqs) {
        req.param.prop_atom = p->reqprop_translate.generate_name(id, req.param.prop_atom);
    }

    // prepare target property (some request target may be parameterized)
    for (auto& req : reqs) {
        // fill or delete the property?
        if (req.param.value.format_bpp) {
            xcb_change_property(p->conn, XCB_PROP_MODE_REPLACE, proxy_wid, req.param.prop_atom,
                req.param.value.prop_type, req.param.value.format_bpp,
                req.param.value.data_len / (req.param.value.format_bpp / 8), req.param.value.data_ptr.get());
        } else {
            xcb_delete_property(p->conn, proxy_wid, req.param.prop_atom);
        }
    }
    xcb_atom_t sel_target, property;
    // is MULTIPLE?
    if (selinfo.multiple) {
        sel_target = p->xa.multiple;
        property = selinfo.multiple;
        // value of MULTIPLE property. consists of ATOM_PAIRs. first = selection target/type. second = property.
        auto mpv = make_unique_malloc((multiple_atom_pair_t*)malloc(sizeof(xcb_atom_t) * reqs.size() * 2));
        auto p_mpv = mpv.get();
        for (int i = 0; i < reqs.size(); ++i) {
            p_mpv[i].target = reqs[i].target;
            p_mpv[i].property = reqs[i].param.prop_atom;
        }
        // write to server and sync
        xcb_change_property(
            p->conn, XCB_PROP_MODE_REPLACE, proxy_wid, property, p->xa.atom_pair, 32, reqs.size() * 2, p_mpv);
        xcb_flush(p->conn);
    } else {
        sel_target = reqs[0].target;
        property = reqs[0].param.prop_atom; // or selinfo.sr_prop
    }
    // request the selection owner
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    err.pop() = xcb_request_check(
        p->conn, xcb_convert_selection(p->conn, proxy_wid, selinfo.selection, sel_target, property, XCB_CURRENT_TIME));
    if (err.get()) {
        LOG(ERROR) << "Error request selection: " << xcb_event_get_error_label(err.get()->error_code);
        return false;
    }
    // done
    p->requestor_ids.push_back(id);
    return true;
}

tuple<uint32_t, SelectionInfo, vector<XProperty>> SelMgr::parse_selection_notify(common::x11::EventPooler* eventpooler,
    xcb_selection_notify_event_t* cevt) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    SelectionInfo retinfo;
    retinfo.selection = cevt->selection;

    // conversion has failed
    if (cevt->property == XCB_ATOM_NONE) {
        uint32_t requestor = p->requestor_ids.front();
        p->requestor_ids.pop_front();
        return make_tuple(requestor, retinfo, vector<XProperty>());
    }
    // bogus?
    if (cevt->requestor != proxy_wid) {
        LOG(WARNING) << "SelMgr SelectionNotify: window ID mismatch";
        return {};
    }

    vector<XProperty> ret;
    auto parsed_prop_name = p->reqprop_translate.parse(cevt->property);

    // check requestor queue
    if (p->requestor_ids.front() == parsed_prop_name.first) {
        p->requestor_ids.pop_front();
    } else {
        LOG(WARNING) << "Selection owner doesn't reply in correct order";
        deque<uint32_t>::iterator it;
        for (it = p->requestor_ids.begin(); it != p->requestor_ids.end(); ++it) {
            if (*it == parsed_prop_name.first) {
                p->requestor_ids.erase(it);
                break;
            }
        }
        if (it == p->requestor_ids.end()) {
            LOG(WARNING) << "Selection request ID mismatch";
        }
    }

    auto get_translate_add_selection_reply = [&](xcb_atom_t target, xcb_atom_t prop) {
        auto repdata = p->get_selection_reply(eventpooler, prop);
        // only include in the reply if no error occurred
        if (repdata.prop_atom && repdata.value.format_bpp) {
            // translate back atom name
            repdata.prop_atom = p->reqprop_translate.parse(repdata.prop_atom).second;
            // strip out blacklisted values if target is TARGET
            xcb_atom_t xa_targets = get_atom(p->conn, false, "TARGETS");
            if (target == xa_targets && repdata.value.format_bpp == 32) {
                auto stripped_targets = make_shared_malloc(malloc(repdata.value.data_len));
                auto targets_ptr = (xcb_atom_t*)repdata.value.data_ptr.get();
                auto stripped_targets_ptr = (xcb_atom_t*)stripped_targets.get();
                int stripped_count = 0;
                for (int i = 0; i < repdata.value.data_len / 4; ++i) {
                    if (!p->blacklist_target_atoms.count(targets_ptr[i])) {
                        stripped_targets_ptr[stripped_count++] = targets_ptr[i];
                    }
                }
                repdata.value.data_len = stripped_count * 4;
                repdata.value.data_ptr = stripped_targets;
            }

            ret.push_back(repdata);
        }
    };

    // is MULTIPLE?
    if (cevt->target == p->xa.multiple) {
        retinfo.multiple = parsed_prop_name.second;
        // get the ACKed ATOM_PAIRs (and delete it)
        auto rep = make_unique_malloc(xcb_get_property_reply(p->conn,
            xcb_get_property(p->conn, 1, proxy_wid, cevt->property, XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE),
            &err.pop()));
        if (err.get()) {
            LOG(ERROR) << "Cannot get MULTIPLE property: " << xcb_event_get_error_label(err.get()->error_code);
            return {};
        }
        if (rep.get()->format != 32) {
            LOG(ERROR) << "Invalid MULTIPLE data type";
            return {};
        }
        auto mult_rep = (multiple_atom_pair_t*)xcb_get_property_value(rep.get());
        const auto mult_rep_count = rep.get()->value_len;
        for (int i = 0; i < mult_rep_count / 2; ++i) {
            if (mult_rep[i].property) {
                get_translate_add_selection_reply(mult_rep[i].target, mult_rep[i].property);
            }
        }
    } else {
        retinfo.sr_prop = parsed_prop_name.second;
        get_translate_add_selection_reply(cevt->target, cevt->property);
    }
    return make_tuple(parsed_prop_name.first, retinfo, ret);
}

xcb_atom_t SelMgr::Priv::reqprop_translate::generate_name(uint32_t requestor_id, xcb_atom_t prop) {
    auto name = string_printf("%s_%d_%s", prefix.c_str(), requestor_id, get_atom_name(conn, prop).c_str());
    return get_atom(conn, false, name);
}

pair<uint32_t, xcb_atom_t> SelMgr::Priv::reqprop_translate::parse(xcb_atom_t atom) {
    constexpr auto err_atom_name = "Passed atom name doesn't match criteria";
    string atom_name = get_atom_name(conn, atom);
    // basic check
    if ((atom_name.size() < prefix.length() + 3) || (atom_name.compare(0, prefix.size(), prefix) != 0)) {
        LOG(WARNING) << err_atom_name;
        return {0, 0};
    }

    int num_begin_pos = prefix.length() + 1;
    uint32_t requestor_id = atoi(&atom_name.c_str()[num_begin_pos]);

    int prop_name_begin_pos = atom_name.find('_', num_begin_pos);
    if (prop_name_begin_pos < 0) {
        LOG(WARNING) << err_atom_name;
        return {0, 0};
    }
    ++prop_name_begin_pos;
    xcb_atom_t prop = get_atom(conn, false, atom_name.substr(prop_name_begin_pos));

    return {requestor_id, prop};
}

bool SelMgr::Priv::add_event_listener(xcb_window_t wid, uint32_t add_event_mask) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto attr =
        make_unique_malloc(xcb_get_window_attributes_reply(conn, xcb_get_window_attributes(conn, wid), &err.pop()));
    if (err.get()) {
        // probably the window has already destroyed
        LOG(INFO) << "Cannot get attribute";
        return false;
    }
    if ((attr->your_event_mask & add_event_mask) == add_event_mask) {
        // no need to change. already listened.
        return true;
    } else {
        uint32_t new_event_mask = attr->your_event_mask | add_event_mask;
        err.pop() = xcb_request_check(
            conn, xcb_change_window_attributes_checked(conn, wid, XCB_CW_EVENT_MASK, &new_event_mask));
        // make sure this suceed
        if (err.get()) {
            LOG(INFO) << "Cannot set event mask attribute";
            return false;
        }
        return true;
    }
}

void SelMgr::Priv::notify_requestor(xcb_window_t requestor, xcb_atom_t sel, xcb_atom_t target, xcb_atom_t prop) {
    xcb_selection_notify_event_t evt;
    memset(&evt, 0, sizeof(evt));
    evt.response_type = XCB_SELECTION_NOTIFY;
    evt.requestor = requestor;
    evt.selection = sel;
    evt.target = target;
    evt.property = prop;
    xcb_send_event(conn, 0, requestor, 0, (char*)&evt);
    xcb_flush(conn);
}

XProperty SelMgr::Priv::get_selection_reply(EventPooler* eventpooler, xcb_atom_t prop) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    // get and delete the property
    auto prop_rep = make_unique_malloc(xcb_get_property_reply(conn,
        xcb_get_property(conn, 1, parent->proxy_wid, prop, XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE),
        &err.pop()));
    if (err.get()) {
        LOG(ERROR) << "Cannot get property: " << xcb_event_get_error_label(err.get()->error_code);
        return {};
    }
    if (!prop_rep.get()->format) {
        return {};
    }
    auto prop_val = xcb_get_property_value(prop_rep.get());

    XProperty ret;
    ret.prop_atom = prop;
    // INCR need special treatments
    if (prop_rep.get()->type == get_atom(conn, false, "INCR")) {
        if (prop_rep.get()->format != 32 || prop_rep.get()->value_len < 1) {
            LOG(WARNING) << "Invalid INCR response";
            return {};
        }
        constexpr timespec INCR_PROP_TIMEOUT{1, 0};
        const uint32_t incr_sz_expected = SelConverterPriv::swap_endian(*((uint32_t*)prop_val));
        uint32_t incr_sz = 0;
        vector<pair<uint32_t, shared_ptr<xcb_get_property_reply_t>>> incr_props;

        shared_ptr<xcb_get_property_reply_t> incr_prop = nullptr;
        bool incr_prop_timed_out = false;
        // begin incremental transfer
        // custom, temporary event handler for XCB's eventpoller. "Output" will be in incr_prop and incr_prop_timed_out.
        function<bool(const xcb_generic_event_t*)> incr_event_handler([&](const xcb_generic_event_t* evt) -> bool {
            // make sure this is the intended event
            if (evt) {
                if ((evt->response_type & 0x7f) == XCB_PROPERTY_NOTIFY) {
                    auto cevt = (xcb_property_notify_event_t*)evt;
                    if (cevt->state == XCB_PROPERTY_NEW_VALUE && cevt->window == parent->proxy_wid &&
                        cevt->atom == prop) {
                        // this event is designated to us, so we always return true regardless of the result
                        incr_prop = make_shared_malloc(
                            xcb_get_property_reply(conn, xcb_get_property(conn, 1, parent->proxy_wid, prop,
                                                             XCB_GET_PROPERTY_TYPE_ANY, 0, MAX_PROPERTIES_SIZE),
                                &err.pop()));
                        if (err.get()) {
                            LOG(ERROR) << util::string_printf(
                                "Cannot read INCR property data: %s. This INCR will be truncated.",
                                xcb_event_get_error_label(err.get()->error_code));
                            incr_prop = nullptr;
                        }
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                incr_prop_timed_out = true;
                return false;
            }
        });
        do {
            // repeat until we get the event that we wanted.
            do {
                eventpooler->custom_wait_for_event(incr_event_handler, INCR_PROP_TIMEOUT);
                if (incr_prop.get()) {
                    break;
                }
            } while (!incr_prop_timed_out);
            if (incr_prop_timed_out && !incr_prop.get()) {
                LOG(WARNING) << "INCR timed out.";
                break;
            }
            // process the event
            uint32_t prop_sz = xcb_get_property_value_length(incr_prop.get());
            if (incr_prop.get() && incr_prop.get()->format && prop_sz) {
                // property info (let's take from first property-reply only)
                if (!ret.value.format_bpp) {
                    ret.value.format_bpp = incr_prop.get()->format;
                    ret.value.prop_type = incr_prop.get()->type;
                }
                // store for later reallocation and join
                incr_sz += prop_sz;
                incr_props.push_back({prop_sz, incr_prop});
                // don't forget to reset this, for the next loop
                incr_prop = nullptr;
            } else {
                break;
            }
        } while (true);

        // make sure the owner actually replies
        if (!incr_props.size()) {
            LOG(WARNING) << "Owner doesn't reply to INCR";
            return {};
        }
        // log on size mismatch
        if (incr_sz != incr_sz_expected) {
            LOG(WARNING) << util::string_printf("INCR size mismatch. Got %d, expected %d. Type: %s | Format: %d bit.",
                incr_sz, incr_sz_expected, get_atom_name(conn, ret.value.prop_type).c_str(), ret.value.format_bpp);
        }
        // combine incr_props values to ret
        ret.value.data_len = incr_sz;
        ret.value.data_ptr = make_unique_malloc(malloc(incr_sz));
        uint32_t incr_offset = 0;
        for (auto& incr_prop : incr_props) {
            memcpy(&((uint8_t*)ret.value.data_ptr.get())[incr_offset], xcb_get_property_value(incr_prop.second.get()),
                incr_prop.first);
            incr_offset += incr_prop.first;
        }
        return ret;
    } else {
        // straightforward!
        if ((ret.value.format_bpp = prop_rep.get()->format)) {
            ret.value.prop_type = prop_rep.get()->type;
            ret.value.data_len = prop_rep.get()->value_len * (prop_rep.get()->format / 8);
            if (ret.value.data_len) {
                ret.value.data_ptr = make_shared_malloc(malloc(ret.value.data_len));
                memcpy(ret.value.data_ptr.get(), xcb_get_property_value(prop_rep.get()), ret.value.data_len);
            }
            return ret;
        } else {
            return {};
        }
    }
}

void SelMgr::Priv::assign_properties(xcb_window_t requestor, vector<XProperty>& properties) {
    for (auto& prop : properties) {
        if (prop.value.format_bpp) {
            // INCR?
            if (prop.value.data_len > INCR_SIZE) {
                // atoms
                xcb_atom_t xa_incr = get_atom(conn, false, "INCR");
                xcb_change_property(
                    conn, XCB_PROP_MODE_REPLACE, requestor, prop.prop_atom, xa_incr, 32, 1, &prop.value.data_len);
            } else {
                xcb_change_property(conn, XCB_PROP_MODE_REPLACE, requestor, prop.prop_atom, prop.value.prop_type,
                    prop.value.format_bpp, prop.value.data_len / (prop.value.format_bpp / 8),
                    prop.value.data_ptr.get());
            }
        } else {
            // delete
            xcb_delete_property(conn, requestor, prop.prop_atom);
        }
    }
    xcb_flush(conn);
}

void SelMgr::Priv::set_multiple_property(xcb_window_t requestor,
    xcb_atom_t mult_prop,
    pair<int, multiple_atom_pair_t*> mult_req,
    vector<XProperty>& replies) {
    set<int> matches;
    for (auto& reply : replies) {
        for (int mult_idx = 0; mult_idx < mult_req.first; ++mult_idx) {
            if (reply.prop_atom == mult_req.second[mult_idx].property) {
                matches.insert(mult_idx);
                break;
            }
        }
    }
    for (int mult_idx = 0; mult_idx < mult_req.first; ++mult_idx) {
        if (!matches.count(mult_idx)) {
            mult_req.second[mult_idx].property = XCB_ATOM_NONE;
        }
    }

    xcb_atom_t xa_atom_pair = get_atom(conn, false, "ATOM_PAIR");
    xcb_change_property(
        conn, XCB_PROP_MODE_REPLACE, requestor, mult_prop, xa_atom_pair, 32, mult_req.first * 2, mult_req.second);
    xcb_flush(conn);
}

void SelMgr::Priv::incr_transfer(EventPooler* eventpooler, xcb_window_t requestor, XProperty& prop) {
    bool incr_prop_timed_out = false;
    bool incr_requestor_ack = false;
    function<bool(const xcb_generic_event_t*)> proplisten = [&](const xcb_generic_event_t* evt) -> bool {
        if (evt) {
            if ((evt->response_type & 0x7f) == XCB_PROPERTY_NOTIFY) {
                auto cevt = (xcb_property_notify_event_t*)evt;
                if (cevt->state == XCB_PROPERTY_DELETE && cevt->window == requestor && cevt->atom == prop.prop_atom) {
                    incr_requestor_ack = true;
                    // this event is designated to us, so we always return true regardless of the result
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            incr_prop_timed_out = true;
            return false;
        }
    };

    timespec ack_timeout{1, 0};
    bool transfer_done = false;
    uint32_t rem = prop.value.data_len;
    uint32_t total_transfer = 0;
    while (!transfer_done) {
        incr_requestor_ack = incr_prop_timed_out = false;
        // wait until requestor "ACK" by deleting the property
        do {
            eventpooler->custom_wait_for_event(proplisten, ack_timeout);
            if (incr_prop_timed_out) {
                LOG(WARNING) << "INCR requestor ack timed out.";
                break;
            }
        } while (!incr_requestor_ack);
        // put the data
        if (rem && incr_requestor_ack) {
            void* dataptr = &((uint8_t*)prop.value.data_ptr.get())[total_transfer];
            uint32_t transfer_bytes = min(rem, INCR_SIZE);
            uint32_t transfer_data_len = min(rem, INCR_SIZE) / (prop.value.format_bpp / 8);
            xcb_change_property(conn, XCB_PROP_MODE_REPLACE, requestor, prop.prop_atom, prop.value.prop_type,
                prop.value.format_bpp, transfer_data_len, dataptr);
            xcb_flush(conn);
            rem -= transfer_bytes;
            total_transfer += transfer_bytes;
        } else {
            transfer_done = true;
        }
    }

    if (rem) {
        // log if transfer is unfinished
        LOG(WARNING) << string_printf(
            "INCR transfer to requestor unfinished (type: '%s')", get_atom_name(conn, prop.value.prop_type).c_str());
    } else {
        // last confirmation (by sending zero sized property)
        xcb_change_property(conn, XCB_PROP_MODE_REPLACE, requestor, prop.prop_atom, prop.value.prop_type,
            prop.value.format_bpp, 0, nullptr);
        xcb_flush(conn);
    }
}

void SelConverter::convert_request_atoms(xcb_connection_t* src_conn,
    xcb_connection_t* dst_conn,
    SelectionInfo& selinfo,
    vector<SelectionRequest>& reqs) {
    selinfo.multiple = translate_atom(src_conn, dst_conn, selinfo.multiple);
    selinfo.selection = translate_atom(src_conn, dst_conn, selinfo.selection);
    selinfo.sr_prop = translate_atom(src_conn, dst_conn, selinfo.sr_prop);
    for (auto& req : reqs) {
        // selection target (type)
        req.target = translate_atom(src_conn, dst_conn, req.target);
        // properties
        req.param.prop_atom = translate_atom(src_conn, dst_conn, req.param.prop_atom);
        req.param.value.prop_type = translate_atom(src_conn, dst_conn, req.param.value.prop_type);
        // also convert property values if the type is ATOM
        if (req.param.value.format_bpp == 32 && req.param.value.prop_type == XCB_ATOM_ATOM) {
            translate_atoms(
                src_conn, dst_conn, req.param.value.data_len / 4, (xcb_atom_t*)req.param.value.data_ptr.get());
        }
    }
}

void SelConverter::convert_reply_atoms(xcb_connection_t* src_conn,
    xcb_connection_t* dst_conn,
    SelectionInfo& selinfo,
    vector<XProperty>& reps) {
    selinfo.multiple = translate_atom(src_conn, dst_conn, selinfo.multiple);
    selinfo.selection = translate_atom(src_conn, dst_conn, selinfo.selection);
    selinfo.sr_prop = translate_atom(src_conn, dst_conn, selinfo.sr_prop);
    for (auto& rep : reps) {
        // properties
        rep.prop_atom = translate_atom(src_conn, dst_conn, rep.prop_atom);
        rep.value.prop_type = translate_atom(src_conn, dst_conn, rep.value.prop_type);
        // also convert property values if the type is ATOM
        if (rep.value.format_bpp == 32 && rep.value.prop_type == XCB_ATOM_ATOM) {
            translate_atoms(src_conn, dst_conn, rep.value.data_len / 4, (xcb_atom_t*)rep.value.data_ptr.get());
        }
    }
}

uint32_t SelConverterPriv::swap_endian(uint32_t num) {
    return (((num & 0x000000FF) << 24) | ((num & 0x0000FF00) << 8) | ((num & 0x00FF0000) >> 8) |
        ((num & 0xFF000000) >> 24));
}