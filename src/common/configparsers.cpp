#include "common/configparsers.h"
#include "shared/i18n.h"
#include <cmath>
#include <cstring>
#include <map>

using namespace pxwm::common::configparsers;
using namespace pxwm::states;
using namespace std;

namespace priv {
    template<typename K>
    static bool parse_class_enum(const char* arg, map<string, K> map, const char* errmsg, K& out) {
        bool status = commonparsers::parse_class_enum<K>(arg, map, out);
        if (!status) {
            puts(_(errmsg));
        }
        return status;
    };
}

bool commonparsers::parse_bool(const char* arg, bool& out) {
    static map<string, bool> tbl{{"yes", true}, {"no", false}, {"true", true}, {"false", false}};
    auto it = tbl.find(arg);
    if (it == tbl.end()) {
        return false;
    }
    out = it->second;
    return true;
}

bool BkFrontEndOptionsParser::parse_default_scale_factor(const char* arg, uint16_t& out) {
    uint16_t scfactor = strtol(arg, nullptr, 10);
    if (scfactor > Consts::values.max_scaling_factor) {
        printf(_("Maximum scaling factor is %d."), Consts::values.max_scaling_factor);
        puts("");
        return false;
    } else if (scfactor < Consts::values.min_scaling_factor) {
        printf(_("Minimum scaling factor is %d."), Consts::values.min_scaling_factor);
        puts("");
        return false;
    }
    out = scfactor;
    return true;
}

bool BkFrontEndOptionsParser::parse_scale_algorithm(const char* arg, ScalingAlgorithm& out) {
    return priv::parse_class_enum(arg, Consts::values.avail_scale_algorithms, _("Invalid scaling algorithm."), out);
}

bool BkFrontEndOptionsParser::parse_framerate_mode(const char* arg, FrameRateMode& out) {
    return priv::parse_class_enum(arg, Consts::values.avail_framerate_modes, _("Invalid frame rate mode."), out);
}

bool BkFrontEndOptionsParser::parse_shm_mode(const char* arg, pxwm::states::SHMMode& out) {
    return priv::parse_class_enum(arg, Consts::values.avail_shm_modes, _("Invalid SHM mode."), out);
}

bool OptionsValidator::check_scaler_scale(pxwm::states::ScalingAlgorithm alg, uint8_t scfactor) {
    // unlisted algorithm in the map means there is no limit about what it can handle
    uint16_t max = Consts::values.max_scaling_factor;
    pair<uint8_t, uint8_t> limit{1, max};
    if (Consts::values.scale_alg_scale_limits.count(alg)) {
        limit = Consts::values.scale_alg_scale_limits.at(alg);
    }
    if (scfactor >= limit.first && scfactor <= limit.second) {
        return true;
    } else {
        printf(_("Scaling factor for algorithm '%1$s' must be between %2$d and %3$d."),
            Consts::values.vmap_scale_algorithms.at(alg).c_str(), limit.first, limit.second);
        puts("");
        return false;
    }
}