#include "app.h"
#include "backend/backend.h"
#include "common/configparsers.h"
#include "common/stringconsts.h"
#include "external/pxdip.h"
#include "ipc/client.h"
#include "ipc/consts.h"
#include "ipc/server.h"
#include "shared/arraytoken.h"
#include "shared/compilerinfo.h"
#include "shared/fixedpt.h"
#include "shared/i18n.h"
#include "sighandlers.h"
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <glog/logging.h>
#include <locale.h>
#include <shared/util.h>
#include <signal.h>
#include <string>
#include <sys/file.h>
#include <thread>
#include <unistd.h>

using namespace px;
using namespace px::util;
using namespace pxwm;
using namespace pxwm::states;
using namespace pxwm::backend;
using namespace pxwm::common::configparsers;
using namespace std;

class MainApp::Priv {
public:
    class ConfigArgParsers;
    class LockFileManager;
    enum class ParseArgsResult { NONE, AS_MAIN, AS_CLIENT };
    shared_ptr<Backend> backend;
    unique_ptr<LockFileManager> lfman;

    Priv(MainApp* r) : parent{r} {}
    Priv(Priv&) = delete;
    Priv& operator=(const Priv&) = delete;

    void init_I18N();
    void show_help();
    ParseArgsResult parse_args_init(int argc, char** argv);

    bool handle_startpaused();

private:
    MainApp* const parent;
};

class MainApp::Priv::ConfigArgParsers {
public:
    static bool parse_args_main(int argc, char** argv, Config& cfg);
    static bool parse_bk_display_options(shared_ptr<ArrayToken> tkzer, string dpy_name, BkFrontEndOptions& cfg);
};

class MainApp::Priv::LockFileManager {
public:
    enum class SysErrorType { NONE, OPEN_OR_CREAT, BIND };
    SysErrorType sys_err_type = SysErrorType::NONE;
    int sys_err_code = 0;
    string sys_err_msg;
    bool another_instance_already_running = false;
    bool has_lock = false;

    LockFileManager(string lockfile);
    LockFileManager(LockFileManager&) = delete;
    ~LockFileManager();
    LockFileManager& operator=(const LockFileManager&) = delete;

private:
    const int LOCK_CREAT_MODE = 0444;
    string lockfile;
    int lock_fd;
};

MainApp::MainApp() {
    p = make_unique<Priv>(this);
    cfg = make_shared<Config>();
    p->init_I18N();
}

MainApp::MainApp(MainApp&& src) {
    p = move(src.p);
    cfg = move(src.cfg);
}

MainApp::~MainApp() {}

void MainApp::run(int argc, char** argv) {
    // determine the type of this instance (client or main instance/server)
    // also retrieve user-defined lock file and socket file path
    auto arg_cmd = p->parse_args_init(argc, argv);
    if (arg_cmd == Priv::ParseArgsResult::NONE) {
        return;
    }
    // the lock file (derived from socketfile)
    p->lfman = make_unique<Priv::LockFileManager>(cfg->socketfile_path + ".lock");

    if (p->lfman->sys_err_type != Priv::LockFileManager::SysErrorType::NONE) {
        // error locking file
        switch (p->lfman->sys_err_type) {
            case Priv::LockFileManager::SysErrorType::BIND:
                fputs(_("Cannot bind the lock file: "), stderr);
                break;
            case Priv::LockFileManager::SysErrorType::OPEN_OR_CREAT:
                fputs(_("Cannot open or create the lock file: "), stderr);
                break;
            default:
                // should not happen
                fputs(_("Unknown error opening lock file: "), stderr);
                LOG(FATAL) << "Unknown error opening lock file";
                break;
        }
        fputsn(p->lfman->sys_err_msg.c_str(), stderr);
    } else if (p->lfman->another_instance_already_running) {
        // client mode
        if (arg_cmd != Priv::ParseArgsResult::AS_CLIENT) {
            fputsn(
                    _("Another instance is already running on this user. To manage the other instance, use the '--manage' switch option."),
                    stderr);
            fputsn(stringconsts::values().PLEASE_READ_HELP, stderr);
            LOG(INFO) << "Attempt to run another instance of px-wm.";
            return;
        }
        ipc::Client ipc_client(cfg->socketfile_path);
        ipc_client.run_cmdline_args(argc, argv);
    } else {
        // main/server mode
        if (arg_cmd != Priv::ParseArgsResult::AS_MAIN) {
            fputsn(_("No other instance is running. Please remove '--manage' switch option."), stderr);
            fputsn(stringconsts::values().PLEASE_READ_HELP, stderr);
            LOG(INFO) << "Attempt to manage a nonexistent instance of px-wm.";
            return;
        }
        if (!Priv::ConfigArgParsers::parse_args_main(argc, argv, *cfg)) {
            return;
        }
        // check the used shared library of px-dip
        if (pxdip_api_version() != PXDIP_API) {
            fprintf(
                    stderr, _("Invalid API version of px-dip. Expected %1$d, got %2$d."), PXDIP_API, pxdip_api_version());
            fputc('\n', stderr);
            return;
        }
        // backend with supplied config
        p->backend = make_shared<Backend>(cfg);

        // start the IPC server (via UDS)
        ipc::Server ipc_server(cfg->socketfile_path, p->backend);
        // and make sure it is up and running
        if (!ipc_server.server_running) {
            fputsn(_("Cannot start IPC server. Aborting program."), stderr);
            return;
        }

        // install signal handlers
        pxwm::sighandlers::install_signal_handlers(p->backend);

        // This will block until backend stops.
        p->backend->run();
    }
}

void MainApp::Priv::init_I18N() {
    char* tmp;
    // make sure every numeric is formatted with 'C' standard
    tmp = setlocale(LC_ALL, "C");
    // but we still want to speak local languages
    tmp = setlocale(LC_MESSAGES, "");

    // gettext magic!
    tmp = bindtextdomain("px-wm", nullptr);
    tmp = textdomain("px-wm");
}

MainApp::Priv::ParseArgsResult MainApp::Priv::parse_args_init(int argc, char** argv) {
    if (argc == 1) {
        show_help();
        return ParseArgsResult::NONE;
    }

    ArrayToken tokenizer(argc, argv);
    // skip 1st arg, which is the current image file name
    tokenizer.next_token();

    bool error_occur = false;
    bool req_client_mode = false;
    char* token;

    while (!tokenizer.is_end()) {
        token = tokenizer.next_token();
        if (strcmp(token, "--help") == 0) {
            show_help();
            return ParseArgsResult::NONE;
        } else if (strcmp(token, "--version") == 0) {
            // info about this application itself
            puts(_("px_wm, The Project X' main window manager."));
            printf(_("Build: %1$d"), VERSION_ORD);
            putchar('\n');
            printf(_("IPC API version: %1$d"), pxwm::ipc::ConstData::IPC_API_VERSION);
            putchar('\n');
            // info about libpx_dip.so shared library: the image processing library
            printf(_("Loaded %1$s library info:"), "libpx_dip.so");
            fputs("\n  ", stdout);
            printf(_("API version: %1$d"), pxdip_api_version());
            fputs("\n  ", stdout);
            printf(_("Revision: %1$d"), pxdip_revision());
            fputs("\n  ", stdout);
            printf(_("Aux version info: '%1$s'"), pxdip_version_desc());
            putchar('\n');
            return ParseArgsResult::NONE;
        } else if (strcmp(token, "--compilerinfo") == 0) {
            print_compiler_info();
            return ParseArgsResult::NONE;
//        } else if (strcmp(token, "--lockfile") == 0) {
//            token = tokenizer.next_token();
//            if (token == nullptr) {
//                puts(_("Please specify path to desired lock file."));
//                error_occur = true;
//            } else {
//                parent->cfg->lockfile_path = token;
//            }
        } else if (strcmp(token, "--socketfile") == 0) {
            token = tokenizer.next_token();
            if (token == nullptr) {
                puts(_("Please specify path to desired Unix socket file."));
                error_occur = true;
            } else {
                parent->cfg->socketfile_path = token;
            }
        } else if (strcmp(token, "--manage") == 0) {
            req_client_mode = true;
        } else if (strcmp(token, "--startpaused") == 0) {
            if (!handle_startpaused()) {
                return ParseArgsResult::NONE;
            }
        }
        if (error_occur) {
            break;
        }
    }

    if (error_occur) {
        fputs(stringconsts::values().ERROR_PARSE_ARG, stderr);
        fputc(' ', stderr);
        fputsn(stringconsts::values().PLEASE_READ_HELP, stderr);
        return ParseArgsResult::NONE;
    }
    return req_client_mode ? ParseArgsResult::AS_CLIENT : ParseArgsResult::AS_MAIN;
}

bool MainApp::Priv::handle_startpaused() {
#ifdef NDEBUG
    printf(_("'%1$s' option is only valid in debug build."), "--startpaused");
    putchar('\n');
    return false;
#else
    // print PID and SIGSTOP
    fputs("'--startpaused' specified. Process will be stopped. Please attach the debugger.\n", stderr);
    fprintf(stderr, "PID: %d\n", getpid());
    raise(SIGSTOP);
    return true;
#endif
}

bool MainApp::Priv::ConfigArgParsers::parse_args_main(int argc, char** argv, Config& cfg) {
    shared_ptr<ArrayToken> tokenizer = make_shared<ArrayToken>(argc, argv);
    // skip 1st arg, which is the current image file name
    tokenizer->next_token();

    bool error_occur = false;
    char* token;

    // quick and dirty argument parsing
    while (!tokenizer->is_end()) {
        token = tokenizer->next_token();
        if (strcmp(token, "--bkdisplay") == 0) {
            auto dpy_name = tokenizer->next_token();
            if (dpy_name == nullptr) {
                puts(_("Please specify X11 DISPLAY to manage."));
                error_occur = true;
                break;
            }
            BkFrontEndOptions bkf_opts;
            if (!ConfigArgParsers::parse_bk_display_options(tokenizer, dpy_name, bkf_opts)) {
                error_occur = true;
                printf(_("One or more option(s) given to backend display '%1$s' is invalid."), dpy_name);
                puts("");
            }
            cfg.bk_displays[string(dpy_name)] = bkf_opts;
        }
        if (error_occur) {
            break;
        }
    }

    if (error_occur) {
        fputs(stringconsts::values().ERROR_PARSE_ARG, stderr);
        fputc(' ', stderr);
        fputsn(stringconsts::values().PLEASE_READ_HELP, stderr);
        LOG(WARNING) << "Invalid option(s) given to program.";
        return false;
    } else {
        return true;
    }
}

bool MainApp::Priv::ConfigArgParsers::parse_bk_display_options(shared_ptr<ArrayToken> tkzer,
    string dpy_name,
    BkFrontEndOptions& cfg) {
    static constexpr int T_SCALE_ALG = 1, T_FRAMERATE_MODE = 2, T_MAX_SHM_MODE = 3, T_DEFAULT_SCALE_FACTOR = 4,
                         T_RETURN = 5;
    static const string arg_common_prefix = "--bkd";
    static const map<string, int> token_table = {{"--bkd-scale-alg", T_SCALE_ALG},
        {"--bkd-framerate-mode", T_FRAMERATE_MODE}, {"--bkd-default-scale-factor", T_DEFAULT_SCALE_FACTOR},
        {"--bkd-max-shm-mode", T_MAX_SHM_MODE}, {"--bkdisplay", T_RETURN}};
    while (!tkzer->is_end()) {
        string token_str = tkzer->peek_next_token();
        auto tk_tbl_iter = token_table.find(token_str);
        int token = tk_tbl_iter == token_table.end() ? 0 : tk_tbl_iter->second;

        // make sure we don't parse arguments / options that doesn't belong to us.
        // if we encountered another '--bkdisplay', then it is caller's time to handle that!
        if (token != T_RETURN && token_str.compare(0, arg_common_prefix.size(), arg_common_prefix) == 0) {
            tkzer->next_token();
        } else {
            return true;
        }

        // every options here takes exactly 1 argument.
        auto param_token = tkzer->next_token();
        if (param_token == nullptr) {
            printf(_("Incomplete argument given to an option for display '%1$s'."), dpy_name.c_str());
            puts("");
            return false;
        }
        bool subparse_status = false;
        switch (token) {
            case T_SCALE_ALG:
                subparse_status = BkFrontEndOptionsParser::parse_scale_algorithm(param_token, cfg.scale_algorithm);
                break;
            case T_FRAMERATE_MODE:
                subparse_status = BkFrontEndOptionsParser::parse_framerate_mode(param_token, cfg.framerate_mode);
                break;
            case T_MAX_SHM_MODE:
                subparse_status = BkFrontEndOptionsParser::parse_shm_mode(param_token, cfg.max_shm_mode);
                break;
            case T_DEFAULT_SCALE_FACTOR:
                subparse_status = BkFrontEndOptionsParser::parse_default_scale_factor(param_token, cfg.scale_factor);
                break;
            default:
                // warn, but just ignore it.
                printf(
                    _("Encountered an unknown display option '%1$s' on '%2$s'."), token_str.c_str(), dpy_name.c_str());
                puts("");
                break;
        }
        if (!subparse_status) {
            printf(_("Invalid argument given to option '%1$s' on display '%2$s'"), token_str.c_str(), dpy_name.c_str());
            puts("");
            return false;
        }
        if (!OptionsValidator::check_scaler_scale(cfg.scale_algorithm, cfg.scale_factor)) {
            return false;
        }
    }
    return true;
}

void MainApp::Priv::show_help() {
    puts("This is help, unlocalized");
}

MainApp::Priv::LockFileManager::LockFileManager(string lockfile) {
    this->lockfile = lockfile;

    util::DeleteInvoker sys_msg_filler([this]() {
        if (sys_err_type != SysErrorType::NONE && sys_err_code) {
            sys_err_msg = strerror(sys_err_code);
        }
    });

    // open the file first in read mode, and create if doesn't exist
    for (int i = 0; i <= 1; ++i) {
        lock_fd = open(lockfile.c_str(), O_CREAT | O_RDONLY, LOCK_CREAT_MODE);
        if (lock_fd < 0) {
            if (i) {
                // The last trial. Unrecoverable failure.
                sys_err_code = errno;
            } else if (errno == EACCES) {
                // Probably wrong permission. Try recreating.
                unlink(lockfile.c_str());
            } else {
                // Unrecoverable failure on the first try.
                sys_err_code = errno;
                break;
            }
        }
    }
    // a system error happened on open or create.
    if (sys_err_code) {
        sys_err_type = SysErrorType::OPEN_OR_CREAT;
        LOG(ERROR) << "Cannot create lock file.";
        return;
    }

    // bind the opened file
    if (flock(lock_fd, LOCK_EX | LOCK_NB) < 0) {
        if (errno == EWOULDBLOCK) {
            another_instance_already_running = true;
        } else {
            // an unknown system error happened on file bind
            sys_err_code = errno;
            sys_err_type = SysErrorType::BIND;
            LOG(ERROR) << "Cannot exclusively bind the lock file.";
        }
    } else {
        has_lock = true;
    }
}

MainApp::Priv::LockFileManager::~LockFileManager() {
    // unlock and delete the lock file
    if (has_lock) {
        flock(lock_fd, LOCK_UN);
        unlink(lockfile.c_str());
    }
}